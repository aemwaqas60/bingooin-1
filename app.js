'use strict';

//require('./nodemon_config');
require('./config/mongo');

const express = require('express');
const path = require('path');
const favicon = require('serve-favicon');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const passport = require('passport');
const session = require('express-session');
const async = require('async');
const moment = require('moment');
const _ = require('underscore');
const jwt = require('jsonwebtoken');
const helmet = require(`helmet`);
const mongoose = require('mongoose');
var morgan = require('morgan')



const jwtSecret = process.env.jwtSecret;
const signedCookieSecret = process.env.signedCookieSecret;
const nsalt = process.env.nsalt;

const cookieOptions = {
  maxAge: 3600000,
  signed: 1,
  /*Indicates if the cookie should be signed.*/
  httpOnly: 1,
  /*Flags the cookie to be accessible only by the web server.*/
  secure: 0,
  /*Marks the cookie to be used with HTTPS only.*/
}

const Salthashpassword = require('./helper/salt-hash-password.js');
let ObjSalthashpassword = new Salthashpassword(nsalt);

const Auth = require('./helper/auth.class');
let objAuth = new Auth(jwt, jwtSecret, _);

const index = require('./routes/index')(ObjSalthashpassword, jwt, path, cookieOptions, jwtSecret, _);
const admin = require('./routes/admin')(ObjSalthashpassword, objAuth, async, moment, path, mongoose, _);
const apis = require('./routes/apis')(ObjSalthashpassword, objAuth, async, moment, path, _);

var app = express();

// morgan middleware( helps to logs all https requests on console)
app.use(morgan('dev'))

app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());



//Helmet helps you secure your Express apps by setting various HTTP headers. 

app.use(helmet());
app.use(helmet.noCache());





// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

// uncomment after placing your favicon in /public
app.use(favicon(path.join(__dirname, 'favicon.ico')));
//app.use(logger('dev'));
app.use(cookieParser(signedCookieSecret)); //secret key for signed flag




app.use('/admin', admin);

app.use('/apis', (req, res, next) => {


  let api_key = req.get('api_key');
  if (api_key === '' || api_key === undefined) {
    return res.json({
      status: 422,
      status_message: "missing api key error",
      data: []
    })
  }
  if (api_key !== process.env.api_key) {
    return res.json({
      status: 401,
      status_message: "Api-key Authentication error",
      data: []
    })
  }
  next();
}, apis);
app.use('/', index);

app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'files/images')));
app.use(express.static(path.join(__dirname, 'files/icons')));
app.use(express.static(path.join(__dirname, 'files/qr')));

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  //res.locals.message = err.message;
  //res.locals.error = req.app.get('env') === 'development' ? err : {};
  if (!err)
    return;
  // render the error page
  //res.status(err.status || 500);
  //res.status(err.status).json({status:err.status})
  console.log(err.status)
  if (err.status === 404)
    return res.status(404).json({
      status: 404,
      status_message: "not found"
    });

  if (err.status === 401)
    return res.status(401).clearCookie('_connecti', {
      path: '/'
    }).json({
      status: 401,
      status_message: "you are authorized for this action."
    })

  if (err.status === 510)
    return res.status(500).json({
      status: 510,
      status_message: err.message
    });

  return res.status(err.status || 500).json({
    status: err.status
  });
  //res.render('error');
});

module.exports = app;