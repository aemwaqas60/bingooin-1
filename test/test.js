'use strict';
const mocha = require('mocha');
const chai = require('chai');
const baseUrl = `http://localhost:3000/apis`;
const request = require('supertest');
let should = chai.should();

var assert = chai.assert;
const expect = chai.expect;
/*
describe('get wardrobe against look', () => {
  it('first case', (done) => {

    request(baseUrl)
      .get('/user/looks/')
      .query('casual')
      .expect('Content-Type', /json/)
      .expect(200)
      .end((err, res)=>{
        if (err) return done(err);
        done();
      })
  });
});
*/
describe('Add product to user wardrobe', () => {
  it('first case', (done) => {

    request(baseUrl)
      .post('/user/products')
      .send({productId: '123123'})
      .expect('Content-Type', /json/)
      .expect(200)
      .expect((res)=>{
        console.log(res.body)
          res.body.should.have.property("productId", "123123");
      })
      .end((err, res)=>{
        if (err) return done(err);
        console.log(res.body)
        expect(res.body.productId).to.equal("12");
        done();
      })
  });
});