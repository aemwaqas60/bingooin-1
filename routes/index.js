const express = require('express'),
      router = express.Router(),
      mongoose = require('mongoose'),
      _ = require('underscore'),
      randomstring = require("randomstring");
require('../models/user.model');
let Users = mongoose.model('User');

module.exports = (ObjSalthashpassword, jwt, path, cookieOptions, jwtSecret, _)=>{

  router.get('/', function(req, res, next) {
    console.log('___+_+_+_+_+_+_+_+_+_+----')
    return res.sendFile( path.join(__dirname, '/../public/account.html'));
    //return res.sendFile( path.join(__dirname, '/../public/index.html'));
  });


  router.get('/signin', (req, res, next)=>{
      res.json(req.user);
  })

  router.post('/signin', (req, res, next)=>{
    console.log(req.body);
    let {username , password} = req.body;
    console.log({username , password})
    ObjSalthashpassword.saltHashPassword(password, (err, token)=>{

      Users.find({username: username, password:token}, (err, user, numAffected) => {
        if(_.isEmpty(user))
          return res.json({status:510, status_message:'either username or password invalid'});

        console.log(user[0]);
        user = user[0];
        let cookieToken = jwt.sign({ data: user }, jwtSecret, { expiresIn: '1h' });

        res.cookie('_connecti' , cookieToken, cookieOptions).json({status:1, status_message:"cookie set"});

      });

    });
  });

  router.get(`/lostPassword/:token`, (req, res, next)=>{
    let {token} = req.params;

    jwt.verify(token, jwtSecret, function(err, decoded) {
          if(err){
            console.log(err)
            return res.redirect('/#/lost-password');
            }

          let {name=``, email=``, id=0, passwordHash=``} = decoded.data;
          if(id ===0 || !token)
            return res.json({status:511, status_message:"internal error"})

          let query = `update users set token="${passwordHash}" where id=${id}`;
          console.log(query);
          return res.redirect('/')
         /* mysql.query(`update users set token=${token} where id=${id}`, (err, result)=>{
            if(err)
              return res.json({status:510, status_message:"mysql query error"})
            
            return res.redirect('/')

          })*/
      });
  })

  router.post('/lostPassword', (req, res, next)=>{
    
    let {username=``} = req.body;

		let _password = randomstring.generate(7);

    console.log(username, _password);
    mysql.query(`select * from users where username = "${username}";`, (err, result)=>{
      if(err)
        return res.json({status:1, status_message:"username not exist"});
      let {email=``, name=``, id=0, username=``} = result[0];

      if(!email)
        return res.json({status:1, status_message:"email address related to this user not found"});

      ObjSalthashpassword.saltHashPassword(_password, (err, passwordHash)=>{
			  console.log(`password => `+_password, `passwordHash => `+passwordHash);
        let token = jwt.sign({email, name, _password, id, passwordHash}, jwtSecret, { expiresIn: '1h' });
        objMailer.sendEmail(email, `Password reset for your QuizApp account`,
        `
        Hi ${name}, <br><br>
        We have received a request to reset the password for this email address.<br><br>

        To log on to the site, use the following credentials:<br>
        <span style="font-weight:bold">Username</span> : ${username}<br>
        <span style="font-weight:bold">Password</span> : ${_password}<br><br>

        and click on the link below to apply newly generated password to your account.<br><br>

        <a href = "${process.env.host}lostPassword/${token}">click here</a> This link will expire in 1 hour.

        <br>Thank you,<br>

        `);

        res.json({status:1, status_message:"Successfully sent"})
		  })
    })

		

		//

  });
  return router;
}