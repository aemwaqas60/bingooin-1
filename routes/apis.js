'use strict';
const express = require('express'),
    router = express.Router(),
	apisClass = require('../helper/apis.class');
const mongoose = require('mongoose');

module.exports = (ObjSalthashpassword, objAuth, async, moment, path,  _) =>{

	let objapisClass = new apisClass (mongoose);

	router.use('/', (req, res, next)=>{
        console.log('--------- Bingooin  APIS --------');
        next();
    });

	router.route('/users')
        .post(objapisClass.createUser);

    router.get('/users', objapisClass.getuser);

    router.route('/user/products')
        .get(objapisClass.getUserProducts)
        .post(objapisClass.postUserProducts);


    router.get('/user/looks/:look/:_id', objapisClass.getUserLook);
    
    router.get('/products/:_id', objapisClass.getProducts);

    router.get('/products', objapisClass.getProducts);
    router.post('/UserWardRobe', objapisClass.UserWardRobe); 
    router.post('/generateNewLook', objapisClass.generateNewLook);  

	return router
};