'use strict';
const express = require('express'),
    router = express.Router(),
	multer = require('multer'),
	file_handler = require('../helper/file_handler'),
	adminClass = require('../helper/admin.class');

module.exports = (ObjSalthashpassword, objAuth, async, moment, path, mongoose,  _) =>{

	let objadminClass = new adminClass (path, mongoose, async);

	router.use('/',objAuth.isAuthenticated);

	router.get('/admin_authentication', objadminClass.isAuthenticate);

	router.get('/logout', objadminClass.logout);

	router.get('/details', objadminClass.adminDetails);

	router.get('/product/combination', objadminClass.productCombination);

	router.route('/products/download/:id')
		.get(objadminClass.downloadFile);	

	router.route('/products/add')
		.get(objadminClass.getProductsAddData);

	router.route('/products',function(req,res){
	  console.log("------------inside product route----------");
	})	.get(objadminClass.getProducts)
		.post(multer({fileFilter:file_handler.fileFilter(),limits:file_handler.limits,storage:file_handler.fileSettings()}).single('file'), objadminClass.postProducts);
		
	router.route('/products/:id')
		.delete(objadminClass.deleteProducts)
		.post(multer({fileFilter:file_handler.fileFilter(),limits:file_handler.limits,storage:file_handler.updatefileSetting()}).single('file'), objadminClass.updateProducts);
	
	 router.route('/copyProduct')
	 .post(multer({fileFilter:file_handler.fileFilter(),limits:file_handler.limits,storage:file_handler.updatefileSetting()}).single('file'),objadminClass.copyProduct);

	router.route('/appsettings')
		.get(objadminClass.getAppsettings)
		.post(objadminClass.postAppsettings);

	router.route('/categories')
		.get(objadminClass.getCategories);

	return router
};



