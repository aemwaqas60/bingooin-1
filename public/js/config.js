// =========================================================================
// CONFIGURATION ROUTE
// =========================================================================

'use strict';
angular.module('blankonConfig', [])

    // Setup global settings
    .factory('settings', ['$rootScope', function($rootScope) {
        // console.log((window.location.href).replace(/^[^\/]+\/\/[^\/]+/,'').replace(/\/production\/admin\/.*$/,'/'))
        // console.log('-------------------')
        // console.log('http://localhost:3000')
        //var baseURL = 'http://localhost:3000',
        var baseURL = window.location.protocol+'//'+window.location.host,//(window.location.href).replace(/^[^\/]+\/\/[^\/]+/,'').replace(/\/production\/admin\/.*$/,'/'), // Setting base url app
        
            settings = {
                baseURL                 : baseURL,
                pluginPath              : baseURL+'/assets/global/plugins/bower_components',
                pluginCommercialPath    : baseURL+'/assets/commercial/plugins',
                globalImagePath         : baseURL+'/img',
                adminImagePath          : baseURL+'/assets/admin/img',
                cssPath                 : baseURL+'/assets/admin/css',
                jsPath                  : baseURL+'/assets/admin/js',
                dataPath                : baseURL+'/data'
        };
        $rootScope.settings = settings;
        return settings;
    }])
    .factory('alertFactory', function ($window){
        return {
            _alert: function (text){
                $("#AlertBox").text(text);
                $("#AlertBox").fadeIn();
                $window.setTimeout(function () {
                    $("#AlertBox").fadeOut(300)
                }, 3000);
            }
        }
    })
    .service('_user', function (){
        var user={
            name:'',
            email:'',
            mobile:'',
        }
        this.set = function (name, email, mobile){
            user.name = name;
            user.email = email;
            user.mobile = mobile;
        }
        this.get = function (){
            return user;
        }
        this.getname = function (){
            return user.name;
        }
    })
    .service('_request', function($rootScope, $http, $log, settings) {
            
            this.get = function (route, params, cb) {
                $http({
                    method: 'GET', 
                    url: route,
                    params: params

                }).then(function (response) {
                    if(response){
                        return cb(response.data);
                    }else{
                        $log.error(response.data.status_message)
                    }

                }, function (response) {
                    console.log(response)
                    if(response.status === 401)
                        window.location = settings.baseURL+"/account.html#/sign-in";
                    if(response.status === 422)
                        alert('request params are not correct')
                        console.log('i am in error');
                });
            }

            this.post = function (route, params, cb){
                $http({
                    method: 'POST', 
                    url: route,
                    headers: { "Content-type": 'application/json'},
                    data: params

                }).then(function (response) {
                    if(response){
                        return cb(response);
                    }else{
                        $log.error(response.data.status_message)
                    }

                }, function (response) {
                    console.log(response)
                    if(response.status === 401)
                        window.location = settings.baseURL+"/account.html#/sign-in";
                    if(response.status === 422)
                        alert('request params are not correct')
                        console.log('i am in error');
                });
            }

            this.postFile = function(route, params, cb){
                $http.post(route, params, {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined}
                })
                .success(function(data){
                    cb(data);
                })
                .error(function(data){
                    console.log(data)
                });
            }

            this.delete = function(route, param, cb){
                $http({
                    url: route,
                    method: 'DELETE',
                    data: {
                        id: param
                    },
                    headers: {
                        "Content-Type": "application/json;charset=utf-8"
                    }
                }).then(function(response) {
                    if(response.status === 200){
                        if(response.data.status === 301)
                            return console.log(response.data.status_message);
                        cb(response)
                    }
                    else{
                        alert('error');
                    }
                    
                }, function(error) {
                    console.log(error);
                });

            }
    })


    // Configuration angular loading bar
    .config(function(cfpLoadingBarProvider, $httpProvider) {
        cfpLoadingBarProvider.includeSpinner = true;
        $httpProvider.defaults.headers.common['Cache-Control'] = 'no-cache';
        $httpProvider.defaults.cache = false;
    })

    // Configuration event, debug and cache
    /*.config(['$ocLazyLoadProvider', function($ocLazyLoadProvider) {
        $ocLazyLoadProvider.config({
            events: true,
            debug: true,
            cache:false,
            cssFilesInsertBefore: 'ng_load_plugins_before',
            modules:[
                /*{
                    name: 'blankonApp.core.demo',
                    files: ['js/modules/core/demo.js']
                }*/
            /*]
        });
    }])
*/
    // Configuration ocLazyLoad with ui router
    .config(function($stateProvider, $locationProvider, $urlRouterProvider) {
        // Redirect any unmatched url

        //$urlRouterProvider.otherwise('sign-in');
        $urlRouterProvider.otherwise(function($injector, $location){
            
            var settings = $injector.get('settings');
            var _request = $injector.get('_request');
            var state = $injector.get('$state');
            _request.get('/admin/admin_authentication', {}, function(response){
                if(response.status != 401)
                    window.location = settings.baseURL+"/index.html#/products";
                else
                    window.location = settings.baseURL+"/account.html#/sign-in"
                return $location.path();
            });
        });
        //$urlRouterProvider.otherwise('dashboard');
        $stateProvider

            // =========================================================================
            // SIGN IN
            // =========================================================================
            .state('signin', {
                url: '/sign-in',
                templateUrl: 'views/sign/sign-in.html',
                data: {
                    pageTitle: 'SIGN IN'
                },
                controller: 'SigninCtrl',
                resolve: {
                    deps: ['$ocLazyLoad', 'settings','_user', function($ocLazyLoad, settings) {

                        var cssPath = settings.cssPath, // Create variable css path
                            pluginPath = settings.pluginPath; // Create variable plugin path

                        // you can lazy load files for an existing module
                        return $ocLazyLoad.load(
                            [
                                {
                                    insertBefore: '#load_css_before',
                                    files: [
                                        cssPath+'/pages/sign.css'
                                    ]
                                },
                                {
                                    files: [
                                        pluginPath+'/jquery-validation/dist/jquery.validate.min.js'
                                    ]
                                },
                                {
                                    name: 'blankonApp.account.signin',
                                    files: [
                                        'js/modules/sign/signin.js'
                                    ]
                                }
                            ]
                        );
                    }]
                }
            })
            // =========================================================================
            // Lost Password
            // =========================================================================
            .state('lostPassword', {
                url: '/lost-password',
                templateUrl: 'views/sign/lost-password.html',
                data: {
                    pageTitle: 'LOST PASSWORD'
                },
                controller: 'lostPasswordCtrl',
                resolve: {
                    deps: ['$ocLazyLoad', 'settings', function($ocLazyLoad, settings) {

                        var cssPath = settings.cssPath; // Create variable css path

                        // you can lazy load files for an existing module
                        return $ocLazyLoad.load(
                            [
                                {
                                    insertBefore: '#load_css_before',
                                    files: [
                                        cssPath+'/pages/sign.css'
                                    ]
                                },
                                {
                                    name: 'blankonApp.account.signin',
                                    files: [
                                        'js/modules/sign/signin.js'
                                    ]
                                }
                            ]
                        );
                    }]
                }
            })

            // =========================================================================
            // DASHBOARD
            // =========================================================================
            .state('dashboard', {
                url: '/dashboard',
                templateUrl: 'views/dashboard.html',
                data: {
                    pageTitle: 'DASHBOARD',
                    pageHeader: {
                        icon: 'fa fa-home',
                        title: 'Dashboard',
                        subtitle: 'dashboard & statistics'
                    }
                },
                controller: 'DashboardCtrl',
                resolve: {
                    deps: ['$ocLazyLoad', 'settings', function($ocLazyLoad, settings) {

                        var pluginPath = settings.pluginPath; // Create variable plugin path

                        return $ocLazyLoad.load( // you can lazy load files for an existing module
                            [
                                {
                                    insertBefore: '#load_css_before',
                                    files: [
                                        pluginPath+'/dropzone/downloads/css/dropzone.css',
                                        pluginPath+'/jquery.gritter/css/jquery.gritter.css'
                                    ]
                                },
                                {
                                    name: 'blankonApp.dashboard',
                                    files: [
                                        pluginPath+'/bootstrap-session-timeout/dist/bootstrap-session-timeout.min.js',
                                        pluginPath+'/flot/jquery.flot.pack.js',
                                        pluginPath+'/dropzone/downloads/dropzone.min.js',
                                        pluginPath+'/jquery.gritter/js/jquery.gritter.min.js',
                                        pluginPath+'/skycons-html5/skycons.js',
                                        'js/modules/dashboard.js'
                                    ]
                                }
                            ]
                        );
                    }]
                }
            })

            // =========================================================================
            // ERROR 400
            // =========================================================================
            .state('pageError403', {
                url: '/page-error-403',
                templateUrl: 'views/pages/page-error-403.html',
                data: {
                    pageTitle: 'ERROR 403',
                    pageHeader: {
                        icon: 'fa fa-ban',
                        title: 'Error 403',
                        subtitle: 'access is denied'
                    },
                    breadcrumbs: [
                        {title: 'Pages'},{title: 'Error 403'}
                    ]
                },
                resolve: {
                    deps: ['$ocLazyLoad', 'settings', function($ocLazyLoad, settings) {

                        var cssPath = settings.cssPath; // Create variable css path

                        return $ocLazyLoad.load( // You can lazy load files for an existing module
                            [
                                {
                                    insertBefore: '#load_css_before',
                                    files: [
                                        cssPath+'/pages/error-page.css'
                                    ]
                                }
                            ]
                        );
                    }]
                }
            })

            // =========================================================================
            // ERROR 404
            // =========================================================================
            .state('pageError404', {
                url: '/page-error-404',
                templateUrl: 'views/pages/page-error-404.html',
                data: {
                    pageTitle: 'ERROR 404',
                    pageHeader: {
                        icon: 'fa fa-ban',
                        title: 'Error 404',
                        subtitle: 'page not found'
                    },
                    breadcrumbs: [
                        {title: 'Pages'},{title: 'Error 404'}
                    ]
                },
                resolve: {
                    deps: ['$ocLazyLoad', 'settings', function($ocLazyLoad, settings) {

                        var cssPath = settings.cssPath; // Create variable css path

                        return $ocLazyLoad.load( // You can lazy load files for an existing module
                            [
                                {
                                    insertBefore: '#load_css_before',
                                    files: [
                                        cssPath+'/pages/error-page.css'
                                    ]
                                }
                            ]
                        );
                    }]
                }
            })

            // =========================================================================
            // ERROR 500
            // =========================================================================
            .state('pageError500', {
                url: '/page-error-500',
                templateUrl: 'views/pages/page-error-500.html',
                data: {
                    pageTitle: 'ERROR 500',
                    pageHeader: {
                        icon: 'fa fa-ban',
                        title: 'Error 500',
                        subtitle: 'internal server error'
                    },
                    breadcrumbs: [
                        {title: 'Pages'},{title: 'Error 500'}
                    ]
                },
                resolve: {
                    deps: ['$ocLazyLoad', 'settings', function($ocLazyLoad, settings) {

                        var cssPath = settings.cssPath; // Create variable css path

                        return $ocLazyLoad.load( // You can lazy load files for an existing module
                            [
                                {
                                    insertBefore: '#load_css_before',
                                    files: [
                                        cssPath+'/pages/error-page.css'
                                    ]
                                }
                            ]
                        );
                    }]
                }
            })
             .state('profilePage', {
                url: '/profilePage',
                templateUrl: 'views/forms/form-profile.html',
                data: {
                    pageTitle: 'Profile Settings',
                    pageHeader: {
                        icon: 'fa fa-male',
                        title: 'Profile',
                        subtitle: ''
                    },
                    breadcrumbs: [
                        {title: 'Profile Settings'}
                    ]
                },
                controller: 'profileCtrl',
                resolve: {
                    deps: ['$ocLazyLoad', 'settings', function($ocLazyLoad, settings) {

                        var pluginPath = settings.pluginPath; // Create variable plugin path

                        return $ocLazyLoad.load( // You can lazy load files for an existing module
                            [{
                                    insertBefore: '#load_css_before',
                                    files: [
                                        pluginPath+'/bootstrap-tagsinput/dist/bootstrap-tagsinput.css',
                                        pluginPath+'/jasny-bootstrap-fileinput/css/jasny-bootstrap-fileinput.min.css',
                                        pluginPath+'/chosen_v1.2.0/chosen.min.css'
                                    ]
                                },
                                {
                                    name: 'ui.bootstrap',
                                    files: [
                                        pluginPath+'/angular-bootstrap/ui-bootstrap-tpls.min.js'
                                    ]
                                },
                                {
                                    name: 'blankonApp.forms.element_profilesettings',
                                    files: [
                                        pluginPath+'/bootstrap-tagsinput/dist/bootstrap-tagsinput-angular.min.js',
                                        pluginPath+'/holderjs/holder.js',
                                        pluginPath+'/bootstrap-maxlength/bootstrap-maxlength.min.js',
                                        pluginPath+'/jquery-autosize/jquery.autosize.min.js',
                                        pluginPath+'/chosen_v1.2.0/chosen.jquery.min.js',
                                        pluginPath+'/angular-spinner/dist/angular-spinner.min.js',
                                        'js/modules/forms/element_profilePage.js'
                                    ]
                                }
                            ]
                        );
                    }]
                }
            })

             .state('appsettings', {
                url: '/app-settings',
                templateUrl: 'views/forms/form-appsettings.html',
                data: {
                    pageTitle: 'App Settings',
                    pageHeader: {
                        icon: 'fa fa-list-alt',
                        title: 'App Settings',
                        subtitle: ''
                    },
                    breadcrumbs: [
                        {title: 'App Settings'}
                    ]
                },
                controller: 'AppSettingsCtrl',
                resolve:{
                    auth:function(settings, _request){
                        _request.get('/admin/admin_authentication', {}, function(response){
                            if(response.status === 401)
                                window.location = settings.baseURL+"/account.html#/sign-in"
                        });
                    },
                    deps: ['$ocLazyLoad', 'settings', function($ocLazyLoad, settings) {

                        var pluginPath = settings.pluginPath; // Create variable plugin path

                        return $ocLazyLoad.load( // You can lazy load files for an existing module
                            [
                                {
                                    insertBefore: '#load_css_before',
                                    files: [
                                        pluginPath+'/angular-ui-tree/dist/angular-ui-tree.min.css'
                                    ]
                                },
                                {
                                    name: 'blankonApp.forms.wizard',
                                    files: [
                                        pluginPath+'/jquery-validation/dist/jquery.validate.min.js',
                                        pluginPath+'/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js',
                                        'js/modules/forms/wizard.js'
                                    ]
                                },
                                {
                                    name: 'blankonApp.tables.default',
                                    files: [
                                        pluginPath+'/angular-bootstrap/ui-bootstrap-tpls.min.js',
                                        pluginPath+'/bootstrap-tagsinput/dist/bootstrap-tagsinput-angular.min.js',
                                        pluginPath+'/jasny-bootstrap-fileinput/js/jasny-bootstrap.fileinput.min.js',
                                        pluginPath+'/holderjs/holder.js',
                                        pluginPath+'/underscore/underscore-min.js',
                                        pluginPath+'/bootstrap-maxlength/bootstrap-maxlength.min.js',
                                        pluginPath+'/jquery-autosize/jquery.autosize.min.js',
                                        pluginPath+'/chosen_v1.2.0/chosen.jquery.min.js',
                                        pluginPath+'/angular-chosen-localytics/dist/angular-chosen.min.js',
                                        pluginPath+'/angular-spinner/dist/angular-spinner.min.js',
                                        pluginPath+'/angular-loading-overlay/dist/angular-loading-overlay.js',
                                        pluginPath+'/checklist-model/checklist-model.js',
                                        'js/modules/tables/default_quizapp.js',
                                    ]
                                },
                                {
                                    name: 'blankonApp.forms.element_appsettings',
                                    files: [
                                        pluginPath+'/angular-bootstrap/ui-bootstrap-tpls.min.js',
                                        pluginPath+'/bootstrap-tagsinput/dist/bootstrap-tagsinput-angular.min.js',
                                        pluginPath+'/holderjs/holder.js',
                                        pluginPath+'/bootstrap-maxlength/bootstrap-maxlength.min.js',
                                        pluginPath+'/jquery-autosize/jquery.autosize.min.js',
                                        pluginPath+'/chosen_v1.2.0/chosen.jquery.min.js',
                                        pluginPath+'/angular-spinner/dist/angular-spinner.min.js',
                                        pluginPath+'/checklist-model/checklist-model.js',
                                        pluginPath+'/underscore/underscore-min.js',
                                        pluginPath+'/angular-ui-tree/dist/angular-ui-tree.js',
                                        'js/modules/forms/element_appsettings.js',
                                    ]
                                }
                            ]
                        );
                    }]
                }
            })

            .state('products', {
                url: '/products',
                templateUrl: 'views/tables/table-products.html',
                data: {
                    pageTitle: 'Products',
                    pageHeader: {
                        icon: 'fa fa-table',
                        title: 'Products',
                        subtitle: ''
                    },
                    breadcrumbs: [
                        {title: 'Products'}
                    ]
                },
                controller: 'TableCtrl_Products',
                resolve: {
                    auth:function(settings, _request){
                        _request.get('/admin/admin_authentication', {}, function(response){
                            if(response.status === 401)
                                window.location = settings.baseURL+"/account.html#/sign-in"
                        });
                    },
                    deps: ['$ocLazyLoad','settings', function($ocLazyLoad, settings) {

                        var pluginPath = settings.pluginPath; // Create variable plugin path

                        return $ocLazyLoad.load( // You can lazy load files for an existing module
                            [ 
                                {
                                    insertBefore: '#load_css_before',
                                    files: [
                                        pluginPath+'/bootstrap-tagsinput/dist/bootstrap-tagsinput.css',
                                        pluginPath+'/jasny-bootstrap-fileinput/css/jasny-bootstrap-fileinput.min.css',
                                        pluginPath+'/chosen_v1.2.0/chosen.min.css'
                                    ]
                                },
                                {
                                    name: 'blankonApp.forms.wizard',
                                    files: [
                                        pluginPath+'/jquery-validation/dist/jquery.validate.min.js',
                                        pluginPath+'/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js',
                                        'js/modules/forms/wizard.js'
                                    ]
                                },
                                {
                                    name: 'blankonApp.tables.default',
                                    files: [
                                        pluginPath+'/angular-bootstrap/ui-bootstrap-tpls.min.js',
                                        pluginPath+'/bootstrap-tagsinput/dist/bootstrap-tagsinput-angular.min.js',
                                        pluginPath+'/jasny-bootstrap-fileinput/js/jasny-bootstrap.fileinput.min.js',
                                        pluginPath+'/holderjs/holder.js',
                                        pluginPath+'/underscore/underscore-min.js',
                                        pluginPath+'/bootstrap-maxlength/bootstrap-maxlength.min.js',
                                        pluginPath+'/jquery-autosize/jquery.autosize.min.js',
                                        pluginPath+'/chosen_v1.2.0/chosen.jquery.min.js',
                                        pluginPath+'/angular-chosen-localytics/dist/angular-chosen.min.js',
                                        pluginPath+'/angular-spinner/dist/angular-spinner.min.js',
                                        pluginPath+'/angular-loading-overlay/dist/angular-loading-overlay.js',
                                        pluginPath+'/checklist-model/checklist-model.js',
                                        'js/modules/tables/default_quizapp.js',
                                    ]
                                },
                            ]
                        );
                    },
                    ]
                }
            })
             .state('product-combination', {
                url: '/product-combination',
                templateUrl: 'views/tables/table-productcombination.html',
                data: {
                    pageTitle: 'Products Combination',
                    pageHeader: {
                        icon: 'fa fa-table',
                        title: 'Products Combination',
                        subtitle: ''
                    },
                    breadcrumbs: [
                        {title: 'Products Combination'}
                    ]
                },
                controller: 'TableCtrl_ProductCombination',
                resolve: {
                    auth:function(settings, _request){
                        _request.get('/admin/admin_authentication', {}, function(response){
                            if(response.status === 401)
                                window.location = settings.baseURL+"/account.html#/sign-in"
                        });
                    },
                    deps: ['$ocLazyLoad','settings', function($ocLazyLoad, settings) {

                        var pluginPath = settings.pluginPath; // Create variable plugin path

                        return $ocLazyLoad.load( // You can lazy load files for an existing module
                            [ 
                                {
                                    insertBefore: '#load_css_before',
                                    files: [
                                        pluginPath+'/bootstrap-tagsinput/dist/bootstrap-tagsinput.css',
                                        pluginPath+'/jasny-bootstrap-fileinput/css/jasny-bootstrap-fileinput.min.css',
                                        pluginPath+'/chosen_v1.2.0/chosen.min.css'
                                    ]
                                },
                                {
                                    name: 'blankonApp.tables.productcombination',
                                    files: [
                                        pluginPath+'/angular-bootstrap/ui-bootstrap-tpls.min.js',
                                        pluginPath+'/bootstrap-tagsinput/dist/bootstrap-tagsinput-angular.min.js',
                                        pluginPath+'/jasny-bootstrap-fileinput/js/jasny-bootstrap.fileinput.min.js',
                                        pluginPath+'/holderjs/holder.js',
                                        pluginPath+'/underscore/underscore-min.js',
                                        pluginPath+'/bootstrap-maxlength/bootstrap-maxlength.min.js',
                                        pluginPath+'/jquery-autosize/jquery.autosize.min.js',
                                        pluginPath+'/chosen_v1.2.0/chosen.jquery.min.js',
                                        pluginPath+'/angular-chosen-localytics/dist/angular-chosen.min.js',
                                        pluginPath+'/angular-spinner/dist/angular-spinner.min.js',
                                        pluginPath+'/angular-loading-overlay/dist/angular-loading-overlay.js',
                                        pluginPath+'/checklist-model/checklist-model.js',
                                        'js/modules/tables/default_quizapp.js',
                                        'js/modules/tables/productcombination.js'
                                    ]
                                },
                            ]
                        );
                    },
                    ]
                }
            })

            .state('searchCombinationProducts', {
                url: '/search-combination-products',
                templateUrl: 'views/pages/page-search-productcombination.html',
                data: {
                    pageTitle: 'SEARCH Product Combinations',
                    pageHeader: {
                        icon: 'fa fa-pencil',
                        title: 'Search Products',
                        subtitle: 'Search Products Combination'
                    },
                    breadcrumbs: [
                        {title: 'Search Products'}
                    ]
                },
                controller: 'TableCtrl_ProductCombination',
                resolve: {
                    auth:function(settings, _request){
                        _request.get('/admin/admin_authentication', {}, function(response){
                            if(response.status === 401)
                                window.location = settings.baseURL+"/account.html#/sign-in"
                        });
                    },
                    deps: ['$ocLazyLoad', 'settings', function($ocLazyLoad, settings) {

                        var cssPath = settings.cssPath, // Create variable css path
                            pluginPath = settings.pluginPath; // Create variable plugin path

                        return $ocLazyLoad.load( // you can lazy load files for an existing module
                            [
                                {
                                    insertBefore: '#load_css_before',
                                    files: [
                                        cssPath+'/pages/search.css',
                                        pluginPath+'/chosen_v1.2.0/chosen.min.css'
                                    ]
                                },
                                {
                                    name: 'blankonApp.tables.productcombination',
                                    files: [
                                        pluginPath+'/angular-bootstrap/ui-bootstrap-tpls.min.js',
                                        pluginPath+'/bootstrap-tagsinput/dist/bootstrap-tagsinput-angular.min.js',
                                        pluginPath+'/jasny-bootstrap-fileinput/js/jasny-bootstrap.fileinput.min.js',
                                        pluginPath+'/holderjs/holder.js',
                                        pluginPath+'/underscore/underscore-min.js',
                                        pluginPath+'/bootstrap-maxlength/bootstrap-maxlength.min.js',
                                        pluginPath+'/jquery-autosize/jquery.autosize.min.js',
                                        pluginPath+'/chosen_v1.2.0/chosen.jquery.min.js',
                                        pluginPath+'/angular-chosen-localytics/dist/angular-chosen.min.js',
                                        pluginPath+'/angular-spinner/dist/angular-spinner.min.js',
                                        pluginPath+'/angular-loading-overlay/dist/angular-loading-overlay.js',
                                        pluginPath+'/checklist-model/checklist-model.js',
                                        'js/modules/tables/default_quizapp.js',
                                        'js/modules/tables/productcombination.js'
                                    ]
                                },
                            ]
                        );
                    }]
                }
            })

             .state('userWardrobeCombination', {
                url: '/user-wardrobe-combination',
                templateUrl: 'views/pages/page-wardrobe-usercombination.html',
                data: {
                    pageTitle: 'User Product Combinations',
                    pageHeader: {
                        icon: 'fa fa-pencil',
                        title: 'Search Products',
                        subtitle: 'Search Products Combination'
                    },
                    breadcrumbs: [
                        {title: 'User Wardrobe'}
                    ]
                },
                controller: 'TableCtrl_UserWardrobeProductCombination',
                resolve: {
                    auth:function(settings, _request){
                        _request.get('/admin/admin_authentication', {}, function(response){
                            if(response.status === 401)
                                window.location = settings.baseURL+"/account.html#/sign-in"
                        });
                    },
                    deps: ['$ocLazyLoad', 'settings', function($ocLazyLoad, settings) {

                        var cssPath = settings.cssPath, // Create variable css path
                            pluginPath = settings.pluginPath; // Create variable plugin path

                        return $ocLazyLoad.load( // you can lazy load files for an existing module
                            [
                                {
                                    insertBefore: '#load_css_before',
                                    files: [
                                        cssPath+'/pages/search.css',
                                        pluginPath+'/chosen_v1.2.0/chosen.min.css'
                                    ]
                                },
                                {
                                    name: 'blankonApp.tables.userwardrobeproductcombination',
                                    files: [
                                        pluginPath+'/angular-bootstrap/ui-bootstrap-tpls.min.js',
                                        pluginPath+'/bootstrap-tagsinput/dist/bootstrap-tagsinput-angular.min.js',
                                        pluginPath+'/jasny-bootstrap-fileinput/js/jasny-bootstrap.fileinput.min.js',
                                        pluginPath+'/holderjs/holder.js',
                                        pluginPath+'/underscore/underscore-min.js',
                                        pluginPath+'/bootstrap-maxlength/bootstrap-maxlength.min.js',
                                        pluginPath+'/jquery-autosize/jquery.autosize.min.js',
                                        pluginPath+'/chosen_v1.2.0/chosen.jquery.min.js',
                                        pluginPath+'/angular-chosen-localytics/dist/angular-chosen.min.js',
                                        pluginPath+'/angular-spinner/dist/angular-spinner.min.js',
                                        pluginPath+'/angular-loading-overlay/dist/angular-loading-overlay.js',
                                        pluginPath+'/checklist-model/checklist-model.js',
                                        'js/modules/tables/default_quizapp.js',
                                        'js/modules/tables/userwardrobeproductcombination.js'
                                    ]
                                },
                            ]
                        );
                    }]
                }
            })
             .state('GenerateNewLook', {
                url: '/generate-new-look',
                templateUrl: 'views/pages/generateNewLook.html',
                data: {
                    pageTitle: 'Generate New Look',
                    pageHeader: {
                        icon: 'fa fa-pencil',
                        title: 'Generate New Look',
                        subtitle: ''
                    },
                    breadcrumbs: [
                        {title: 'Generate New Look'}
                    ]
                },
                controller: 'GenerateNewLookCtrl',
                resolve: {
                    auth:function(settings, _request){
                        _request.get('/admin/admin_authentication', {}, function(response){
                            if(response.status === 401)
                                window.location = settings.baseURL+"/account.html#/sign-in"
                        });
                    },
                    deps: ['$ocLazyLoad', 'settings', function($ocLazyLoad, settings) {

                        var cssPath = settings.cssPath,
                            jsPath = settings.jsPath, // Create variable js path
                            pluginPath = settings.pluginPath; // Create variable plugin path

                        return $ocLazyLoad.load( // you can lazy load files for an existing module
                            [
                              
                                // {
                                //     insertBefore: '#load_css_before',
                                //     files: [
                                //         pluginPath+'/flexslider/flexslider.css',
                                      
                                //     ]
                                // },
                             
                                {
                                    name: 'blankonApp.pages.generateNewLook',
                                    files: [
                                        pluginPath+'/angular-bootstrap/ui-bootstrap-tpls.min.js',
                                        pluginPath+'/bootstrap-tagsinput/dist/bootstrap-tagsinput-angular.min.js',
                                        pluginPath+'/jasny-bootstrap-fileinput/js/jasny-bootstrap.fileinput.min.js',
                                        pluginPath+'/holderjs/holder.js',
                                        pluginPath+'/underscore/underscore-min.js',
                                        pluginPath+'/bootstrap-maxlength/bootstrap-maxlength.min.js',
                                        pluginPath+'/jquery-autosize/jquery.autosize.min.js',
                                        pluginPath+'/chosen_v1.2.0/chosen.jquery.min.js',
                                        //   pluginPath+'/jquery-nicescroll/jquery.nicescroll.min.js', 
                                        //   pluginPath+'/jquery-easing-original/jquery.easing.1.3.min.js', 
                                                   
                                        pluginPath+'/angular-chosen-localytics/dist/angular-chosen.min.js',
                                        pluginPath+'/angular-spinner/dist/angular-spinner.min.js',
                                        pluginPath+'/angular-loading-overlay/dist/angular-loading-overlay.js',
                                        pluginPath+'/checklist-model/checklist-model.js',
                                        'js/modules/tables/default_quizapp.js',
                                        'js/modules/pages/generateNewUserLook.js'
                                    ]
                                },
                            ]
                        );
                    }]
                }
            })

    })

    // Init app run
    .run(["$rootScope", "settings", "$state", function($rootScope, settings, $state, $location) {
        $rootScope.$state = $state; // state to be accessed from view
        $rootScope.$location = $location; // location to be accessed from view
        $rootScope.settings = settings; // global settings to be accessed from view
    }]);