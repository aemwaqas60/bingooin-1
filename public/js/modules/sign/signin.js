"use strict";
(function(){


    
    angular.module('blankonApp.account.signin', [])
        .controller('SigninCtrl', function($scope, settings, _request, $log) {

            $scope.signin = function (data){
                $scope._isVisible = false;

                if(!data || !data.username || !data.password){
                    $('#sign-wrapper').addClass('animated shake');
                    setTimeout(function(){$('#sign-wrapper').removeClass('animated shake')}, 1500);
                    return;
                }

                var btn = $('#login-btn');
                btn.html('Checking ...');
                btn.attr('disabled', 'disabled');
               
                _request.post('/signin', data, function (response){
                    response = response.data;
                    if(response.status === 1){
                        btn.removeAttr('disabled');
                        setTimeout(function () {
                            window.location = settings.baseURL+"/index.html#/products";
                        }, 2000);
                        
                    }
                    else{
                        $scope._isVisible = true;
                        btn.text('Sign-In');
                        btn.removeAttr('disabled');
                        $scope.user = {
                            username:'',
                            password:'',
                        } 
                    }
                })
            }

            if($('.sign-in').length && $(window).width() >= 1024){

                /*
                $('.sign-in').validate(
                    {
                        invalidHandler:
                            function() {
                                // Add effect animation css
                                $('#sign-wrapper').addClass('animated shake');
                                setTimeout(function(){$('#sign-wrapper').removeClass('animated shake')}, 1500);

                                // Add effect sound
                                if($('.page-sound').length){
                                    ion.sound.play("light_bulb_breaking");
                                }
                            },
                        rules:{
                            username: {
                                required: true
                            },
                            password: {
                                required: true
                            }
                        },
                        messages: {
                            username: {
                                required: "Just fill anything mr awesome"
                            },
                            password: {
                                required: "Please provide a password"
                            }
                        },
                        highlight:function(element) {
                            $(element).parents('.form-group').addClass('has-error has-feedback');
                        },
                        unhighlight: function(element) {
                            $(element).parents('.form-group').removeClass('has-error');
                        },
                        submitHandler: function(form){
                            var btn = $('#login-btn');
                            btn.html('Checking ...');
                            // Add sounds
                            if($('.page-sound').length){
                                ion.sound.play("cd_tray");
                            }
                            btn.attr('disabled', 'disabled');
                            setTimeout(function() {
                                btn.text('Great MR AWESOME !');
                            }, 2000);
                            btn.removeAttr('disabled');
                            setTimeout(function () {
                                form.submit();
                                //window.location = settings.baseURL+'/production/admin/angularjs/#/dashboard';
                                console.log(window.location);
                            }, 2500);
                        }
                    }
                );*/
            }

            // =========================================================================
            // SETTING HEIGHT
            // =========================================================================
            $('#sign-wrapper').css('min-height',$(window).outerHeight());

            // =========================================================================
            // INPUT SOUNDS
            // =========================================================================
            // Add sounds
            if($('.page-sound').length){
                $('input').on('input', function(){
                    ion.sound.play("tap");
                });
                $('input[type=checkbox]').on('click', function(){
                    ion.sound.play("button_tiny");
                });
            }

        })


        .controller('lostPasswordCtrl', function($scope, settings, _request, $log, $location, $timeout) {


            $scope.submit = function (user){
                 $scope._isVisible

                if(!user || !user.username){
                    $scope.msg = "username field cant be empty";
                    $scope._isVisible = true;
                }
                _request.post('/lostPassword', user, function (response){
                    $scope._isVisible = true;
                    $scope.msg = "Email Successfully Sent.";
                    $timeout(function () {
                        $location.path('/#/sign-in');
                    }, 7000);
                    
                    
                })
            }


        });

})();