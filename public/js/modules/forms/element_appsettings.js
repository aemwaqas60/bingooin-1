"use strict";
(function() {
  angular
    .module("blankonApp.forms.element_appsettings", ["ui.bootstrap", "ui.tree"])

    // =========================================================================
    // CHOSEN SELECT
    // =========================================================================
    .directive("chosenSelect", function() {
      return {
        restrict: "A",
        link: function(scope, element) {
          element.chosen();
        }
      };
    })

    // =========================================================================
    // CHARACTER LIMIT
    // =========================================================================
    .directive("characterLimit", function() {
      return {
        restrict: "A",
        link: function(scope, element) {
          element.maxlength({
            alwaysShow: true,
            threshold: 20,
            warningClass: "label label-success",
            limitReachedClass: "label label-danger",
            separator: " of ",
            preText: "You have ",
            postText: " chars remaining.",
            placement: "centered-right"
          });
        }
      };
    })

    // =========================================================================
    // TEXTAREA AUTOSIZE
    // =========================================================================
    .directive("autosize", function() {
      return {
        restrict: "A",
        link: function(scope, element) {
          element.autosize();
        }
      };
    })
    .controller("AppSettingsCtrl", function(
      $scope,
      $http,
      settings,
      _request,
      $window,
      $modal,
      SharedDataService
    ) {
      var arr = [];
      var nodes_add = [];
      $scope._remove = function(scope) {
        $scope.appsettings.deleteCategories.push(scope.$modelValue.path);
        scope.remove();
      };

      $scope.edit = function(scope) {
        $modal.open({
          templateUrl: "textbox.html", // loads the template
          backdrop: true, // setting backdrop allows us to close the modal window on clicking outside the modal window
          windowClass: "modal", // windowClass - additional CSS class(es) to be added to a modal window template
          // size: 'sm',
          controller: categoryTree_manipulation,
          resolve: {
            obj: function() {
              return {
                _scope: $scope.appsettings,
                scope: scope,
                type: 2
              };
            }
          }
        });
      };

      $scope.toggle = function(scope) {
        scope.toggle();
      };

      $scope.newSubItem = function(scope) {
        $modal.open({
          templateUrl: "textbox.html", // loads the template
          backdrop: true, // setting backdrop allows us to close the modal window on clicking outside the modal window
          windowClass: "modal", // windowClass - additional CSS class(es) to be added to a modal window template
          // size: 'sm',
          controller: categoryTree_manipulation,
          resolve: {
            obj: function() {
              return {
                _scope: $scope.appsettings,
                scope: scope,
                type: 1
              };
            }
          }
        });
      };

      var categories = Array("categories", "common", "top", "bottom", "shoes");
      var steps = Array();
      for (var items in categories) {
        steps.push({
          text: categories[items],
          calss: "fa fa-user"
        });
      }
      $scope.steps = steps;

      _request.get("/admin/appsettings", {}, function(response) {
        console.log(
          "-------- Inside <--GET--> /admin/appsettings response.data--------",
          response.data
        );
        response = response.data;
        $scope.appsettings = {
          size: response.size || [],
          color: response.color || [],
          agegroup: response.agegroup || [],
          season: response.season || [],
          style: response.style || [],

          material: response.material || {
            top: [],
            bottom: [],
            shoes: []
          },
          pattern: response.pattern || {
            top: [],
            bottom: [],
            shoes: []
          },
          brand: response.brand || {
            top: [],
            bottom: [],
            shoes: []
          },
          finishing: response.finishing || {
            top: [],
            bottom: [],
            shoes: []
          },
          id: response._id,
          deleteCategories: [],
          editCategories: []
        };

        _request.get("/admin/categories", {}, function(response) {
          console.log(
            "--------  <--- GET---> Inside /admin/categories  response------- ",
            response
          );
          var maincategories = _.filter(response, function(x) {
            return _.isEmpty(x.path);
          });

          console.log(
            "--------- <----GET---->  Inside /admin/categories  mainCategories------",
            maincategories
          );

          $scope.appsettings.categories = maincategories;
        });
      });

      $scope.form_submit = function(data) {
        var __categories = data.categories;
        var __editCategories = data.editCategories;

        console.log("---categories-----", __categories);

        for (var items in __editCategories) {
          if (__editCategories[items].path.length > 2) {
            for (var _items in __categories) {
              if (
                __categories[_items]._id === __editCategories[items].path[0]
              ) {
                var nodes = __categories[_items].nodes;
                for (var __items in nodes) {
                  if (nodes[__items]._id === __editCategories[items].path[1])
                    nodes[__items].children[
                      _.indexOf(
                        nodes[__items].children,
                        __editCategories[items].old
                      )
                    ] =
                      __editCategories[items].new;
                }
              }
            }
          } else {
            for (var _items in __categories) {
              if (
                __categories[_items]._id === __editCategories[items].path[0]
              ) {
                __categories[_items].children[
                  _.indexOf(
                    __categories[_items].children,
                    __editCategories[items].old
                  )
                ] =
                  __editCategories[items].new;
              }
            }
          }
        }

        data.categories = __categories;
        console.log("-------__categories------", __categories);

        if (data.size && !(data.size instanceof Array))
          data.size = data.size.split(",");

        if (data.color && !(data.color instanceof Array))
          data.color = data.color.split(",");

        if (data.agegroup && !(data.agegroup instanceof Array))
          data.agegroup = data.agegroup.split(",");

        if (data.season && !(data.season instanceof Array))
          data.season = data.season.split(",");

        if (data.style && !(data.style instanceof Array))
          data.style = data.style.split(",");

        for (var items in data.brand) {
          if (data.brand[items] && !(data.brand[items] instanceof Array))
            data.brand[items] = data.brand[items].split(",");
        }
        for (var items in data.material) {
          if (data.material[items] && !(data.material[items] instanceof Array))
            data.material[items] = data.material[items].split(",");
        }
        for (var items in data.pattern) {
          if (data.pattern[items] && !(data.pattern[items] instanceof Array))
            data.pattern[items] = data.pattern[items].split(",");
        }
        for (var items in data.finishing) {
          if (
            data.finishing[items] &&
            !(data.finishing[items] instanceof Array)
          )
            data.finishing[items] = data.finishing[items].split(",");
        }

        if (typeof data === "undefined")
          alert("please complete all the fields before submit");
        var _categories = {
          top: Array(),
          bottom: Array(),
          shoes: Array()
        };
        var obj = {
          size: data.size || [],
          color: data.color || [],
          agegroup: data.agegroup || [],
          season: data.season || [],
          style: data.style || [],
          material: data.material || _categories,
          pattern: data.pattern || _categories,
          brand: data.brand || _categories,
          finishing: data.finishing || _categories,
          _id: data.id,
          categories: data.categories,
          deleteCategories: data.deleteCategories || [],
          editCategories: data.editCategories || []
        };

        //console.log("-----Object:------ ",obj);
        //console.log("-----data.categories:------ ",data.categories);

        _request.post("/admin/appsettings", obj, function(response) {
          console.log(
            "--------<---POST-->  /admin/appsettings   response--------",
            response
          );
          $("#AlertBox").text("Successfully Updated");
          $("#AlertBox").fadeIn();

          _request.get("/admin/products/add", {}, function(res) {
            console.log(
              "-----<--GET-->---- /admin/products/add   response--------",
              res
            );
            SharedDataService.setfieldObj(res.data);
          });

          $window.setTimeout(function() {
            $("#AlertBox").fadeOut(300);
          }, 3000);
        });
      };
    });
})();

function categoryTree_manipulation($scope, $modalInstance, $log, obj) {
  var _scope = obj._scope;
  var scope = obj.scope;
  var type = obj.type;
  var nodeData = scope.$modelValue;
  console.log("-------nodeData:------", nodeData);

  $scope.inputText = "";
  $scope.category_header_title =
    type === 1
      ? "Add New Sub-Category of " + nodeData._id
      : "Edit " + nodeData._id;
  $scope.cancel = function() {
    $modalInstance.dismiss("cancel");
  };

  if (type === 2) {
    $scope.inputText = nodeData._id;
    $scope.func = function(data) {
      nodeData.path[_.indexOf(nodeData.path, nodeData._id)] = data;

      $scope.isShow = false;
      _scope.editCategories.push({
        path: nodeData.path,
        old: nodeData._id,
        new: data
      });
      scope.$modelValue._id = data;
      $modalInstance.dismiss("cancel");
    };
  } else {
    var path = [];

    $scope.func = function(data) {
      if (nodeData.path === null) path = [nodeData._id, data];
      else {
        path = JSON.parse(JSON.stringify(nodeData.path));
        path.push(data);
      }
      nodeData.nodes.push({
        _id: data,
        path: path,
        children: [],
        nodes: []
      });
      nodeData.children === undefined
        ? (nodeData.children = [data])
        : nodeData.children.push(data);
      console.log("newNode:---", nodeData);
      $modalInstance.dismiss("cancel");
    };
  }
}
