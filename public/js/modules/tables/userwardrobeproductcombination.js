"use strict";

(function () {
  angular
    .module("blankonApp.tables.userwardrobeproductcombination", [
      "ui.bootstrap",
      "angularSpinner",
      "angularMoment",
      "localytics.directives",
      "checklist-model",
      "blankonApp.tables.default"
    ])
    .controller(
      "TableCtrl_UserWardrobeProductCombination",
      TableCtrl_ProductCombination
    );
})();

function TableCtrl_ProductCombination(
  $scope,
  $log,
  $http,
  settings,
  _request,
  $modal,
  $window,
  alertFactory,
  SharedDataService
) {
  var productsArr = [];
  var looktypeProductsArr = [];

  $scope.looks = ["Formal Look", "Casual Look", "Business Look"];
  $scope.genders = ["Male", "Female"];

  $scope.casualMaleShirts = [];
  $scope.casualMalePents = [];
  $scope.casualMaleShoes = [];

  $scope.formalMaleShirts = [];
  $scope.formalMalePents = [];
  $scope.formalMaleShoes = [];

  $scope.businessMaleShirts = [];
  $scope.businessMalePents = [];
  $scope.businessMaleShoes = [];

  $scope.casualFemaleShirts = [];
  $scope.casualFemalePents = [];
  $scope.casualFemaleShoes = [];

  $scope.formalFemaleShirts = [];
  $scope.formalFemalePents = [];
  $scope.formalFemaleShoes = [];

  $scope.businessFemaleShirts = [];
  $scope.businessFemalePents = [];
  $scope.businessFemaleShoes = [];

  _request.get("/admin/products", {}, function (data) {
    console.log("==============GET---> /admin/products==============");
    data = data.data;
    console.log("data------->", data);
    $scope.products = data;
    let businessShirts = 0;
    let businessPents = 0;
    let businessShoes = 0;


    let maleCasualShirts = 0;
    let maleCasualPents = 0;
    let maleCasualShoes = 0;


    for (var index in data) {


      console.log("Index:-------->", index);

      // let val = data[index].category[0].split(",");
      // let tempLook = val[0].replace(/[^a-z]/gi, "");
      // let tempClass = val[1].replace(/[^a-z]/gi, "");
  
      var val = data[index].category[0].split(",");
      let tempLook = val[0];
      let tempClass = val[1];

      let tempGender = data[index].gender;


      console.log("TempClass:-------->", tempClass);
      console.log("TempLook:-------->", tempLook);


      if (tempGender === "Male") {
        if (tempClass == "Casual" && tempLook == "Top") {
          $scope.casualMaleShirts.push(data[index]);
          maleCasualShirts++;
        } else if (tempClass == "Casual" && tempLook == "Bottom") {
          maleCasualPents++;
          $scope.casualMalePents.push(data[index]);
        } else if (tempClass == "Casual" && tempLook == "Shoes") {
          maleCasualShoes++;
          $scope.casualMaleShoes.push(data[index]);
        } else if (tempClass == "Formal" && tempLook == "Top") {
          $scope.formalMaleShirts.push(data[index]);
        } else if (tempClass == "Formal" && tempLook == "Bottom") {
          $scope.formalMalePents.push(data[index]);
        } else if (tempClass == "Formal" && tempLook == "Shoes") {
          $scope.formalMaleShoes.push(data[index]);
        } else if (tempClass == "Business" && tempLook == "Top") {
          businessShirts++;
          $scope.businessMaleShirts.push(data[index]);
        } else if (tempClass == "Business" && tempLook == "Bottom") {
          businessPents++;
          $scope.businessMalePents.push(data[index]);
        } else if (tempClass == "Business" && tempLook == "Shoes") {
          businessShoes++;
          $scope.businessMaleShoes.push(data[index]);
        }
      } else if (tempGender === "Female") {
        if (tempClass == "Casual" && tempLook == "Top") {
          $scope.casualFemaleShirts.push(data[index]);
        } else if (tempClass == "Casual" && tempLook == "Bottom") {
          $scope.casualFemalePents.push(data[index]);
        } else if (tempClass == "Casual" && tempLook == "Shoes") {
          $scope.casualFemaleShoes.push(data[index]);
        } else if (tempClass == "Formal" && tempLook == "Top") {
          $scope.formalFemaleShirts.push(data[index]);
        } else if (tempClass == "Formal" && tempLook == "Bottom") {
          $scope.formalFemalePents.push(data[index]);
        } else if (tempClass == "Formal" && tempLook == "Shoes") {
          $scope.formalFemaleShoes.push(data[index]);
        } else if (tempClass == "Business" && tempLook == "Top") {
          $scope.businessFemaleShirts.push(data[index]);
        } else if (tempClass == "Business" && tempLook == "Bottom") {
          $scope.businessFemalePents.push(data[index]);
        } else if (tempClass == "Business" && tempLook == "Shoes") {
          $scope.businessFemaleShoes.push(data[index]);
        }
      }
    }
    console.log("businessShoes Shirts are:", businessShirts);
    console.log("businessPents Pents are:", businessPents);
    console.log("businessShoes Shoes are:", businessShoes);

    console.log("maleCasualShoes Shirts are:", maleCasualShirts);
    console.log("maleCasualPents Pents are:", maleCasualPents);
    console.log("maleCasualShoes Shoes are:", maleCasualShoes);



    //console.log("$scope.products------->", $scope.products);
    /*   var _products = _.groupBy(data, function(x) {
      return x.category[0].toLowerCase();
    }); */
    // $scope.products = _.shuffle(dataManipulation(_products));
    //$scope.products = _.shuffle(dataManipulation(_products));
    // console.log("$scope.products------->",$scope.products);
    //  angular.copy(data, productsArr);
  });

  // if(_.isEmpty(SharedDataService.getfieldObj()) === false){
  //     var settings = SharedDataService.getfieldObj().settings;
  //     $scope.settings = settings;
  //     $scope.settings.brand = _.union(settings.brand['top'], settings.brand['bottom'], settings.brand['shoes']);
  //     $scope.settings.filter = ["Brand", "Color", "Material", "Pattern"];
  // }
  // else{
  //     _request.get('/admin/products/add', {},  function (data){
  //         console.log("==============GET--->Inside /admin/products/add==============");
  //         data = data.data;
  //         SharedDataService.setfieldObj(data);
  //         $scope.settings = data.settings;
  //         console.log(data.settings);
  //         $scope.settings.brand = _.union(data.settings.brand['top'], data.settings.brand['bottom'], data.settings.brand['shoes']);
  //         $scope.settings.filter = ["Brand", "Color", "Material", "Pattern"];
  //     });
  // }

  //  $scope.reset = function (data){

  //     console.log("==============Inside Reset==============");

  //      $scope.settings.selectedlooktype = '';
  //      $scope.settings.selectedgender = '';
  //      $scope.settings.selectedsize = '';
  //      $scope.settings.selectedbrand = '';
  //      $scope.settings.selectedfilter = '';
  //      $scope.products = dataManipulation(_.groupBy(productsArr, function(x){ return x.category[0].toLowerCase() }));
  //  }

  $scope.search = function (data) {
    console.log("==============Inside Search==============");

    var _products = productsArr;
    var selectedlooktype = data.selectedlooktype || "";
    var selectedgender = data.selectedgender || "";
    console.log(
      "selectedlooktype:----->",
      selectedlooktype,
      "selectedgender:---->",
      selectedgender
    );

    if (selectedlooktype === "Casual Look" && selectedgender === "Male") {
      console.log("inside Casual look ");
      $scope.generateMaleCasualLooks();
    } else if (selectedlooktype === "Formal Look" && selectedgender === "Male") {
      $scope.generateMaleFormalLooks();
    } else if (selectedlooktype === "Business Look" && selectedgender === "Male") {
      $scope.generateMaleBussinessLooks();
    } else if (selectedlooktype === "Casual Look" && selectedgender === "Female") {
      console.log("inside Casual look ");
      $scope.generateFemaleCasualLooks();
    } else if (selectedlooktype === "Formal Look" && selectedgender === "Female") {
      $scope.generateFemaleFormalLooks();
    } else if (selectedlooktype === "Business Look" && selectedgender === "Female") {
      $scope.generateFemaleBuinessLooks();
    }



    //  var selectedsize = data.selectedsize || '';
    //  var selectedbrand = data.selectedbrand || '';
    //  var selectedfilter = data.selectedfilter|| '';

    //  if(selectedlooktype === '' && selectedgender === '' && selectedsize === '' && selectedbrand === '' && selectedfilter === ''){
    //      $scope.products = dataManipulation(_.groupBy(_products, function(x){ return x.category[0].toLowerCase() }));
    //      return;
    //  }

    //  _request.get('/admin/product/combination', {selectedlooktype:selectedlooktype, selectedgender:selectedgender, selectedsize:selectedsize, selectedbrand:selectedbrand}, function(response){
    //     console.log("============== GET--> /admin/product/combination===============");

    //     _products = _.groupBy(response, function(x){ return x.category[0].toLowerCase() });
    //     console.log({top:_.isEmpty(_products.top), bottom:_.isEmpty(_products.bottom), shoes:_.isEmpty(_products.shoes)});
    //     $scope.products = _.shuffle(dataManipulation(_products));
    //  })
  };
  $scope.generateMaleCasualLooks = function () {
    console.log("=======generate Male Casual Looks=======");

    $scope.generatedLooks = [];
    $scope.Looksvisible = false;

    for (var index in $scope.casualMaleShirts) {
      for (var index1 in $scope.casualMalePents) {
        for (var index2 in $scope.casualMaleShoes) {
          var looks = {
            shirt: $scope.casualMaleShirts[index].productimage,
            shirtName: $scope.casualMaleShirts[index].name,
            pent: $scope.casualMalePents[index1].productimage,
            pentName: $scope.casualMalePents[index1].name,
            shoe: $scope.casualMaleShoes[index2].productimage,
            shoeName: $scope.casualMaleShoes[index2].name
          }
          $scope.generatedLooks.push(looks);
        }
      }
    }
    if ($scope.generatedLooks.length == 0) {
      $scope.Looksvisible = true;
    }
  }


  $scope.generateMaleFormalLooks = function () {
    console.log("=======generate Male Formal Looks=======");
    $scope.Looksvisible = false;
    $scope.generatedLooks = [];
    for (var index in $scope.formalMaleShirts) {
      for (var index1 in $scope.formalMalePents) {
        for (var index2 in $scope.formalMaleShoes) {
          var looks = {
            shirt: $scope.formalMaleShirts[index].productimage,
            shirtName: $scope.formalMalePents[index].name,
            pent: $scope.formalMalePents[index1].productimage,
            pentName: $scope.formalMalePents[index1].name,
            shoe: $scope.formalMaleShoes[index2].productimage,
            shoeName: $scope.formalMaleShoes[index2].name

          }
          $scope.generatedLooks.push(looks);
        }
      }

    }
    if ($scope.generatedLooks.length == 0) {
      $scope.Looksvisible = true;
    }
  }


  $scope.generateMaleBussinessLooks = function () {
  
  console.log("=======generate Male Business Looks=======");
  
  $scope.Looksvisible = false;
    $scope.generatedLooks = [];
    for (var index in $scope.businessMaleShirts) {
      for (var index1 in $scope.businessMalePents) {
        for (var index2 in $scope.businessMaleShoes) {
          var looks = {
            shirt: $scope.businessMaleShirts[index].productimage,
            shirtName: $scope.businessMaleShirts[index].name,
            pent: $scope.businessMalePents[index1].productimage,
            pentName: $scope.businessMalePents[index1].name,
            shoe: $scope.businessMaleShoes[index2].productimage,
            shoeName: $scope.businessMaleShoes[index2].name

          }
          $scope.generatedLooks.push(looks);
        }
      }

    }
    if ($scope.generatedLooks.length == 0) {
      $scope.Looksvisible = true;
    }

  }



  $scope.generateFemaleCasualLooks = function () {
    console.log("=======generate Male Casual Looks=======");
    
    $scope.Looksvisible = false;
    $scope.generatedLooks = [];
    for (var index in $scope.casualFemaleShirts) {
      for (var index1 in $scope.casualFemalePents) {
        for (var index2 in $scope.casualFemaleShoes) {
          var looks = {
            shirt: $scope.casualFemaleShirts[index].productimage,
            shirtName: $scope.casualFemaleShirts[index].name,
            pent: $scope.casualFemalePents[index1].productimage,
            pentName: $scope.casualFemalePents[index1].name,
            shoe: $scope.casualFemaleShoes[index2].productimage,
            shoeName: $scope.casualFemaleShoes[index2].name
          }
          $scope.generatedLooks.push(looks);
        }
      }
    }
    if ($scope.generatedLooks.length == 0) {
      $scope.Looksvisible = true;
    }
  }


  $scope.generateFemaleFormalLooks = function () {
    console.log("=======generate Male Formal Looks=======");

    $scope.Looksvisible = false;
    $scope.generatedLooks = [];
    for (var index in $scope.formalFemaleShirts) {
      for (var index1 in $scope.formalFemalePents) {
        for (var index2 in $scope.formalFemaleShoes) {
          var looks = {
            shirt: $scope.formalFemaleShirts[index].productimage,
            shirtName: $scope.formalFemaleShirts[index].name,
            pent: $scope.formalFemalePents[index1].productimage,
            pentName: $scope.formalFemalePents[index1].name,
            shoe: $scope.formalFemaleShoes[index2].productimage,
            shoeName: $scope.formalFemaleShoes[index2].name

          }
          $scope.generatedLooks.push(looks);
        }
      }

    }
    if ($scope.generatedLooks.length == 0) {
      $scope.Looksvisible = true;
    }

  }


  $scope.generateFemaleBuinessLooks = function () {
    console.log("=======generate Male Business Looks=======");
    $scope.Looksvisible = false;
    $scope.generatedLooks = [];
    for (var index in $scope.businessFemaleShirts) {
      for (var index1 in $scope.businessFemalePents) {
        for (var index2 in $scope.businessFemaleShoes) {
          var looks = {
            shirt: $scope.businessFemaleShirts[index].productimage,
            shirtName: $scope.businessFemaleShirts[index].name,
            pent: $scope.businessFemalePents[index1].productimage,
            pentName: $scope.businessFemalePents[index1].name,
            shoe: $scope.businessFemaleShoes[index2].productimage,
            shoeName: $scope.businessFemaleShoes[index2].name

          }
          $scope.generatedLooks.push(looks);
        }
      }

    }
    if ($scope.generatedLooks.length == 0) {
      $scope.Looksvisible = true;
    }

  }



}

// function dataManipulation(_products){
//     if(_.isEmpty(_products.top) === false && _.isEmpty(_products.bottom)===false && _.isEmpty(_products.shoes)===false)
//         _products = cartesianProductOf(_products.top||[], _products.bottom||[], _products.shoes||[]);

//     else if(_.isEmpty(_products.top) === false && _.isEmpty(_products.bottom) === true && _.isEmpty(_products.shoes)===true){
//         var __products = _products.top;
//         var temp = Array();
//         for(var items in __products){
//             temp.push(Array(__products[items], {}, {}));
//         }
//         _products = temp;
//     }
//     else if(_.isEmpty(_products.top) === true && _.isEmpty(_products.bottom) === false && _.isEmpty(_products.shoes)=== true){
//         var __products = _products.bottom;
//         var temp = Array();
//         for(var items in __products){
//             temp.push(Array({}, __products[items], {}));
//         }
//         _products = temp;
//     }
//     else if(_.isEmpty(_products.top) === true && _.isEmpty(_products.bottom) === true && _.isEmpty(_products.shoes)===false){
//         var __products = _products.shoes;
//         var temp = Array();
//         for(var items in __products){
//             temp.push(Array({}, {}, __products[items]));
//         }
//         _products = temp;
//     }

//     else if(_.isEmpty(_products.top)){
//         _products = cartesianProductOf(_products.bottom||[], _products.shoes||[]);
//         _.map(_products, function(x){ return x.splice(0, 0, {})});
//     }
//     else if(_.isEmpty(_products.bottom)){
//         _products = cartesianProductOf(_products.top||[], _products.shoes||[]);
//         _.map(_products, function(x){ return x.splice(1, 0, {})});
//     }
//     else if(_.isEmpty(_products.shoes)){
//         _products = cartesianProductOf(_products.top||[], _products.bottom||[]);
//         _.map(_products, function(x){ return x.splice(2, 0, {})});
//     }else{
//         console.log('nothing');
//     }

//     return _products;
// }

// function cartesianProductOf() {
//     return _.reduce(arguments, function(a, b) {
//         return _.flatten(_.map(a, function(x) {
//             return _.map(b, function(y) {
//                 return x.concat([y]);
//             });
//         }), true);
//     }, [ [] ]);
// };