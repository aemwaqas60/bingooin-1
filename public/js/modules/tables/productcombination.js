"use strict";
(function(){



    angular.module('blankonApp.tables.productcombination', ['ui.bootstrap', 'angularSpinner','angularMoment', 'localytics.directives', 'checklist-model', 'blankonApp.tables.default'])
        .controller('TableCtrl_ProductCombination', TableCtrl_ProductCombination)
        ;
})();

function TableCtrl_ProductCombination($scope, $log, $http, settings, _request, $modal, $window, alertFactory, SharedDataService){
    var productsArr = [];
    var looktypeProductsArr = [];
    _request.get('/admin/products', {},  function (data){
        data = data.data;

        console.log(data)
        var _products = _.groupBy(data, function(x){ return x.category[0].toLowerCase() });
        $scope.products = _.shuffle(dataManipulation(_products));
        angular.copy(data, productsArr);
        
    });

    if(_.isEmpty(SharedDataService.getfieldObj()) === false){
        // var settings = SharedDataService.getfieldObj().settings;
        // $scope.settings = settings;
        // $scope.settings.brand = _.union(settings.brand['top'], settings.brand['bottom'], settings.brand['shoes']);
        // $scope.settings.filter = ["Brand", "Color", "Material", "Pattern"];
    }
    else{
        _request.get('/admin/products/add', {},  function (data){
            data = data.data;
            SharedDataService.setfieldObj(data);
            $scope.settings = data.settings;
            console.log(data.settings);
            $scope.settings.brand = _.union(data.settings.brand['top'], data.settings.brand['bottom'], data.settings.brand['shoes']);
            $scope.settings.filter = ["Brand", "Color", "Material", "Pattern"];
        });
    }
    
     $scope.changeLooktype = function(data){
         return ;
         //console.log(products);
         var _products = _.filter(productsArr, function(x){ return _.find(x.looktype, function(exist){ return exist == data; })});
         _products = _.groupBy(_products, function(x){ return x.category[0].toLowerCase() });

         if(_.isEmpty(_products.top) === false && _.isEmpty(_products.bottom)===false && _.isEmpty(_products.shoes)===false)
            _products = cartesianProductOf(_products.top||[], _products.bottom||[], _products.shoes||[]);
            
        else if(_.isEmpty(_products.top)){
            _products = cartesianProductOf(_products.bottom||[], _products.shoes||[]);
            _.map(_products, function(x){ return x.splice(0, 0, {})});
        }
        else if(_.isEmpty(_products.bottom)){
            _products = cartesianProductOf(_products.top||[], _products.shoes||[]);
            _.map(_products, function(x){ return x.splice(1, 0, {})});            
        }
        else if(_.isEmpty(_products.shoes)){
            _products = cartesianProductOf(_products.top||[], _products.bottom||[]);
            _.map(_products, function(x){ return x.splice(2, 0, {})});            
        }  
        angular.copy(_products, looktypeProductsArr);
        $scope.products = _products;
     }
     $scope.changeGender = function (data){
         return ;
         console.log(data)
         var _products = angular.copy(looktypeProductsArr , _products);
         console.log(_products);

         for(var items in _products){
            _products[items] = _.map(_products[items], function (y){
                if(y.gender !== undefined && y.gender == data)
                    return y;
                else
                    return {};
            })
         }
         $scope.products = _products;

     }

     $scope.reset = function (data){
         $scope.settings.selectedlooktype = '';
         $scope.settings.selectedgender = '';
         $scope.settings.selectedsize = '';
         $scope.settings.selectedbrand = '';
         $scope.settings.selectedfilter = '';
         $scope.products = dataManipulation(_.groupBy(productsArr, function(x){ return x.category[0].toLowerCase() }));
     }

     $scope.search = function (data){
         var _products = productsArr;
         var selectedlooktype = data.selectedlooktype || '';
         var selectedgender = data.selectedgender || '';
         var selectedsize = data.selectedsize || '';
         var selectedbrand = data.selectedbrand || '';
         var selectedfilter = data.selectedfilter|| '';

         if(selectedlooktype === '' && selectedgender === '' && selectedsize === '' && selectedbrand === '' && selectedfilter === ''){
             $scope.products = dataManipulation(_.groupBy(_products, function(x){ return x.category[0].toLowerCase() }));
             return;
         }


         _request.get('/admin/product/combination', {selectedlooktype:selectedlooktype, selectedgender:selectedgender, selectedsize:selectedsize, selectedbrand:selectedbrand}, function(response){

            _products = _.groupBy(response, function(x){ return x.category[0].toLowerCase() });
            console.log({top:_.isEmpty(_products.top), bottom:_.isEmpty(_products.bottom), shoes:_.isEmpty(_products.shoes)})
            $scope.products = _.shuffle(dataManipulation(_products));
         })
        

     }

    
}


function dataManipulation(_products){
    if(_.isEmpty(_products.top) === false && _.isEmpty(_products.bottom)===false && _.isEmpty(_products.shoes)===false)
        _products = cartesianProductOf(_products.top||[], _products.bottom||[], _products.shoes||[]);

    else if(_.isEmpty(_products.top) === false && _.isEmpty(_products.bottom) === true && _.isEmpty(_products.shoes)===true){
        var __products = _products.top;
        var temp = Array();
        for(var items in __products){
            temp.push(Array(__products[items], {}, {}));
        }
        _products = temp;
    }
    else if(_.isEmpty(_products.top) === true && _.isEmpty(_products.bottom) === false && _.isEmpty(_products.shoes)=== true){
        var __products = _products.bottom;
        var temp = Array();
        for(var items in __products){
            temp.push(Array({}, __products[items], {}));
        }
        _products = temp;
    }
    else if(_.isEmpty(_products.top) === true && _.isEmpty(_products.bottom) === true && _.isEmpty(_products.shoes)===false){
        var __products = _products.shoes;
        var temp = Array();
        for(var items in __products){
            temp.push(Array({}, {}, __products[items]));
        }
        _products = temp;
    }
    
    else if(_.isEmpty(_products.top)){
        _products = cartesianProductOf(_products.bottom||[], _products.shoes||[]);
        _.map(_products, function(x){ return x.splice(0, 0, {})});
    }
    else if(_.isEmpty(_products.bottom)){
        _products = cartesianProductOf(_products.top||[], _products.shoes||[]);
        _.map(_products, function(x){ return x.splice(1, 0, {})});            
    }
    else if(_.isEmpty(_products.shoes)){
        _products = cartesianProductOf(_products.top||[], _products.bottom||[]);
        _.map(_products, function(x){ return x.splice(2, 0, {})}); 
    }else{
        console.log('nothing')
    }

    return _products;
}



function cartesianProductOf() {
    return _.reduce(arguments, function(a, b) {
        return _.flatten(_.map(a, function(x) {
            return _.map(b, function(y) {
                return x.concat([y]);
            });
        }), true);
    }, [ [] ]);
};
