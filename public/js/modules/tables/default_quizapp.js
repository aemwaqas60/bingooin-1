"use strict";
(function () {
  angular
    .module("blankonApp.tables.default", [
      "ui.bootstrap",
      "angularSpinner",
      "angularMoment",
      "localytics.directives",
      "checklist-model"
    ])
    .constant("DATETIME_FORMAT", "DD MMM YY | hh:mm A")
    .directive("fileModel", fileModel)

    .controller("TableCtrl_Products", TableCtrl_Products)
    .controller("CtrlAddProduct", CtrlAddProduct)
    .controller("CtrlEditProduct", CtrlEditProduct)
    .controller("CtrlDeleteProduct", CtrlDeleteProduct)
    .service("SharedDataService", SharedDataService);
})();

function SharedDataService() {
  var products = [];
  var fieldsObj = {};
  this.setProducts = function (data) {
    products = data;
  };
  this.getProducts = function () {
    return products;
  };
  this.setfieldObj = function (data) {
    fieldsObj = data;
  };
  this.getfieldObj = function () {
    return fieldsObj;
  };
}

function fileModel($parse) {
  return {
    restrict: "A",
    link: function (scope, element, attrs) {
      var model = $parse(attrs.fileModel);
      var modelSetter = model.assign;
      element.bind("change", function () {
        scope.$apply(function () {
          var re = /(\.jpg|\.jpeg|\.png)$/i;
          if (element[0].files[0]) {
            if (!re.exec(element[0].files[0].name)) {
              alert("File extension not supported!");
              return;
            }
          }
          console.log(element[0].files[0]);
          modelSetter(scope, element[0].files[0]);
        });
      });
    }
  };
}

function TableCtrl_Products(
  $scope,
  $log,
  $http,
  settings,
  _request,
  $modal,
  $window,
  DATETIME_FORMAT,
  alertFactory,
  SharedDataService
) {
  _request.get("/admin/products", {}, function (response) {
    var data = response.data;
    $scope.products = data;
    console.log("$scope.products", $scope.products);
    SharedDataService.setProducts(data);
  });

  if (_.isEmpty(SharedDataService.getfieldObj()) === true) {
    _request.get("/admin/products/add", {}, function (data) {
      data = data.data;
      // console.log("file setting data: ",data);
      SharedDataService.setfieldObj(data);
    });
  }

  $scope.download = function (data) {
    window.open("/admin/products/download/" + data._id);
  };
  $scope.delete = function (data) {
    console.log("insdie delte", data);
    // pending...
    $modal.open({
      templateUrl: "productDelete.html", // loads the template
      backdrop: true, // setting backdrop allows us to close the modal window on clicking outside the modal window
      windowClass: "modal", // windowClass - additional CSS class(es) to be added to a modal window template
      size: "lg",
      controller: CtrlDeleteProduct,
      resolve: {
        deleteObj: function () {
          return data;
        }
      }
    });
  };

  $scope.edit = function (obj) {
    $modal.open({
      templateUrl: "productEditForm.html", // loads the template
      backdrop: true, // setting backdrop allows us to close the modal window on clicking outside the modal window
      windowClassss: "modal", // windowClass - additional CSS class(es) to be added to a modal window template
      // size: 'sm',
      controller: CtrlEditProduct,
      resolve: {
        editObj: function () {
          return obj;
        }
      }
    });
  };


  $scope.copyProduct = function (data) {

    
  console.log("copyProduct controller");

  let category = new Array();


  console.log("data: ", data);


 // console.log(filter_top_brand);

  let filter_top_brand = data.filter_top_brand;
  let filter_top_material = data.filter_top_material;
  let filter_top_pattern = data.filter_top_pattern;
  let filter_top_finishing = data.filter_top_finishing;
  let filter_top_color = data.filter_top_color;
  let filter_top_size = data.filter_top_size;
  let filter_top_agegroup = data.filter_top_agegroup;
  let filter_top_season = data.filter_top_season;
  let filter_top_style = data.filter_top_style;

  let filter_bottom_brand = data.filter_bottom_brand;
  let filter_bottom_material = data.filter_bottom_material;
  let filter_bottom_pattern = data.filter_bottom_pattern;
  let filter_bottom_finishing = data.filter_bottom_finishing;
  let filter_bottom_color = data.filter_bottom_color;
  let filter_bottom_size = data.filter_bottom_size;
  let filter_bottom_agegroup = data.filter_bottom_agegroup;
  let filter_bottom_season = data.filter_bottom_season;
  let filter_bottom_style = data.filter_bottom_style;

  let filter_shoes_brand = data.filter_shoes_brand;
  let filter_shoes_material = data.filter_shoes_material;
  let filter_shoes_pattern = data.filter_shoes_pattern;
  let filter_shoes_finishing = data.filter_shoes_finishing;
  let filter_shoes_color = data.filter_shoes_color;
  let filter_shoes_size = data.filter_shoes_size;
  let filter_shoes_agegroup = data.filter_shoes_agegroup;
  let filter_shoes_season = data.filter_shoes_season;
  let filter_shoes_style = data.filter_shoes_style;

  /* if (data.categorylevel1) category.push(data.categorylevel1);
  if (data.categorylevel2) category.push(data.categorylevel2); */
  
  // if (data.categorylevel3) category.push(data.categorylevel3);
 
  var fd = new FormData();
  fd.append("name", data.name+"-copy");
  fd.append("product_id", data.product_id);
  fd.append("link", data.link);
  fd.append("brand", data.brand);
  fd.append("material", data.material);
  fd.append("pattern", data.pattern);
  fd.append("finishing", data.finishing);
  fd.append("gender", data.gender);
  fd.append("agegroup", data.agegroup);
  fd.append("color", data.color);
  fd.append("season", data.season);
  fd.append("style", data.style);
  fd.append("size", data.size);
  fd.append("productimage", data.productimage);
  fd.append("category", data.category[0]);
  fd.append("active", data.active || 1);
  fd.append("file", $scope.myFile);


  fd.append("filter_top_brand", filter_top_brand);
  fd.append("filter_top_material", filter_top_material);
  fd.append("filter_top_pattern", filter_top_pattern);
  fd.append("filter_top_finishing", filter_top_finishing);
  fd.append("filter_top_color", filter_top_color);
  fd.append("filter_top_size", filter_top_size);
  fd.append("filter_top_agegroup", filter_top_agegroup);
  fd.append("filter_top_season", filter_top_season);
  fd.append("filter_top_style", filter_top_style);

  fd.append("filter_bottom_brand", filter_bottom_brand);
  fd.append("filter_bottom_material", filter_bottom_material);
  fd.append("filter_bottom_pattern", filter_bottom_pattern);
  fd.append("filter_bottom_finishing", filter_bottom_finishing);
  fd.append("filter_bottom_color", filter_bottom_color);
  fd.append("filter_bottom_size", filter_bottom_size);
  fd.append("filter_bottom_agegroup", filter_bottom_agegroup);
  fd.append("filter_bottom_season", filter_bottom_season);
  fd.append("filter_bottom_style", filter_bottom_style);

  fd.append("filter_shoes_brand", filter_shoes_brand);
  fd.append("filter_shoes_material", filter_shoes_material);
  fd.append("filter_shoes_pattern", filter_shoes_pattern);
  fd.append("filter_shoes_finishing", filter_shoes_finishing);
  fd.append("filter_shoes_color", filter_shoes_color);
  fd.append("filter_shoes_size", filter_shoes_size);
  fd.append("filter_shoes_agegroup", filter_shoes_agegroup);
  fd.append("filter_shoes_season", filter_shoes_season);
  fd.append("filter_shoes_style", filter_shoes_style); 


  // console.log("filter_shoes_style", filter_shoes_style);
  //console.log("filter_shoes_brand", filter_shoes_brand);

   //console.log("fd:----->", fd);
  // console.log("category insdie submit:", category);

  _request.postFile("/admin/copyProduct", fd, function (data) {
    data = data.data;
    if (!_.isEmpty(data.images))
      data.image = data.images[0]._id + data.images[0].extn;

    SharedDataService.getProducts().splice(0, 0, data);
    alertFactory._alert(" Product Successfully copied");
  });
  
   
  };

  $scope.add = function (data) {
    $modal.open({
      templateUrl: "productForm.html", // loads the template
      backdrop: true, // setting backdrop allows us to close the modal window on clicking outside the modal window
      windowClass: "modal", // windowClass - additional CSS class(es) to be added to a modal window template
      size: "lg",
      controller: CtrlAddProduct
    });
  };
}

function CtrlDeleteProduct(
  $scope,
  $modalInstance,
  $log,
  alertFactory,
  _request,
  deleteObj,
  SharedDataService
) {
  let obj = deleteObj;
  //console.log("obj in contoller",obj);

  $scope.dissmissModel = function () {
    $modalInstance.dismiss("cancel");
  };

  $scope.deleteProduct = function () {
    //console.log("Delete product");
    //console.log("deleteObj",deleteObj);

    _request.delete("/admin/products/" + obj._id, "", function (_data) {
      console.log(_data);
      if (_data.data.status === 1) {
        var _products = SharedDataService.getProducts();
        for (var items in _products) {
          if (_products[items]._id === obj._id) {
            _products.splice(items, 1);
            $modalInstance.dismiss("cancel");
            alertFactory._alert("SuccessFully Deleted");
          }
        }
      }
    });
  };
}

function CtrlEditProduct(
  $scope,
  $modalInstance,
  $log,
  alertFactory,
  _request,
  editObj,
  SharedDataService
) {
  $scope.title = "Edit";
  $scope.imageButtons = true;
  var _categories = [];
  var obj = editObj;

  // console.log("edit obj: ", obj);

  var data = SharedDataService.getfieldObj();
  //var categories = data.categories;
  let settings = data.settings;
  console.log("settings:", settings);

  let categoriesLevel1 = ["Top", "Bottom", "Shoes"];
  let categoriesLevel2 = ["Casual", "Formal", "Business"];

  let selectedCategories = obj.category[0].split(",");
  let cat1 = selectedCategories[0];
  let cat2 = selectedCategories[1];
  $scope.steps = new Array();

  $scope.imgsrc = obj.image;

  // Initializing previous selected compatibilites
  $scope.filter = {
    Top: {
      brand: obj.filter_top_brand,
      material: obj.filter_top_material,
      pattern: obj.filter_top_pattern,
      finishing: obj.filter_top_finishing,
      color: obj.filter_top_color,
      size: obj.filter_top_size,
      agegroup: obj.filter_top_agegroup,
      season: obj.filter_top_season,
      style: obj.filter_top_style
    },

    Bottom: {
      brand: obj.filter_bottom_brand,
      material: obj.filter_bottom_material,
      pattern: obj.filter_bottom_pattern,
      finishing: obj.filter_bottom_finishing,
      color: obj.filter_bottom_color,
      size: obj.filter_bottom_size,
      agegroup: obj.filter_bottom_agegroup,
      season: obj.filter_bottom_season,
      style: obj.filter_bottom_style
    },

    Shoes: {
      brand: obj.filter_shoes_brand,
      material: obj.filter_shoes_material,
      pattern: obj.filter_shoes_pattern,
      finishing: obj.filter_shoes_finishing,
      color: obj.filter_shoes_color,
      size: obj.filter_shoes_size,
      agegroup: obj.filter_shoes_agegroup,
      season: obj.filter_shoes_season,
      style: obj.filter_shoes_style
    }
  };


  console.log("$scope.filter", $scope.filter);

  $scope._item = {
    _id: obj._id,
    images: obj.images,
    name: obj.name,
    link:obj.link,
    product_id:obj.product_id,
    gender: ["Unisex", "Male", "Female"],
    selectedGender: obj.gender,
    categoryLevel1: categoriesLevel1,
    selectedCategoryLevel1: cat1,
    categoryLevel2: categoriesLevel2,
    selectedCategoryLevel2: cat2,
    brand: settings.brand[cat1.toLowerCase()][0].split(","),
    selectedBrand: obj.brand,
    material: settings.material[cat1.toLowerCase()][0].split(","),
    selectedMaterial: obj.material[0].split(","),
    pattern: settings.pattern[cat1.toLowerCase()][0].split(","),
    selectedPattern: obj.pattern,
    finishing: settings.finishing[cat1.toLowerCase()][0].split(","),
    selectedFinishing: obj.finishing,
    agegroup: settings.agegroup[0].split(","),
    selectedAgegroup: obj.agegroup[0].split(","),
    color: settings.color[0].split(","),
    selectedColor: obj.color[0].split(","),
    size: settings.size[0].split(","),
    selectedSize: obj.size[0].split(","),
    season: settings.season[0].split(","),
    selectedSeason: obj.season[0].split(","),
    style: settings.style[0].split(","),
    selectedStyle: obj.style
  };

  if (cat1 == "Top") {
    $scope.steps = ["Bottom", "Shoes"];

    $scope.compatibleBrands = [
      settings.brand.bottom[0].split(",").map(function (item) {
        return item.trim();
      }),
      settings.brand.bottom[0].split(",").map(function (item) {
        return item.trim();
      })
    ];

    $scope.compatibleMaterials = [
      settings.material.bottom[0].split(",").map(function (item) {
        return item.trim();
      }),
      settings.material.shoes[0].split(",").map(function (item) {
        return item.trim();
      })
    ];
    $scope.compatiblePatterns = [
      settings.pattern.bottom[0].split(",").map(function (item) {
        return item.trim();
      }),
      settings.pattern.shoes[0].split(",").map(function (item) {
        return item.trim();
      })
    ];
    $scope.compatibleFinishings = [
      settings.finishing.bottom[0].split(",").map(function (item) {
        return item.trim();
      }),
      settings.finishing.shoes[0].split(",").map(function (item) {
        return item.trim();
      })
    ];
  } else if (cat1 == "Bottom") {
    $scope.steps = ["Top", "Shoes"];

    $scope.compatibleBrands = [
      settings.brand.top[0].split(",").map(function (item) {
        return item.trim();
      }),
      settings.brand.shoes[0].split(",").map(function (item) {
        return item.trim();
      })
    ];

    $scope.compatibleMaterials = [
      settings.material.top[0].split(",").map(function (item) {
        return item.trim();
      }),
      settings.material.shoes[0].split(",").map(function (item) {
        return item.trim();
      })
    ];
    $scope.compatiblePatterns = [
      settings.pattern.top[0].split(",").map(function (item) {
        return item.trim();
      }),
      settings.pattern.shoes[0].split(",").map(function (item) {
        return item.trim();
      })
    ];
    $scope.compatibleFinishings = [
      settings.finishing.top[0].split(",").map(function (item) {
        return item.trim();
      }),
      settings.finishing.shoes[0].split(",").map(function (item) {
        return item.trim();
      })
    ];
  } else if (cat1 == "Shoes") {
    $scope.steps = ["Top", "Bottom"];

    $scope.compatibleBrands = [
      settings.brand.top[0].split(",").map(function (item) {
        return item.trim();
      }),
      settings.brand.bottom[0].split(",").map(function (item) {
        return item.trim();
      })
    ];

    $scope.compatibleMaterials = [
      settings.material.top[0].split(",").map(function (item) {
        return item.trim();
      }),
      settings.material.bottom[0].split(",").map(function (item) {
        return item.trim();
      })
    ];
    $scope.compatiblePatterns = [
      settings.pattern.top[0].split(",").map(function (item) {
        return item.trim();
      }),
      settings.pattern.bottom[0].split(",").map(function (item) {
        return item.trim();
      })
    ];
    $scope.compatibleFinishings = [
      settings.finishing.top[0].split(",").map(function (item) {
        return item.trim();
      }),
      settings.finishing.bottom[0].split(",").map(function (item) {
        return item.trim();
      })
    ];
  }

  $scope.updateCategoryLevel_Brand_Material_Pattern_Finishing = function (data) {
    console.log(
      "------Data Inside updateCategoryLevel_Brand_Material_Pattern_Finishing:----------",
      data
    );

    //console.log("Brand[0]",$scope._item.brand.bottom[0].split(","));
    if (data == "Top") {
      //   console.log("------Inside Top:--------");

      //changing the tab 2 and 3 headers according to selected category level 1
      $scope.steps = ["Bottom", "Shoes"];

      // changing the tab 1 data according to selected category level 1
      $scope._item.brand = settings.brand.top[0].split(",");
      $scope._item.material = settings.material.top[0].split(",");
      $scope._item.pattern = settings.pattern.top[0].split(",");
      $scope._item.finishing = settings.finishing.top[0].split(",");

      // console.log("$scope.showBrand:", $scope.showBrand);

      // changing the tab 2 and 3 data according to selected category level 1
      $scope.compatibleBrands = [
        settings.brand.bottom[0].split(","),
        settings.brand.shoes[0].split(",")
      ];

      $scope.compatibleMaterials = [
        settings.material.bottom[0].split(","),
        settings.material.shoes[0].split(",")
      ];
      $scope.compatiblePatterns = [
        settings.pattern.bottom[0].split(","),
        settings.pattern.shoes[0].split(",")
      ];
      $scope.compatibleFinishings = [
        settings.finishing.bottom[0].split(","),
        settings.finishing.shoes[0].split(",")
      ];
    } else if (data == "Bottom") {
      // console.log("------Inside bottom:--------");

      //changing the tab 2 and 3 headers according to selected category level 1
      $scope.steps = ["Top", "Shoes"];

      //changing the tab 1 data according to selected category level

      $scope._item.brand = settings.brand.bottom[0].split(",");
      $scope._item.material = settings.material.bottom[0].split(",");
      $scope._item.pattern = settings.pattern.bottom[0].split(",");
      $scope._item.finishing = settings.finishing.bottom[0].split(",");

      // changing the tab 2 and 3 data according to selected category level 1
      $scope.compatibleBrands = [
        settings.brand.top[0].split(","),
        settings.brand.shoes[0].split(",")
      ];

      $scope.compatibleMaterials = [
        settings.material.top[0].split(","),
        settings.material.shoes[0].split(",")
      ];
      $scope.compatiblePatterns = [
        settings.pattern.top[0].split(","),
        settings.pattern.shoes[0].split(",")
      ];
      $scope.compatibleFinishings = [
        settings.finishing.top[0].split(","),
        settings.finishing.shoes[0].split(",")
      ];
    } else if (data == "Shoes") {
      // console.log("------Inside shoes:--------");

      //changing the tab 2 and 3 headers according to selected category level 1
      $scope.steps = ["Top", "Bottom"];

      //changing the tab 1 data according to selected category level 1
      $scope._item.brand = settings.brand.shoes[0].split(",");
      $scope._item.material = settings.material.shoes[0].split(",");
      $scope._item.pattern = settings.pattern.shoes[0].split(",");
      $scope._item.finishing = settings.finishing.shoes[0].split(",");

      // changing the tab 2 and 3 data according to selected category level 1
      $scope.compatibleBrands = [
        settings.brand.top[0].split(","),
        settings.brand.bottom[0].split(",")
      ];

      $scope.compatibleMaterials = [
        settings.material.top[0].split(","),
        settings.material.bottom[0].split(",")
      ];
      $scope.compatiblePatterns = [
        settings.pattern.top[0].split(","),
        settings.pattern.bottom[0].split(",")
      ];
      $scope.compatibleFinishings = [
        settings.finishing.top[0].split(","),
        settings.finishing.bottom[0].split(",")
      ];
    }
  };


  $scope.checkAll = function (item, category) {

    //console.log("myCheckbox",check);
    console.log("myCheckbox", category);

    switch (item) {
      case "brand":
        cat1=category.toLowerCase();
        $scope.filter[category].brand = settings.brand[cat1][0].split(",");
        // console.log(settings.brand.top[0].split(","));
        break;
      case "material":

      cat1=category.toLowerCase();
      $scope.filter[category].material = settings.material[cat1][0].split(",");

        break;
      case "pattern":
      cat1=category.toLowerCase();
      $scope.filter[category].pattern = settings.pattern[cat1][0].split(",");

        break;
      case "finishing":

      cat1=category.toLowerCase();
        $scope.filter[category].finishing = settings.finishing[cat1][0].split(",");
        break;

      case "color":
      $scope.filter[category].color = settings.color[0].split(",");

        break;
      case "ageGroup":
      $scope.filter[category].agegroup = settings.agegroup[0].split(",");

        break;
      case "size":
      $scope.filter[category].size = settings.size[0].split(",");
      break;

      case "season":
      $scope.filter[category].season = settings.season[0].split(",");
      break;

      case "style":
      $scope.filter[category].style = settings.style[0].split(",");
        break;

      default:
    }

  }

  $scope.unCheckAll = function (item, category) {


    //console.log("myCheckbox",check);
    //console.log("category", category.length);


    switch (item) {
      case "brand":
        $scope.filter[category].brand = [];

        break;
      case "material":

        $scope.filter[category].material = [];
        break;
      case "pattern":
        $scope.filter[category].pattern= [];
        break;
      case "finishing":

        $scope.filter[category].finishing = [];
        break;
      case "color":
        $scope.filter[category].color = [];
        break;
      case "ageGroup":
        $scope.filter[category].agegroup = [];
        break;
      case "size":
        $scope.filter[category].size = [];
        break;

      case "season":
        $scope.filter[category].season = [];
        break;

      case "style":
        $scope.filter[category].style = [];
        break;
    }

  }



  $scope.submit = function (data) {
    //console.log("==========Edit product submit=========");
    console.log("data: ", data);
    console.log("file:::::::::::",$scope.myFile);

    let Top_filter = $scope.filter.Top;
    let Bottom_filter = $scope.filter.Bottom;
    let Shoes_filter = $scope.filter.Shoes;

    //console.log("filter", $scope.filter);

    let filter_top_brand = $scope.filter.Top.brand;
    let filter_top_material = $scope.filter.Top.material;
    let filter_top_pattern = $scope.filter.Top.pattern;
    let filter_top_finishing = $scope.filter.Top.finishing;
    let filter_top_color = $scope.filter.Top.color;
    let filter_top_size = $scope.filter.Top.size;
    let filter_top_agegroup = $scope.filter.Top.agegroup;
    let filter_top_season = $scope.filter.Top.season;
    let filter_top_style = $scope.filter.Top.style;

    let filter_bottom_brand = $scope.filter.Bottom.brand;
    let filter_bottom_material = $scope.filter.Bottom.material;
    let filter_bottom_pattern = $scope.filter.Bottom.pattern;
    let filter_bottom_finishing = $scope.filter.Bottom.finishing;
    let filter_bottom_color = $scope.filter.Bottom.color;
    let filter_bottom_size = $scope.filter.Bottom.size;
    let filter_bottom_agegroup = $scope.filter.Bottom.agegroup;
    let filter_bottom_season = $scope.filter.Bottom.season;
    let filter_bottom_style = $scope.filter.Bottom.style;

    let filter_shoes_brand = $scope.filter.Shoes.brand;
    let filter_shoes_material = $scope.filter.Shoes.material;
    let filter_shoes_pattern = $scope.filter.Shoes.pattern;
    let filter_shoes_finishing = $scope.filter.Shoes.finishing;
    let filter_shoes_color = $scope.filter.Shoes.color;
    let filter_shoes_size = $scope.filter.Shoes.size;
    let filter_shoes_agegroup = $scope.filter.Shoes.agegroup;
    let filter_shoes_season = $scope.filter.Shoes.season;
    let filter_shoes_style = $scope.filter.Shoes.style;

    //console.log("filter_shoes_brand", filter_shoes_brand);

    var imageId = "";
    var filelink = "";
    var image = _.findWhere(data.images, {
      default: true
    });


    if (image) {
      imageId = image._id;
      filelink = image._id + image.extn;
    }
   console.log("image id: ",imageId);
   console.log("file link:",filelink );


    let category = new Array();
    if (data.selectedCategoryLevel1) category.push(data.selectedCategoryLevel1);
    if (data.selectedCategoryLevel2) category.push(data.selectedCategoryLevel2);

    var fd = new FormData();
    fd.append("_id", data._id);
    fd.append("product_id", data.product_id); 
    fd.append("link", data.link);
    fd.append("name", data.name);
    fd.append("brand", data.selectedBrand);
    fd.append("material", data.selectedMaterial);
    fd.append("pattern", data.selectedPattern);
    fd.append("finishing", data.selectedFinishing);
    fd.append("gender", data.selectedGender);
    fd.append("agegroup", data.selectedAgegroup);
    fd.append("color", data.selectedColor);
    fd.append("season", data.selectedSeason);
    fd.append("style", data.selectedStyle);
    fd.append("size", data.selectedSize);
    fd.append("category", category);
    fd.append("active", data.active || 1);
    fd.append("imageId", imageId);
    fd.append("file", $scope.myFile);

    fd.append("filter_top_brand", filter_top_brand);
    fd.append("filter_top_material", filter_top_material);
    fd.append("filter_top_pattern", filter_top_pattern);
    fd.append("filter_top_finishing", filter_top_finishing);
    fd.append("filter_top_color", filter_top_color);
    fd.append("filter_top_size", filter_top_size);
    fd.append("filter_top_agegroup", filter_top_agegroup);
    fd.append("filter_top_season", filter_top_season);
    fd.append("filter_top_style", filter_top_style);

    fd.append("filter_bottom_brand", filter_bottom_brand);
    fd.append("filter_bottom_material", filter_bottom_material);
    fd.append("filter_bottom_pattern", filter_bottom_pattern);
    fd.append("filter_bottom_finishing", filter_bottom_finishing);
    fd.append("filter_bottom_color", filter_bottom_color);
    fd.append("filter_bottom_size", filter_bottom_size);
    fd.append("filter_bottom_agegroup", filter_bottom_agegroup);
    fd.append("filter_bottom_season", filter_bottom_season);
    fd.append("filter_bottom_style", filter_bottom_style);

    fd.append("filter_shoes_brand", filter_shoes_brand);
    fd.append("filter_shoes_material", filter_shoes_material);
    fd.append("filter_shoes_pattern", filter_shoes_pattern);
    fd.append("filter_shoes_finishing", filter_shoes_finishing);
    fd.append("filter_shoes_color", filter_shoes_color);
    fd.append("filter_shoes_size", filter_shoes_size);
    fd.append("filter_shoes_agegroup", filter_shoes_agegroup);
    fd.append("filter_shoes_season", filter_shoes_season);
    fd.append("filter_shoes_style", filter_shoes_style);

    _request.postFile("/admin/products/" + data._id, fd, function (data) {
      data = data.data;
      if (!_.isEmpty(data.images))
        data.image = data.images[0]._id + data.images[0].extn;

      var _products = SharedDataService.getProducts();
      for (var items in _products) {
        if (_products[items]._id === data._id) {
          _products[items] = data;
        }
      }
      alertFactory._alert("SuccessFully Edited");
      $modalInstance.dismiss("cancel");
    });
  };
}



function CtrlAddProduct(
  $scope,
  $modalInstance,
  $log,
  alertFactory,
  _request,
  SharedDataService
) {
  //console.log("===========Controller Add Product===========");

  $scope.enableSubmision = false;
  $scope.title = "Add";
  $scope.imageButtons = true;
  //  var stepcategories = [];
  $scope.expression = false;
  var steps = Array();

  $scope.steps = steps;

  $scope.filter = {
    Top: {
      brand: [],
      material: [],
      pattern: [],
      finishing: [],
      color: [],
      size: [],
      agegroup: [],
      season: [],
      style: []
    },

    Bottom: {
      brand: [],
      material: [],
      pattern: [],
      finishing: [],
      color: [],
      size: [],
      agegroup: [],
      season: [],
      style: []
    },

    Shoes: {
      brand: [],
      material: [],
      pattern: [],
      finishing: [],
      color: [],
      size: [],
      agegroup: [],
      season: [],
      style: []
    }
  };

  var data = SharedDataService.getfieldObj();
  var products = SharedDataService.getProducts();
  //console.log("---products:--->", products);

  // var categories = data.categories;
  // console.log("---categories:--->", categories);

  var settings = data.settings;
  var _data = data["_result"];
  // var _categorylevel1 = categories;
  var _categorylevel1 = ["Top", "Bottom", "Shoes"];
  // console.log(_categorylevel1[0].id,_categorylevel1[1].id,_categorylevel1[2].id);
  //console.log(_categorylevel1);
  //console.log("brands:", settings.brand);
  //console.log("Settings before creating $scope._item:----->", settings);

  //console.log(categories);
  //console.log("===========Controller Add Product===========");

  $scope._item = {
    categorylevel1: _categorylevel1,
    agegroup: settings.agegroup[0].split(","),
    brand: settings.brand,
    material: settings.material,
    pattern: settings.pattern,
    finishing: settings.finishing,
    size: settings.size[0].split(",") || "",
    color: settings.color[0].split(",") || "",
    gender: ["Male", "Female", "Unisex"] || "",
    season: settings.season[0].split(",") || "",
    style: settings.style[0].split(",") || ""
  };
  var temp_item = $scope._item;
  // console.log("temp_item: ", temp_item);
  // console.log("$scope._item", $scope._item);

  // $scope.topBrands = temp_item.brand.top[0].split(",");
  // $scope.bottomBrands = temp_item.brand.bottom[0].split(",");
  // $scope.shoesBrands = temp_item.brand.shoes[0].split(",");
  /* 
    $scope.filters = function (data) {
      //console.log(data);
    };
   */

  $scope.checkAll = function (item,category) {

    //console.log("myCheckbox",check);
    // console.log("myCheckbox", item);

      let cat1;
    switch (item) {
      case "brand":
        cat1=category.toLowerCase();
        $scope.filter[category].brand = settings.brand[cat1][0].split(",");
        // console.log(settings.brand.top[0].split(","));
        break;
      case "material":

      cat1=category.toLowerCase();
      $scope.filter[category].material = settings.material[cat1][0].split(",");

        break;
      case "pattern":
      cat1=category.toLowerCase();
      $scope.filter[category].pattern = settings.pattern[cat1][0].split(",");

        break;
      case "finishing":

      cat1=category.toLowerCase();
        $scope.filter[category].finishing = settings.finishing[cat1][0].split(",");
        break;

      case "color":
      $scope.filter[category].color = settings.color[0].split(",");

        break;
      case "ageGroup":
      $scope.filter[category].agegroup = settings.agegroup[0].split(",");

        break;
      case "size":
      $scope.filter[category].size = settings.size[0].split(",");
      break;

      case "season":
      $scope.filter[category].season = settings.season[0].split(",");
      break;

      case "style":
      $scope.filter[category].style = settings.style[0].split(",");
        break;

      default:
    }



  }

  $scope.unCheckAll = function (item,category) {

    //console.log("myCheckbox",check);
    // console.log("myCheckbox", item);


    switch (item) {
      case "brand":
        $scope.filter[category].brand = [];

        break;
      case "material":

        $scope.filter[category].material = [];
        break;
      case "pattern":
        $scope.filter[category].pattern= [];
        break;
      case "finishing":

        $scope.filter[category].finishing = [];
        break;
      case "color":
        $scope.filter[category].color = [];
        break;
      case "ageGroup":
        $scope.filter[category].agegroup = [];
        break;
      case "size":
        $scope.filter[category].size = [];
        break;

      case "season":
        $scope.filter[category].season = [];
        break;

      case "style":
        $scope.filter[category].style = [];
        break;
    }

  }



  $scope.updateCategorylevel2 = function (data) {
    $scope.enableSubmision = true;
    $scope._item.categorylevel2 = [];
    $scope._item.categorylevel2 = ["Casual", "Formal", "Business"];
    // console.log("data inside updateCategorylevel2 ", data);
    if (data == "Top") {
      // $scope._item.categorylevel2 = ["Casual", "Formal", "Business"];
      $scope.steps = ["Bottom", "Shoes"];
    } else if (data == "Bottom") {
      // $scope._item.categorylevel2 = ["Casual", "Formal", "Business"];
      $scope.steps = ["Top", "Shoes"];
    } else if (data == "Shoes") {
      //$scope._item.categorylevel2 = ["Casual", "Formal", "Business"];
      $scope.steps = ["Top", "Bottom"];
    }
  };

  $scope.updateCategoryLevelBrand = function (data) {
    // console.log(
    //   "------Data Inside updateCategory Level Brand:----------",
    //   data
    // );

    //console.log("Brand[0]",$scope._item.brand.bottom[0].split(","));
    if (data == "Top") {
      //   console.log("------Inside Top:--------");

      $scope.showBrand = $scope._item.brand.top[0].split(",");

      // console.log("$scope.showBrand:", $scope.showBrand);
      $scope.compatibleBrands = [
        $scope._item.brand.bottom[0].split(","),
        $scope._item.brand.shoes[0].split(",")
      ];
    } else if (data == "Bottom") {
      // console.log("------Inside bottom:--------");

      $scope.showBrand = $scope._item.brand.bottom[0].split(",");
      $scope.compatibleBrands = [
        $scope._item.brand.top[0].split(","),
        $scope._item.brand.shoes[0].split(",")
      ];
    } else if (data == "Shoes") {
      // console.log("------Inside shoes:--------");

      $scope.showBrand = $scope._item.brand.shoes[0].split(",");
      $scope.compatibleBrands = [
        $scope._item.brand.top[0].split(","),
        $scope._item.brand.bottom[0].split(",")
      ];
    }
  };

  $scope.updateCategoryLevelMaterial = function (data) {
    // console.log(
    //   "------Data inside updateCategory Level Material:----------",
    //   data
    // );
    //console.log("Brand[0]",$scope._item.brand.bottom[0].split(","));
    if (data == "Top") {
      $scope.showMaterial = $scope._item.material.top[0].split(",");
      $scope.compatibleMaterials = [
        $scope._item.material.bottom[0].split(","),
        $scope._item.material.shoes[0].split(",")
      ];
    } else if (data == "Bottom") {
      $scope.showMaterial = $scope._item.material.bottom[0].split(",");
      $scope.compatibleMaterials = [
        $scope._item.material.top[0].split(","),
        $scope._item.material.shoes[0].split(",")
      ];
    } else if (data == "Shoes") {
      $scope.showMaterial = $scope._item.material.shoes[0].split(",");
      $scope.compatibleMaterials = [
        $scope._item.material.top[0].split(","),
        $scope._item.material.bottom[0].split(",")
      ];
    }
  };

  $scope.updateCategoryLevelPattern = function (data) {
    // console.log(
    //   "------Data inside updateCategory Level Pattern:----------",
    //   data
    // );
    //console.log("Brand[0]",$scope._item.brand.bottom[0].split(","));
    if (data == "Top") {
      $scope.showPattern = $scope._item.pattern.top[0].split(",");
      $scope.compatiblePatterns = [
        $scope._item.pattern.bottom[0].split(","),
        $scope._item.pattern.shoes[0].split(",")
      ];
    } else if (data == "Bottom") {
      $scope.showPattern = $scope._item.pattern.bottom[0].split(",");
      $scope.compatiblePatterns = [
        $scope._item.pattern.top[0].split(","),
        $scope._item.pattern.shoes[0].split(",")
      ];
    } else if (data == "Shoes") {
      $scope.showPattern = $scope._item.pattern.shoes[0].split(",");
      $scope.compatiblePatterns = [
        $scope._item.pattern.top[0].split(","),
        $scope._item.pattern.bottom[0].split(",")
      ];
    }
    // console.log("$scope._item inside updateCategoryLevelPattern", $scope._item);
  };

  $scope.updateCategoryLevelFinishing = function (data) {
    // console.log(
    //   "------Data inside updateCategory Level Finishing:----------",
    //   data
    // );
    //console.log("Brand[0]",$scope._item.brand.bottom[0].split(","));
    if (data == "Top") {
      $scope.showFinshing = $scope._item.finishing.top[0].split(",");
      $scope.compatibleFinishing = [
        $scope._item.finishing.bottom[0].split(","),
        $scope._item.finishing.shoes[0].split(",")
      ];
    } else if (data == "Bottom") {
      $scope.showFinshing = $scope._item.finishing.bottom[0].split(",");
      $scope.compatibleFinishing = [
        $scope._item.finishing.top[0].split(","),
        $scope._item.finishing.shoes[0].split(",")
      ];
    } else if (data == "Shoes") {
      $scope.showFinshing = $scope._item.finishing.shoes[0].split(",");
      $scope.compatibleFinishing = [
        $scope._item.finishing.top[0].split(","),
        $scope._item.finishing.bottom[0].split(",")
      ];
    }
  };

  /*
    $scope.checkAll = function (_filter, type){
        switch(_filter){
            case 'brand':
                var aa = _.map($scope._item.brand, function(item) { return item });
                console.log(aa);
                $scope._item.item.filter['top'].brands = _.map($scope._item.brand, function(item) { return item });
            break;
            case 'color':
                $scope._item.filter.colors = _.map($scope._item.color, function(item) { return item });
            break;
            case 'material':
                $scope._item.filter.materials = _.map($scope._item.material, function(item) { return item });
            break;
            case 'pattern':
                $scope._item.filter.patterns = _.map($scope._item.pattern, function(item) { return item });
            break;
            default:
                console.log('no match');
            break;
        }
        
    }

    $scope.uncheckAll = function (_filter){
        switch(_filter){
            case 'brand':
                $scope._item.filterbrands = [];
            break;
            case 'color':
                $scope._item.filtercolors = [];
            break;
            case 'material':
                $scope._item.filtermaterials = [];
            break;
            case 'pattern':
                $scope._item.filterpatterns = [];
        }
    }
*/
  $scope.submit = function (data) {
    // console.log("==============================");
    // console.log("=========inside submit=========");
    // console.log("===============================");

    let category = new Array();

    let Top_filter = $scope.filter.Top;
    let Bottom_filter = $scope.filter.Bottom;
    let Shoes_filter = $scope.filter.Shoes;

    console.log("data: ", data);

    let filter_top_brand = $scope.filter.Top.brand;
    let filter_top_material = $scope.filter.Top.material;
    let filter_top_pattern = $scope.filter.Top.pattern;
    let filter_top_finishing = $scope.filter.Top.finishing;
    let filter_top_color = $scope.filter.Top.color;
    let filter_top_size = $scope.filter.Top.size;
    let filter_top_agegroup = $scope.filter.Top.agegroup;
    let filter_top_season = $scope.filter.Top.season;
    let filter_top_style = $scope.filter.Top.style;

    let filter_bottom_brand = $scope.filter.Bottom.brand;
    let filter_bottom_material = $scope.filter.Bottom.material;
    let filter_bottom_pattern = $scope.filter.Bottom.pattern;
    let filter_bottom_finishing = $scope.filter.Bottom.finishing;
    let filter_bottom_color = $scope.filter.Bottom.color;
    let filter_bottom_size = $scope.filter.Bottom.size;
    let filter_bottom_agegroup = $scope.filter.Bottom.agegroup;
    let filter_bottom_season = $scope.filter.Bottom.season;
    let filter_bottom_style = $scope.filter.Bottom.style;

    let filter_shoes_brand = $scope.filter.Shoes.brand;
    let filter_shoes_material = $scope.filter.Shoes.material;
    let filter_shoes_pattern = $scope.filter.Shoes.pattern;
    let filter_shoes_finishing = $scope.filter.Shoes.finishing;
    let filter_shoes_color = $scope.filter.Shoes.color;
    let filter_shoes_size = $scope.filter.Shoes.size;
    let filter_shoes_agegroup = $scope.filter.Shoes.agegroup;
    let filter_shoes_season = $scope.filter.Shoes.season;
    let filter_shoes_style = $scope.filter.Shoes.style;

    if (data.categorylevel1) category.push(data.categorylevel1);
    if (data.categorylevel2) category.push(data.categorylevel2);
    // if (data.categorylevel3) category.push(data.categorylevel3);


    var fd = new FormData();
    fd.append("name", data.name);
    fd.append("product_id", data.product_id);
    fd.append("link", data.link);
    fd.append("brand", data.brand);
    fd.append("material", data.material);
    fd.append("pattern", data.pattern);
    fd.append("finishing", data.finishing);
    fd.append("gender", data.gender);
    fd.append("agegroup", data.agegroup);
    fd.append("color", data.color);
    fd.append("season", data.season);
    fd.append("style", data.style);
    fd.append("size", data.size);
    fd.append("category", category);
    fd.append("active", data.active || 1);
    fd.append("file", $scope.myFile);


    fd.append("filter_top_brand", filter_top_brand);
    fd.append("filter_top_material", filter_top_material);
    fd.append("filter_top_pattern", filter_top_pattern);
    fd.append("filter_top_finishing", filter_top_finishing);
    fd.append("filter_top_color", filter_top_color);
    fd.append("filter_top_size", filter_top_size);
    fd.append("filter_top_agegroup", filter_top_agegroup);
    fd.append("filter_top_season", filter_top_season);
    fd.append("filter_top_style", filter_top_style);

    fd.append("filter_bottom_brand", filter_bottom_brand);
    fd.append("filter_bottom_material", filter_bottom_material);
    fd.append("filter_bottom_pattern", filter_bottom_pattern);
    fd.append("filter_bottom_finishing", filter_bottom_finishing);
    fd.append("filter_bottom_color", filter_bottom_color);
    fd.append("filter_bottom_size", filter_bottom_size);
    fd.append("filter_bottom_agegroup", filter_bottom_agegroup);
    fd.append("filter_bottom_season", filter_bottom_season);
    fd.append("filter_bottom_style", filter_bottom_style);

    fd.append("filter_shoes_brand", filter_shoes_brand);
    fd.append("filter_shoes_material", filter_shoes_material);
    fd.append("filter_shoes_pattern", filter_shoes_pattern);
    fd.append("filter_shoes_finishing", filter_shoes_finishing);
    fd.append("filter_shoes_color", filter_shoes_color);
    fd.append("filter_shoes_size", filter_shoes_size);
    fd.append("filter_shoes_agegroup", filter_shoes_agegroup);
    fd.append("filter_shoes_season", filter_shoes_season);
    fd.append("filter_shoes_style", filter_shoes_style);


    // console.log("filter_shoes_style", filter_shoes_style);
    //console.log("filter_shoes_brand", filter_shoes_brand);

    // console.log("fd:----->", fd);
    // console.log("category insdie submit:", category);

    _request.postFile("/admin/products", fd, function (data) {
      data = data.data;
      if (!_.isEmpty(data.images))
        data.image = data.images[0]._id + data.images[0].extn;

      SharedDataService.getProducts().splice(0, 0, data);
      alertFactory._alert("SuccessFully Added");
      $modalInstance.dismiss("cancel");
    });
  };
}