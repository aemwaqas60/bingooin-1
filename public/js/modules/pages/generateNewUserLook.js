"use strict";

(function () {
  angular
    .module("blankonApp.pages.generateNewLook", [
      "ui.bootstrap",
      "angularSpinner",
      "angularMoment",
      "localytics.directives",
      "checklist-model",
      "blankonApp.tables.default"
    ])
    .controller("GenerateNewLookCtrl", TableCtrl_ProductCombination);
})();

function TableCtrl_ProductCombination(
  $scope,
  $log,
  $http,
  settings,
  _request,
  $modal,
  $window,
  alertFactory,
  SharedDataService
) {
  var productsArr = [];
  var looktypeProductsArr = [];

  $scope.looks = ["Formal Look", "Casual Look", "Business Look"];
  $scope.genders = ["Male", "Female"];
  $scope.records = [{
      Name: "Alfreds Futterkiste",
      Country: "Germany"
    },
    {
      Name: "Berglunds snabbköp",
      Country: "Sweden"
    },
    {
      Name: "Centro comercial Moctezuma",
      Country: "Mexico"
    },
    {
      Name: "Ernst Handel",
      Country: "Austria"
    }
  ];

  $scope.showGenerateButton = true;
  $scope.casualMaleShirts = [];
  $scope.casualMalePents = [];
  $scope.casualMaleShoes = [];

  $scope.formalMaleShirts = [];
  $scope.formalMalePents = [];
  $scope.formalMaleShoes = [];

  $scope.businessMaleShirts = [];
  $scope.businessMalePents = [];
  $scope.businessMaleShoes = [];

  $scope.casualFemaleShirts = [];
  $scope.casualFemalePents = [];
  $scope.casualFemaleShoes = [];

  $scope.formalFemaleShirts = [];
  $scope.formalFemalePents = [];
  $scope.formalFemaleShoes = [];

  $scope.businessFemaleShirts = [];
  $scope.businessFemalePents = [];
  $scope.businessFemaleShoes = [];

  $scope.shirts = [];
  $scope.pents = [];
  $scope.shoes = [];

  _request.get("/admin/products", {}, function (data) {
    // console.log("==============GET---> /admin/products==============");
    data = data.data;
    console.log("data------->", data);
    $scope.products = data;
    let businessShirts = 0;
    let businessPents = 0;
    let businessShoes = 0;

    let maleCasualShirts = 0;
    let maleCasualPents = 0;
    let maleCasualShoes = 0;

    // for (var key in data) {
    //   console.log("inside first for loop index:", key);
    // }

    for (var index in data) {
      // console.log("Index:-------->", index);

      var val = data[index].category[0].split(",");

      let tempLook = val[0];
      let tempClass = val[1];

      let tempGender = data[index].gender;

      if (tempLook == "Top") {
        $scope.shirts.push(data[index]);
      } else if (tempLook == "Bottom") {
        $scope.pents.push(data[index]);
      } else if (tempLook == "Shoes") {
        $scope.shoes.push(data[index]);
      }

      /* if (tempGender === "Male") {
        if (tempClass == "Casual" && tempLook == "Top") {
          $scope.casualMaleShirts.push(data[index]);
          maleCasualShirts++;
        } else if (tempClass == "Casual" && tempLook == "Bottom") {
          maleCasualPents++;
          $scope.casualMalePents.push(data[index]);
        } else if (tempClass == "Casual" && tempLook == "Shoes") {
          maleCasualShoes++;
          $scope.casualMaleShoes.push(data[index]);
        } else if (tempClass == "Formal" && tempLook == "Top") {
          $scope.formalMaleShirts.push(data[index]);
        } else if (tempClass == "Formal" && tempLook == "Bottom") {
          $scope.formalMalePents.push(data[index]);
        } else if (tempClass == "Formal" && tempLook == "Shoes") {
          $scope.formalMaleShoes.push(data[index]);
        } else if (tempClass == "Business" && tempLook == "Top") {
          businessShirts++;
          $scope.businessMaleShirts.push(data[index]);
        } else if (tempClass == "Business" && tempLook == "Bottom") {
          businessPents++;
          $scope.businessMalePents.push(data[index]);
        } else if (tempClass == "Business" && tempLook == "Shoes") {
          businessShoes++;
          $scope.businessMaleShoes.push(data[index]);
        }
      } else if (tempGender === "Female") {
        if (tempClass == "Casual" && tempLook == "Top") {
          $scope.casualFemaleShirts.push(data[index]);
        } else if (tempClass == "Casual" && tempLook == "Bottom") {
          $scope.casualFemalePents.push(data[index]);
        } else if (tempClass == "Casual" && tempLook == "Shoes") {
          $scope.casualFemaleShoes.push(data[index]);
        } else if (tempClass == "Formal" && tempLook == "Top") {
          $scope.formalFemaleShirts.push(data[index]);
        } else if (tempClass == "Formal" && tempLook == "Bottom") {
          $scope.formalFemalePents.push(data[index]);
        } else if (tempClass == "Formal" && tempLook == "Shoes") {
          $scope.formalFemaleShoes.push(data[index]);
        } else if (tempClass == "Business" && tempLook == "Top") {
          $scope.businessFemaleShirts.push(data[index]);
        } else if (tempClass == "Business" && tempLook == "Bottom") {
          $scope.businessFemalePents.push(data[index]);
        } else if (tempClass == "Business" && tempLook == "Shoes") {
          $scope.businessFemaleShoes.push(data[index]);
        }
      } */
    }
    // console.log("businessShoes Shirts are:", businessShirts);
    // console.log("businessPents Pents are:", businessPents);
    // console.log("businessShoes Shoes are:", businessShoes);

    // console.log("maleCasualShoes Shirts are:", maleCasualShirts);
    // console.log("maleCasualPents Pents are:", maleCasualPents);
    // console.log("maleCasualShoes Shoes are:", maleCasualShoes);

    //console.log("$scope.products------->", $scope.products);
    /*   var _products = _.groupBy(data, function(x) {
      return x.category[0].toLowerCase();
    }); */
    // $scope.products = _.shuffle(dataManipulation(_products));
    //$scope.products = _.shuffle(dataManipulation(_products));
    // console.log("$scope.products------->",$scope.products);
    //  angular.copy(data, productsArr);
  });

  $scope.imageClick = function (item, i) {
    $scope.check = true;
    $scope.preCheck = false;
    //console.log("item:", item);

    $scope.showGenerateButton = false;
    $scope.slectedImage = item.productimage;
    $scope.compatibleItems1 = [];
    $scope.compatibleItems2 = [];``
    let val = item.category[0].split(",");
    let tempLook = val[0].replace(/[^a-z]/gi, "");
    let tempClass = val[1].replace(/[^a-z]/gi, "");

    // console.log("TempClass:-------->", tempClass);
    // console.log("TempLook:-------->", tempLook);

    $scope.previousSlected = {
      border: "0px solid gray"
    };
    $scope.slected = {
      border: "5px solid gray"
    };


    let filters = [
      "brand",
      "material",
      "finishing",
      "pattern",
      "agegroup",
      "size",
      "color",
      "season",
      "style"
    ];

    //Getting compatible Pents and Shoes

    if (tempLook == "Top") {
      // console.log("=================================");
      // console.log("=========Inside Top =============");
      // console.log("=================================");
      $scope.imageIndex1 = i;
      $scope.imageIndex2 = -1;
      $scope.imageIndex3 = -1;

      // getting compatible pents
      for (const index in $scope.pents) {

        let itemMatched = false;
        let allAttributeMatched = false;

        // console.log( index +` pent`, $scope.pents[index]);

        for (const filter in filters) {

          itemMatched = false;
          allAttributeMatched = false;

          // console.log("filter:", filters[filter]);
          let current_filters = item[
            "filter_bottom_" + filters[filter]];

            console.log("current_filters", current_filters);

          for (const current_filter in current_filters) {
            // console.log(
            //   "current_filter[curr_filter]:",
            //   current_filters[current_filter]
            // );

            // checking multiple value attributes
            if (
              filters[filter] == "material" ||
              filters[filter] == "agegroup" ||
              filters[filter] == "color" ||
              filters[filter] == "size" ||
              filters[filter] == "season"
            ) {
              // console.log("-----------");
              // console.log("filters[filter]----", filters[filter]);
              // console.log("-----------");

              //spliting multiple value attributes
              let m = $scope.pents[index][filters[filter]][0].split(",");
              console.log("splited multiple value  of ",filters[filter] ,m);


              for (const key2 in m) {
                // console.log("---------------");
                // console.log("spllited multiple value m[key2] " + m[key2]);
                // console.log("---------------");

                if (m[key2] == current_filters[current_filter]) {
                  // console.log("inside if statement of multiple vaues");
                  itemMatched = true;
                  allAttributeMatched = true;
                  break;
                }
              }

              if (itemMatched == true) {
                break;
              }

            } else {
              // checking single value attributes
              if (

                $scope.pents[index][filters[filter]] ==
                current_filters[current_filter]
              ) {
                // console.log("inside if statement of single value checking");
                // console.log("inside if statement");
                itemMatched = true;
                allAttributeMatched = true;
                break;
              }
            }

          }
          if (itemMatched == false) {
            allAttributeMatched = false;
            break;
          }
        }
        //console.log("allAttributeMatched", allAttributeMatched);
        if (allAttributeMatched == true) {
          $scope.compatibleItems1.push($scope.pents[index]);
        }
      }


      //  Getting compatible shoes
      for (const index in $scope.shoes) {
        let itemMatched = false;
        let allAttributeMatched = false;

        //console.log("pent", $scope.shoes[index]);

        for (const filter in filters) {

          itemMatched = false;
          allAttributeMatched = false;

          // console.log("filter:", filters[filter]);

          // checking each filter's multiple values
          let current_filters = item[
            "filter_shoes_" + filters[filter]
          ];
          // console.log("temp", temp);

          for (const current_filter in current_filters) {
            /*  console.log(
               "current_filter[curr_filter]:",
               current_filters[current_filter]
             ); */

            // checking multiple value attributes
            if (
              filters[filter] == "material" ||
              filters[filter] == "agegroup" ||
              filters[filter] == "color" ||
              filters[filter] == "size" ||
              filters[filter] == "season"
            ) {
              // console.log("-----------");
              // console.log("key----", filters[filter]);
              // console.log("-----------");

              //spliting multiple value attributes
              let m = $scope.shoes[index][filters[filter]][0].split(",");

              for (const key2 in m) {
                // console.log("---------------");
                // console.log("splited " + filters[filter] + "  " + m[key2]);
                // console.log("---------------");

                if (m[key2] == current_filters[current_filter]) {
                  //console.log("inside if statement");
                  itemMatched = true;
                  allAttributeMatched = true;
                  break;
                }
              }
              if (itemMatched == true) {
                break;
              }
            } else {
              // checking single value attributes
              if (
                $scope.shoes[index][filters[filter]] ==
                current_filters[current_filter]
              ) {
                //console.log("inside if statement");
                itemMatched = true;
                allAttributeMatched = true;
                break;
              }
            }
          }
          if (itemMatched == false) {
            allAttributeMatched = false;
            break;
          }
        }
        if (allAttributeMatched == true) {
          $scope.compatibleItems2.push($scope.shoes[index]);
        }
      }
    } else if (tempLook == "Bottom") {
      //Getting compatible shirts and shoes
      $scope.imageIndex1 = -1;
      $scope.imageIndex2 = i;
      $scope.imageIndex3 = -1;

      // console.log("=================================");
      // console.log("=========Inside Bottom===========");
      // console.log("=================================");

      // Getting compatible shirts
      for (const index in $scope.shirts) {
        let itemMatched = false;
        let allAttributeMatched = false;

        //console.log("shirt", $scope.shirts[index]);

        for (const filter in filters) {

          itemMatched = false;
          allAttributeMatched = false;

          //console.log("filter:", filters[filter]);
          let current_filters = item["filter_top_" + filters[filter]];
          // console.log("temp", temp);

          for (const current_filter in current_filters) {
            /*   console.log(
                "current_filter[curr_filter]:",
                current_filters[current_filter]
              ); */

            // checking multiple value attributes
            if (
              filters[filter] == "material" ||
              filters[filter] == "agegroup" ||
              filters[filter] == "color" ||
              filters[filter] == "size" ||
              filters[filter] == "season"
            ) {
              /*  console.log("-----------");
              console.log("key----", filters[filter]);
              console.log("-----------"); */

              //spliting multiple value attributes
              let m = $scope.shirts[index][filters[filter]][0].split(",");

              for (const key2 in m) {
                /*  console.log("---------------");
                 console.log("splited " + filters[filter] + "  " + m[key2]);
                 console.log("---------------"); */

                if (m[key2] == current_filters[current_filter]) {
                  //console.log("inside if statement");
                  itemMatched = true;
                  allAttributeMatched = true;
                  break;
                }
              }
              if (itemMatched == true) {
                break;
              }
            } else {
              // checking single value attributes
              if (
                $scope.shirts[index][filters[filter]] ==
                current_filters[current_filter]
              ) {
                //console.log("inside if statement");
                itemMatched = true;
                allAttributeMatched = true;
                break;
              }
            }
          }
          if (itemMatched == false) {
            allAttributeMatched = false;
            break;
          }
        }
        // console.log("allAttributeMatched", allAttributeMatched);
        if (allAttributeMatched == true) {
          $scope.compatibleItems1.push($scope.shirts[index]);
        }
      }
      //  Getting compatible shoes
      for (const index in $scope.shoes) {
        let itemMatched = false;
        let allAttributeMatched = false;

        //console.log("shoes", $scope.shoes[index]);

        for (const filter in filters) {

          itemMatched = false;
          allAttributeMatched = false;

          console.log("filter:", filters[filter]);

          // checking each filter's multiple values
          let current_filters = item[
            "filter_shoes_" + filters[filter]
          ];
          // console.log("temp", temp);

          for (const current_filter in current_filters) {
            /*    console.log(
                 "current_filter[curr_filter]:",
                 current_filters[current_filter]
               ); */

            // checking multiple value attributes
            if (
              filters[filter] == "material" ||
              filters[filter] == "agegroup" ||
              filters[filter] == "color" ||
              filters[filter] == "size" ||
              filters[filter] == "season"
            ) {
              /*   console.log("-----------");
                console.log("key----", filters[filter]);
                console.log("-----------"); */

              //spliting multiple value attributes
              let m = $scope.shoes[index][filters[filter]][0].split(",");

              for (const key2 in m) {
                /*  console.log("---------------");
                 console.log("splited " + filters[filter] + "  " + m[key2]);
                 console.log("---------------"); */

                if (m[key2] == current_filters[current_filter]) {
                  //console.log("inside if statement");
                  itemMatched = true;
                  allAttributeMatched = true;
                  break;
                }
              }
              if (itemMatched == true) {
                break;
              }
            } else {
              // checking single value attributes
              if (
                $scope.shoes[index][filters[filter]] ==
                current_filters[current_filter]
              ) {
                // console.log("inside if statement");
                itemMatched = true;
                allAttributeMatched = true;
                break;
              }
            }
          }
          if (itemMatched == false) {
            allAttributeMatched = false;
            break;
          }
        }
        if (allAttributeMatched == true) {
          $scope.compatibleItems2.push($scope.shoes[index]);
        }
      }

      //Getting compatible shirts and Pents
    } else if (tempLook == "Shoes") {
      $scope.imageIndex1 = -1;
      $scope.imageIndex2 = -1;
      $scope.imageIndex3 = i;

      // console.log("================================");
      // console.log("=========Inside Shoes===========");
      // console.log("================================");

      //  Getting compatible shirts
      for (const index in $scope.shirts) {

        let itemMatched = false;
        let allAttributeMatched = false;

        //console.log("shirt", $scope.shirts[index]);

        for (const filter in filters) {

          itemMatched = false;
          allAttributeMatched = false;

          //console.log("filter:", filters[filter]);

          // checking each filter's multiple values
          let current_filters = item["filter_top_" + filters[filter]];
          // console.log("temp", temp);

          for (const current_filter in current_filters) {
            /*  console.log(
               "current_filter[curr_filter]:",
               current_filters[current_filter]
             ); */

            // checking multiple value attributes
            if (
              filters[filter] == "material" ||
              filters[filter] == "agegroup" ||
              filters[filter] == "color" ||
              filters[filter] == "size" ||
              filters[filter] == "season"
            ) {
              //   console.log("-----------");
              // console.log("key----", filters[filter]);
              // console.log("-----------");

              //spliting multiple value attributes
              let m = $scope.shirts[index][filters[filter]][0].split(",");

              for (const key2 in m) {
                //    console.log("---------------");
                // console.log("splited " + filters[filter] + "  " + m[key2]);
                // console.log("---------------");

                if (m[key2] == current_filters[current_filter]) {
                  //console.log("inside if statement");
                  itemMatched = true;
                  allAttributeMatched = true;
                  break;
                }
              }
              if (itemMatched == true) {
                break;
              }
            } else {
              // checking single value attributes
              if (
                $scope.shirts[index][filters[filter]] ==
                current_filters[current_filter]
              ) {
                //  console.log("inside if statement");
                itemMatched = true;
                allAttributeMatched = true;
                break;
              }
            }
          }
          if (itemMatched == false) {
            allAttributeMatched = false;
            break;
          }
        }
        if (allAttributeMatched == true) {
          $scope.compatibleItems1.push($scope.shirts[index]);
        }
      }
      // getting compatible pents
      for (const index in $scope.pents) {
        let itemMatched = false;
        let allAttributeMatched = false;

        // console.log("pent", $scope.pents[index]);

        for (const filter in filters) {

          itemMatched = false;
          allAttributeMatched = false;

          //console.log("filter:", filters[filter]);
          let current_filters = item[
            "filter_bottom_" + filters[filter]];
          // console.log("temp", temp);

          for (const current_filter in current_filters) {
            /* console.log(
              "current_filter[curr_filter]:",
              current_filters[current_filter]
            ); */

            // checking multiple value attributes
            if (
              filters[filter] == "material" ||
              filters[filter] == "agegroup" ||
              filters[filter] == "color" ||
              filters[filter] == "size" ||
              filters[filter] == "season"
            ) {
              /*  console.log("-----------");
               console.log("key----", filters[filter]);
               console.log("-----------"); */

              //spliting multiple value attributes
              let m = $scope.pents[index][filters[filter]][0].split(",");

              for (const key2 in m) {
                /*   console.log("---------------");
                  console.log("splited " + filters[filter] + "  " + m[key2]);
                  console.log("---------------"); */

                if (m[key2] == current_filters[current_filter]) {
                  //console.log("inside if statement");
                  itemMatched = true;
                  allAttributeMatched = true;
                  break;
                }
              }
              if (itemMatched == true) {
                break;
              }
            } else {
              // checking single value attributes
              if (
                $scope.pents[index][filters[filter]] ==
                current_filters[current_filter]
              ) {
                //console.log("inside if statement");
                itemMatched = true;
                allAttributeMatched = true;
                break;
              }
            }
          }
          if (itemMatched == false) {
            allAttributeMatched = false;
            break;
          }
        }

        
        // console.log("allAttributeMatched", allAttributeMatched);
        if (allAttributeMatched == true) {
          $scope.compatibleItems2.push($scope.pents[index]);
        }
      }
    }
  };
}