const _ = require('underscore');
class filters{

    constructor(){

    }

    static removenulldata(data){
        data = _.filter(data, x => x._product !== null);
        data = _.pluck(data, '_product');
        
        _.map(data, x=>{
            x.toplevelcategory = x.category.level1;
            delete x.category;
            if(_.isEmpty(x.productimage) === false)
                x.productimage = `${process.env.host}${x.productimage}`;
            return;
        });
        
        return _.groupBy(data, x => (x.toplevelcategory).toLowerCase() );
    }

    static manipulateData(data, cb){
        data = this.removenulldata(data);

        var categories = ["top", "bottom", "shoes", "accessories"];

        var keys = [];
        _.each( data, ( val, key )=> {
            if ( val ) {
                keys.push(key);
            }
        });

        function dataObj (){
            let obj = Object();
            for(var items in categories){
                obj[categories[items]] = {};
            }
            return obj;
        }

        if(keys.length > 1){

            let arr = Array();
            for(var items in keys){
                arr.push(data[keys[items]]);
            }

            let _data = cartesianProductOf(...arr);
            
            let __data = Array();
            _.map(_data, x=>{
                let obj = new dataObj();
                for(var items in keys){
                    obj[keys[items]] = x[items];
                }
                __data.push(obj);
            });
            return cb(null, __data);
        }

        if(keys.length === 1){
            let obj = new dataObj();
            obj[keys] = data[keys][0];
            return cb(null, obj);
        }

        if(!keys.length)
            return cb(null, new dataObj());        
    }

    static _preFilterData(data){
        data = _.filter(data, x => x._product !== null);
        _.map(data, x=>{  x._product.color = x.color;});
        data = _.pluck(data, '_product');
        data = _.groupBy(data, x => (x.category[0]).toLowerCase());
        return data;
    }

    static filterBrand(data, cb){
        data = this._preFilterData(data);

        let tops = data.top || Array();
        let bottoms = data.bottom || Array();
        let shoes = data.shoes || Array();
        let _data = Array();
        let _tops = Array();
        let _bottoms = Array();
        let _shoes = Array();

        function dataObj (top={}, bottom={}, shoes={}){
            return {top, bottom, shoes};
        }

        /**
         * tops != null
         * bottoms != null
         * shoes != null
         */

        if(_.isEmpty(tops) === false && _.isEmpty(bottoms) === false && _.isEmpty(shoes) === false){
            
            for(let x in tops){
                let topBrands = tops[x].filter['bottom'].brand;

                for(let y in bottoms){
                    let bottomBrands = bottoms[y].filter['shoes'].brand;
                    let obj = new dataObj();
                    if(_.contains(topBrands, bottoms[y].brand) === true){
                        let condBrands = _.uniq(_.union(topBrands, bottomBrands));
                        obj.top = tops[x];
                        obj.bottom = bottoms[y];
                        _tops.push(tops[x]._id);
                        _bottoms.push(bottoms[y]._id);
                        let _flag = 0
                        for(let z in shoes){

                            if(_.contains(condBrands, shoes[z].brand) === true){
                                let obj = new dataObj();
                                obj.shoes = shoes[z];
                                obj.top = tops[x];
                                obj.bottom = bottoms[y];
                                _tops.push(tops[x]._id);
                                _bottoms.push(bottoms[y]._id);
                                _shoes.push(shoes[z]._id);
                                _data.push(obj);
                                _flag =1;
                            }
                        }
                        if(_flag === 0)
                            _data.push(obj);
                    }
                }
                for(let z in shoes){
                    if(_.contains(_shoes, shoes[z]._id) === false && _.contains(topBrands, shoes[z].brand) === true){
                            obj.top = tops[x];
                            obj.shoes = shoes[z];
                            _tops.push(tops[x]._id);
                            _shoes.push(shoes[z]._id);
                            _data.push(obj);
                    }
                }
            }

            for(let y in bottoms){
                let bottomBrands = bottoms[y].filter['shoes'].brand;
                let obj = new dataObj();
                if(_.contains(_bottoms, bottoms[y]._id) === false){

                    for(let z in shoes){
                        if(_.contains(_shoes, shoes[z]._id) === false && _.contains(bottomBrands, shoes[z].brand) === true){

                                obj.bottom = bottoms[y];
                                obj.shoes = shoes[z];
                                _bottoms.push(bottoms[y]._id);
                                _shoes.push(shoes[z]._id);
                                _data.push(obj);
                        }
                    }
                }
            }
        }

        /**
         * tops != null
         * bottoms != null
         * shoes = null
         */

        if(_.isEmpty(tops) === false && _.isEmpty(bottoms) === false && _.isEmpty(shoes) === true){
            for(let x in tops){
                console.log(tops[x].filter);
                
                let compbrands = tops[x].filter['bottom'].brand;
                let obj = new dataObj();
                for(let y in bottoms){
                    
                    if(_.contains(compbrands, bottoms[y].brand) === true){
                        
                        obj.top = tops[x];
                        obj.bottom = bottoms[y];
                        _tops.push(tops[x]._id);
                        _bottoms.push(bottoms[y]._id);
                        _data.push(obj);
                    }
                }
            }
        }

        /**
         * tops != null
         * bottoms = null
         * shoes != null
         */

        if(_.isEmpty(tops) === false && _.isEmpty(bottoms) === true && _.isEmpty(shoes) === false){
            for(let x in tops){
                let compbrands = tops[x].filter['shoes'].brand;
                let obj = new dataObj();
                for(let y in shoes){
                    
                    if(_.contains(compbrands, shoes[y].brand) === true){
                        
                        obj.top = tops[x];
                        obj.shoes = shoes[y];
                        _tops.push(tops[x]._id);
                        _shoes.push(shoes[y]._id);
                        _data.push(obj);
                    }
                }
            }
        }


        /**
         * tops = null
         * bottoms != null
         * shoes != null
         */
        if(_.isEmpty(tops) === true && _.isEmpty(bottoms) === false && _.isEmpty(shoes) === false){
            for(let x in bottoms){
                let compbrands = bottoms[x].filter['shoes'].brand;
                let obj = new dataObj();
                for(let y in shoes){
                    
                    if(_.contains(compbrands, shoes[y].brand) === true){
                        
                        obj.bottom = bottoms[x];
                        obj.shoes = shoes[y];
                        _bottoms.push(bottoms[x]._id);
                        _shoes.push(shoes[y]._id);
                        _data.push(obj);
                    }
                }
            }
        }


        /**
         * get remaining entries or those which are not matched
         */

        let arr = Array(tops, bottoms, shoes);
        let _arr = Array(_tops, _bottoms, _shoes);
        let categories = Array('top', 'bottom', 'shoes');

        for(let items in arr){

            let item = arr[items];
            for(let rows in item){

                if(_.contains(_arr[items], item[rows]._id) === false){
                    let obj = new dataObj();
                     obj[categories[items]] = item[rows];
                    _data.push(obj);
                }
            }
        }
        console.log(_data);
        return cb('null', _data)
    }



    static filterChain(data, filterItem){

        let topArr = Array();
        let bottomArr = Array();
        let shoesArr = Array();

        for(var items in data){

            let obj = data[items];
            if(obj.top.filter != undefined){
                
                let bottomFilterItem = obj.top.filter['bottom'][filterItem] || Array();
                let shoesFilterItem = obj.top.filter['shoes'][filterItem] || Array();
                let bottomshoesFilterItem = _.uniq(_.union(bottomFilterItem, shoesFilterItem));
                if(_.contains(obj.top.filter['bottom'], ['color', 'material', 'pattern']) === true || _.contains(obj.top.filter['shoes'], ['color', 'material', 'pattern']) === true){
                    if(obj.bottom[filterItem] != undefined && _.contains(bottomFilterItem, obj.bottom[filterItem]) === false){
                        bottomArr.push(obj.bottom);
                        obj.bottom = {};
                    }

                    if(obj.shoes[filterItem] != undefined && _.contains(bottomshoesFilterItem, obj.shoes[filterItem]) === false){
                        shoesArr.push(obj.shoes);
                        obj.shoes = {};
                    }
                }
            }


            if(obj.bottom.filter != undefined){

                let shoesFilterItem = obj.bottom.filter['shoes'][filterItem] || Array();
                if(_.contains(obj.bottom.filter['shoes'], ['color', 'material', 'pattern']) === true ){
                    if(obj.shoes[filterItem] != undefined && _.contains(shoesFilterItem, obj.shoes[filterItem]) === false){
                        shoesArr.push(obj.shoes);
                        obj.shoes = {};
                    }
                }
            }
        }

        if(_.isEmpty(topArr) === true || _.isEmpty(bottomArr) === true || _.isEmpty(shoesArr) === true)
            data = data_manipulation(data, [_.uniq(topArr), _.uniq(bottomArr), _.uniq(shoesArr)], ['top', 'bottom', 'shoes']);

        return data;

    }

    // static filterColors(data, cb){
    //     data = this.filterChain(data, 'color')
    //     return cb(null, data);
    // }

    static preFilterChain(data, cb){

        let filters = ['color', 'material', 'pattern'];
        for(let items in filters)
            data = this.filterChain(data, filters[items]);

        return cb(null, data);
    }


/*
    static filterColors(data, cb){

        let topArr = Array();
        let bottomArr = Array();
        let shoesArr = Array();

        for(var items in data){

            let obj = data[items];
            if(obj.top.filter != undefined){
                
                let bottomColorFilter = obj.top.filter['bottom']['color'] || Array();
                let shoesColorFilter = obj.top.filter['shoes']['color'] || Array();
                let bottomshoesColorFilter = _.uniq(_.union(bottomColorFilter, shoesColorFilter));
                
                if(obj.bottom.color != undefined && _.contains(bottomColorFilter, obj.bottom.color) === false){
                    bottomArr.push(obj.bottom);
                    obj.bottom = {};
                }

                if(obj.shoes.color != undefined && _.contains(bottomshoesColorFilter, obj.shoes.color) === false){
                    shoesArr.push(obj.shoes);
                    obj.shoes = {};
                }
            }


            if(obj.bottom.filter != undefined){

                let shoesColor = obj.bottom.filter['shoes']['color'] || Array();
                if(obj.shoes.color != undefined && _.contains(shoesColor, obj.shoes.color) === false){
                    shoesArr.push(obj.shoes);
                    obj.shoes = {};
                }
            }
        }

        data = data_manipulation(data, [_.uniq(topArr), _.uniq(bottomArr), _.uniq(shoesArr)], ['top', 'bottom', 'shoes']);

        return cb(null, data)
    }

    static filterMaterials(data, cb){

        let topArr = Array();
        let bottomArr = Array();
        let shoesArr = Array();

        for(var items in data){

            let obj = data[items];
            if(obj.top.filter != undefined && (_.isEmpty(obj.bottom) === false || _.isEmpty(obj.bottom) === false)){
                
                let bottomMaterialFilter = obj.top.filter['bottom']['material'] || Array();
                let shoesMaterialFilter = obj.top.filter['shoes']['material'] || Array();
                let bottomshoesMaterialFilter = _.uniq(_.union(bottomMaterialFilter, shoesMaterialFilter));
                
                if(obj.bottom.material != undefined && _.contains(bottomMaterialFilter, obj.bottom.material) === false){
                    bottomArr.push(obj.bottom);
                    obj.bottom = {};
                }

                if(obj.shoes.material != undefined && _.contains(bottomshoesMaterialFilter, obj.shoes.material) === false){
                    shoesArr.push(obj.shoes);
                    obj.shoes = {};
                }
            }


            if(obj.bottom.filter != undefined && (_.isEmpty(obj.shoes) === false)){

                let shoesMaterial = obj.bottom.filter['shoes']['material'] || Array();
                if(obj.shoes.material != undefined && _.contains(shoesMaterial, obj.shoes.material) === false){
                    shoesArr.push(obj.shoes);
                    obj.shoes = {};
                }
            }
        }

        let Arr = Array(_.uniq(topArr), _.uniq(bottomArr), _.uniq(shoesArr));
        let Arrkeys = Array('top', 'bottom', 'shoes');
        data = data_manipulation(data, Arr, Arrkeys);
        return cb(null, data)

    };


    static filterPatterns(data, cb){

        let topArr = Array();
        let bottomArr = Array();
        let shoesArr = Array();

        for(var items in data){

            let obj = data[items];
            if(obj.top.filter != undefined && (_.isEmpty(obj.bottom) === false || _.isEmpty(obj.bottom) === false)){
                
                let bottomPatternFilter = obj.top.filter['bottom']['pattern'] || Array();
                let shoesPatternFilter = obj.top.filter['shoes']['pattern'] || Array();
                let bottomshoesPatternFilter = _.uniq(_.union(bottomPatternFilter, shoesPatternFilter));
                
                if(obj.bottom.pattern != undefined && _.contains(bottomPatternFilter, obj.bottom.pattern) === false){
                    bottomArr.push(obj.bottom);
                    obj.bottom = {};
                }

                if(obj.shoes.pattern != undefined && _.contains(bottomshoesPatternFilter, obj.shoes.pattern) === false){
                    shoesArr.push(obj.shoes);
                    obj.shoes = {};
                }
            }

            if(obj.bottom.filter != undefined && (_.isEmpty(obj.shoes) === false)){

                let shoesPattern = obj.bottom.filter['shoes']['Pattern'] || Array();
                if(obj.shoes.pattern != undefined && _.contains(shoesMaterial, obj.shoes.pattern) === false){
                    shoesArr.push(obj.shoes);
                    obj.shoes = {};
                }
            }
        }

        let Arr = Array(_.uniq(topArr), _.uniq(bottomArr), _.uniq(shoesArr));
        let Arrkeys = Array('top', 'bottom', 'shoes');
        data = data_manipulation(data, Arr, Arrkeys);
        return cb(null, data)
    }

*/


}



function data_manipulation(data, Arr, Arrkeys){
    function dataObj(top={}, bottom={}, shoes={}){
        return {top, bottom, shoes};
    }

    for(let items in Arr){
        let item = Arr[items];
        let itemKey = Arrkeys[items];

        for(let rows in item){
            let flag = 0;
            for(let _data in data){
                let cats = data[_data];
                for(let cat in cats){
                    if(_.isEmpty(cats[cat]) === false && cats[cat]._id == item[rows]._id)
                        flag = 1;
                }
            }

            if(flag === 0){
                let obj = new dataObj();
                obj[itemKey] = item[rows];
                data.push(obj);
            }
        }
    }

    return data;
}

function cartesianProductOf() {
    return _.reduce(arguments, function(a, b) {
        return _.flatten(_.map(a, function(x) {
            return _.map(b, function(y) {
                return x.concat([y]);
            });
        }), true);
    }, [ [] ]);
};
module.exports = filters;