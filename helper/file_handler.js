require('../models/product.model');
const fs = require('fs'),
	path = require('path'),
	multer = require('multer'),
	main_folder_path = path.join(__dirname, '../files/images'),
	uuidV4 = require('uuid/v4'),
	mysql = require('../config/db-config'),
	mongoose = require('mongoose'),
	moment = require('moment');

let Products = mongoose.model('Product');

module.exports = {
	limits: {
		fileSize: 10000000000
	},

	deletefile: (file_path, cb) => {
		fs.unlink(file_path, (err) => {
			if (err)
				return console.error(err);
			else
				cb({
					data: 200,
					dataStatus: notify.successNotices.success.value
				});

		});

	},

	fileFilter: () => {
		return (req, file, cb) => {
			console.log('------------==================------------------')
			console.log('------------ in File Filter   ------------------')
			console.log('------------ =================------------------')
			let mimetype_accept = ['image/jpeg', 'image/jpg', 'image/png', ];
			let extname_accept = ['.jpeg', '.jpg', '.png'];
			if (mimetype_accept.indexOf(file.mimetype) != -1 && extname_accept.indexOf(path.extname(file.originalname).toLowerCase()) != -1)
				cb(null, true)
			else {
				//cb('file mimeType is not right extension: '+path.extname(file.originalname) + '  mimeType: '+file.mimetype,false);
				cb(new Error('file mimeType is not right extension: ' + path.extname(file.originalname) + '  mimeType: ' + file.mimetype))
			}
		}
	},

	fileSettings: () => {


		console.log('------------==================------------------')
		console.log('------------ Insdie File Settings------------------')
		console.log('------------ =================------------------')

		return multer.diskStorage({

			destination: (req, file, cb) => {

				console.log('-=-=-=-=-=-=-=-=-=-=-=-=-=-=');
				cb(null, main_folder_path);
			},
			filename: (req, file, cb) => {
				let obj = new Products({
					images: {
						name: file.originalname,
						extn: path.extname(file.originalname).toLowerCase(),
						default: 1,
					}
				});
				obj.save((err, product, numAffected) => {
					if (err)
						return cb(err, "error in query");

					req._insertId = product._id;
					req._productimage = product.images[0]._id + path.extname(file.originalname).toLowerCase();
					return cb(null, product.images[0]._id + path.extname(file.originalname).toLowerCase());

				});

			}
		})

	},


	updatefileSetting: () => {


		console.log('------------==================------------------');
		console.log('------------ Update File Setting----------------');
		console.log('------------ =================------------------');


		return multer.diskStorage({
			destination: (req, file, cb) => {
				let path = main_folder_path + `/` + req.body.filelink;

				if (fs.existsSync(path)) {
					fs.unlink(path, (err) => {
						if (err)
							cb(err);
					});
				}
				cb(null, main_folder_path);
			},
			filename: (req, file, cb) => {
				Products.findOneAndUpdate({
					_id: req.body._id,
					'images._id': req.body.imageId
				}, {
					"$set": {
						'images.$.name': file.originalname,
						'images.$.extn': path.extname(file.originalname)
					}
				}, (err, doc) => {
					if (err)
						cb(err);
					cb(null, req.body.imageId + path.extname(file.originalname).toLowerCase());
				})
			}
		})
	},

	file_rename: function (current_file, new_file, cb) {
		fs.open(current_file, 'r', (err, fd) => {
			if (err) {
				if (err.code === "ENOENT") {
					console.error(current_file + ' does not exist');
					return cb();
				} else
					throw err;
			}
			fs.rename(current_file, new_file, (err) => {
				if (err)
					throw err;
				else {
					console.log('changed');
					cb({
						data: 200,
						dataStatus: notify.successNotices.success.value
					});
				}
			})
		});
	},
};

function createDirectory(_directory) {
	if (!fs.existsSync(_directory)) {
		fs.mkdirSync(_directory);
		console.log('Directory created at ' + _directory)
	}

}