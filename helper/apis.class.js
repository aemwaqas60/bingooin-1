'use strict';

require('../models/product.model');
require('../models/category.model');
require('../models/settings.model');
require('../models/user.model');

const async = require('async');
const _ = require('underscore');
const Q = require('q');
const filtersClass = require('./filters.class');

// let Products = mongoose.model('Product');
// let Category = mongoose.model('Category');
// let Settings = mongoose.model('Settings');
// let Users = mongoose.model('User');



let _self;
class apis {

    constructor(mongoose) {
        _self = this;
        this.Products = mongoose.model('Product');
        this.Category = mongoose.model('Category');
        this.Settings = mongoose.model('Settings');
        this.Users = mongoose.model('User');
    }
    createUser(req, res, next) {
        let UserObj = new Users({
            name: 'user',
            role: 'user',
            email: "user@user.com",
            username: "user",
            wardrobe: {}
        });

        UserObj.save(err => {
            console.log(err)
            console.log('done');
        })
    };

    getuser(req, res, next) {
        _self.Users.findOne({
                _id: "592c0d452743890934923d67"
            })
            .populate('wardrobe._product')
            .exec(function (err, doc) {
                if (err) return handleError(err);
                console.log(doc);
                res.json(doc)
                // prints "The creator is Aaron"
            });
    }


    getProducts(req, res, next) {

        console.log("------Get All Products-----");
        _self.Products.find({}).exec((err, products) => {
            if (err) {
                res.josn({
                    status: 500,
                    status_message: 'something went wrong | internal server error',
                    data: []
                })
            }


            let data = _self.chnageProductsResponse(products);
            res.json({
                status: 200,
                status_message: "ok",
                data: data
            });
        })
    }

    chnageProductsResponse(products) {

        let data = [];


        for (let i = 0; i < products.length; i++) {
            //console.log("pro");
            let product_detials = {
                _id: products[i]._id,
                name: products[i].name,
                created_at: products[i].created_at,
                qr: products[i].qr,
                link: products[i].link,
                product_id: products[i].product_id,
                gender: products[i].gender,
                style: products[i].style,
                season: products[i].season,
                agegroup: products[i].agegroup,
                size: products[i].size,
                color: products[i].color,
                finishing: products[i].finishing,
                pattern: products[i].pattern,
                material: products[i].material,
                brand: products[i].brand,
                productimage: products[i].productimage,
            }
            let filter_top = {
                filter_top_brand: products[i].filter_top_brand,
                filter_top_material: products[i].filter_top_material,
                filter_top_pattern: products[i].filter_top_pattern,
                filter_top_finishing: products[i].filter_top_finishing,
                filter_top_style: products[i].filter_top_style,
                filter_top_season: products[i].filter_top_season,
                filter_top_agegroup: products[i].filter_top_agegroup,
                filter_top_size: products[i].filter_top_size,
                filter_top_color: products[i].filter_top_color,

            }
            let filter_bottom = {
                filter_bottom_brand: products[i].filter_bottom_brand,
                filter_bottom_material: products[i].filter_bottom_material,
                filter_bottom_pattern: products[i].filter_bottom_pattern,
                filter_bottom_finishing: products[i].filter_bottom_finishing,
                filter_bottom_style: products[i].filter_bottom_style,
                filter_bottom_season: products[i].filter_bottom_season,
                filter_bottom_agegroup: products[i].filter_bottom_agegroup,
                filter_bottom_size: products[i].filter_bottom_size,
                filter_bottom_color: products[i].filter_bottom_color,

            }
            let filter_shoes = {
                filter_shoes_brand: products[i].filter_shoes_brand,
                filter_shoes_material: products[i].filter_shoes_material,
                filter_shoes_pattern: products[i].filter_shoes_pattern,
                filter_shoes_finishing: products[i].filter_shoes_finishing,
                filter_shoes_style: products[i].filter_shoes_style,
                filter_shoes_season: products[i].filter_shoes_season,
                filter_shoes_agegroup: products[i].filter_shoes_agegroup,
                filter_shoes_size: products[i].filter_shoes_size,
                filter_shoes_color: products[i].filter_shoes_color,

            }

            let product = {
                product_detalls: product_detials,
                filter_top: filter_top,
                filter_bottom: filter_bottom,
                filter_shoes: filter_shoes,
            };

            data.push(product);
        }

        return data;
    }

    changeProductResponse(product) {


        let product_detials = {
            _id: product._id,
            name: product.name,
            created_at: product.created_at,
            qr: product.qr,
            link: product.link,
            product_id: product.product_id,
            gender: product.gender,
            style: product.style,
            season: product.season,
            agegroup: product.agegroup,
            size: product.size,
            color: product.color,
            finishing: product.finishing,
            pattern: product.pattern,
            material: product.material,
            brand: product.brand,
            productimage: product.productimage,
        }
        let filter_top = {
            filter_top_brand: product.filter_top_brand,
            filter_top_material: product.filter_top_material,
            filter_top_pattern: product.filter_top_pattern,
            filter_top_finishing: product.filter_top_finishing,
            filter_top_style: product.filter_top_style,
            filter_top_season: product.filter_top_season,
            filter_top_agegroup: product.filter_top_agegroup,
            filter_top_size: product.filter_top_size,
            filter_top_color: product.filter_top_color,

        }
        let filter_bottom = {
            filter_bottom_brand: product.filter_bottom_brand,
            filter_bottom_material: product.filter_bottom_material,
            filter_bottom_pattern: product.filter_bottom_pattern,
            filter_bottom_finishing: product.filter_bottom_finishing,
            filter_bottom_style: product.filter_bottom_style,
            filter_bottom_season: product.filter_bottom_season,
            filter_bottom_agegroup: product.filter_bottom_agegroup,
            filter_bottom_size: product.filter_bottom_size,
            filter_bottom_color: product.filter_bottom_color,

        }
        let filter_shoes = {
            filter_shoes_brand: product.filter_shoes_brand,
            filter_shoes_material: product.filter_shoes_material,
            filter_shoes_pattern: product.filter_shoes_pattern,
            filter_shoes_finishing: product.filter_shoes_finishing,
            filter_shoes_style: product.filter_shoes_style,
            filter_shoes_season: product.filter_shoes_season,
            filter_shoes_agegroup: product.filter_shoes_agegroup,
            filter_shoes_size: product.filter_shoes_size,
            filter_shoes_color: product.filter_shoes_color,

        }

        let pro = {
            product_detalls: product_detials,
            filter_top: filter_top,
            filter_bottom: filter_bottom,
            filter_shoes: filter_shoes,
        };


        return pro;
    }

    UserWardRobe(req, res, next) {

        console.log("---------getUserWardRobe Api---------");

        let {
            gender,
            look_type
        } = req.body;

        if (gender === '' || gender === undefined || look_type === '' || look_type === undefined) {
            res.json({
                status: 422,
                status_message: 'missing_field',
                data: []
            });
        }

        _self.Products.find({
            gender: {
                $regex: gender,
                $options: 'i'
            },
            category: {
                $regex: look_type,
                $options: "i"
            }
        }).exec((err, products) => {
            if (err) {
                res.json({
                    status: 500,
                    status_message: "somethig went wrong | internal server error",
                    data: []
                });
            }

            let wardRobes = _self.getWardRobes(products);

            if (wardRobes === "Not Available") {
                res.json({
                    status: "200",
                    status_message: "No Looks are available",
                    data: []
                });
            } else {

                res.json({
                    status: "200",
                    status_message: "wardrobe generated successfully",
                    data: wardRobes
                });
            }

        });

    }

    getWardRobes(products) {


        let shirts = [];
        let pents = [];
        let shoes = [];
        for (let i = 0; i < products.length; i++) {
            let cat = products[i].category[0].split(",");
            let type = cat[0];
            console.log(type);
            if (type == "Top") {
                shirts.push(products[i]);
            } else if (type == "Bottom") {
                pents.push(products[i]);
            } else if (type == "Shoes") {
                shoes.push(products[i]);
            }
        }

        if (shirts.length == 0 || pents.length == 0 || shoes.length == 0) {
            return "Not Available";
        }

        let wardRobes = [];

        for (let shirtIndex = 0; shirtIndex < shirts.length; shirtIndex++) {

            for (let pentIndex = 0; pentIndex < pents.length; pentIndex++) {

                for (let shoesIndex = 0; shoesIndex < shoes.length; shoesIndex++) {


                    let shirt_changed = _self.changeProductResponse(shirts[shirtIndex]);
                    let pent_changed = _self.changeProductResponse(pents[pentIndex]);
                    let shoes_changed = _self.changeProductResponse(shoes[shoesIndex]);
                    var look = {
                        shirt: shirt_changed,
                        pent: pent_changed,
                        shoes: shoes_changed
                    };

                    wardRobes.push(look);

                }
            }
        }

        return wardRobes;

    }

    manupluteProductData(product) {

        let category = product.category[0].split(",");
        let type = category[0];
        let gender = product.gender;
        let type1, type2;
        let brand1, brand2, material1, material2, pattern1, pattern2, finishing1, finishing2;
        let style1, style2, season1, season2, color1, color2, ageGroup1, ageGroup2, size1, size2;


        if (type == "Top") {
            type1 = "Bottom";
            type2 = "Shoes";
            brand1 = product.filter_bottom_brand;
            brand2 = product.filter_shoes_brand;
            material1 = product.filter_bottom_material;
            material2 = product.filter_shoes_material;
            pattern1 = product.filter_bottom_pattern;
            pattern2 = product.filter_shoes_pattern;
            finishing1 = product.filter_bottom_finishing;
            finishing2 = product.filter_shoes_finishing;

            style1 = product.filter_bottom_style;
            style2 = product.filter_shoes_style;

            season1 = product.filter_bottom_season;
            season2 = product.filter_shoes_season;

            color1 = product.filter_bottom_color;
            color2 = product.filter_shoes_color;

            ageGroup1 = product.filter_bottom_agegroup;
            ageGroup2 = product.filter_shoes_agegroup;

            size1 = product.filter_bottom_size;
            size2 = product.filter_bottom_size;



        } else if (type = "Bottom") {

            type1 = "Top";
            type2 = "Shoes";
            brand1 = product.filter_top_brand;
            brand2 = product.filter_shoes_brand;
            material1 = product.filter_top_material;
            material2 = product.filter_shoes_material;
            pattern1 = product.filter_top_pattern;
            pattern2 = product.filter_shoes_pattern;
            finishing1 = product.filter_top_finishing;
            finishing2 = product.filter_shoes_finishing;


            style1 = product.filter_top_style;
            style2 = product.filter_shoes_style;

            season1 = product.filter_top_season;
            season2 = product.filter_shoes_season;

            color1 = product.filter_top_color;
            color2 = product.filter_shoes_color;

            ageGroup1 = product.filter_top_agegroup;
            ageGroup2 = product.filter_shoes_agegroup;

            size1 = product.filter_top_size;
            size2 = product.filter_shoes_size;

        } else {
            type1 = "Top";
            type2 = "Bottom";
            brand1 = product.filter_bottom_brand;
            brand2 = product.filter_top_brand;
            material1 = product.filter_bottom_material;
            material2 = product.filter_top_material;
            pattern1 = product.filter_bottom_pattern;
            pattern2 = product.filter_top_pattern;
            finishing1 = product.filter_bottom_finishing;
            finishing2 = product.filter_top_finishing;


            style1 = product.filter_bottom_style;
            style2 = product.filter_top_style;

            season1 = product.filter_bottom_season;
            season2 = product.filter_top_season;

            color1 = product.filter_bottom_color;
            color2 = product.filter_top_color;

            ageGroup1 = product.filter_bottom_agegroup;
            ageGroup2 = product.filter_top_agegroup;

            size1 = product.filter_bottom_size;
            size2 = product.filter_top_size;


        }

        let data = {
            gender,
            type1,
            type2,
            brand1,
            brand2,
            material1,
            material2,
            pattern1,
            pattern2,
            finishing1,
            finishing2,
            style1,
            style2,
            season1,
            season2,
            color1,
            color2,
            ageGroup1,
            ageGroup2,
            size1,
            size2
        }

        return data;
    }
    //still working on     
    generateNewLook(req, res, next) {
        console.log("---------generateNewLook Api---------");

        let id = req.body.product_id;
        let firstLooks = [];
        let secondLooks = [];

        _self.Products.findById(id).then((product) => {

            let data = _self.manupluteProductData(product);

            _self.Products.find({
                gender: {
                    $regex: data.gender,
                    $options: 'i'
                },
                category: {
                    $regex: data.type1,
                    $options: 'i'
                },
                brand: {
                    $in: data.brand1
                },
                pattern: {
                    $in: data.pattern1
                },
                finishing: {
                    $in: data.finishing1
                },
                style: {
                    $in: data.style1
                }

            }).then((item1) => {

                _self.Products.find({
                    gender: {
                        $regex: data.gender,
                        $options: 'i'
                    },
                    category: {
                        $regex: data.type2,
                        $options: 'i'
                    },
                    brand: {
                        $in: data.brand2
                    },
                    pattern: {
                        $in: data.pattern2
                    },
                    finishing: {
                        $in: data.finishing2
                    },
                    style: {
                        $in: data.style2
                    }

                }).then((item2) => {
                    async.waterfall([
                        function findItemOne(callback) {

                            if (item1.length < 1) {
                                console.log("---------------inside item one if--------------", );
                                console.log("item1.length", item1.length)
                                callback(null, firstLooks)
                            }

                            // checking each item's multiple value attributes with selected item's multiple value atttributes
                            item1.forEach((item, index) => {
                                console.log("Inside item one");
                                console.log("Index", index);
                                console.log("item1.length", item1.length);


                                let item_materials = item.material[0].split(",");
                                let item_colors = item.color[0].split(",");
                                let item_sizes = item.size[0].split(",");
                                let item_ages = item.agegroup[0].split(",");
                                let item_seasons = item.season[0].split(",");

                                // current Item multiple values attributes
                                let item_filter = new Array();
                                item_filter[0] = item_materials;
                                item_filter[1] = item_colors;
                                item_filter[2] = item_sizes;
                                item_filter[3] = item_ages;
                                item_filter[4] = item_seasons;

                                console.log(item_filter);
                                let filter = ['material1', 'color1', 'size1', 'ageGroup1', 'season1']

                                let itemMatched = false;
                                // checking current item's multiple attrbutes with selected item's multiple value attributes
                                for (let i = 0; i < 5; i++) {
                                    itemMatched = false;

                                    for (let j = 0; j < item_filter[i].length; j++) {
                                        console.log("f[i][j]", item_filter[i][j])

                                        for (let k = 0; k < data[filter[i]].length; k++) {
                                            if (item_filter[i][j] == data[filter[i]][k]) {
                                                itemMatched = true;
                                                break;
                                            }
                                        }
                                        if (itemMatched == true) {
                                            break;
                                        }
                                    }

                                    if (itemMatched == false) {
                                        break;
                                    }
                                }
                                if (itemMatched == true) {
                                    firstLooks.push(item);
                                }

                                if ((item1.length - 1) == index) {
                                    callback(null, firstLooks);
                                }
                            });
                        },
                        function findItemTwo(look1, callback) {

                            // if item not found 
                            if (item2.length < 1) {
                                console.log("item2.length", item2.length)
                                callback(null, look1)

                            }
                            item2.forEach((item, index) => {
                                console.log("Inside item two");
                                console.log("Index", index);
                                console.log("item2.length", item2.length);

                                // console.log(item);

                                let item_materials = item.material[0].split(",");
                                let item_colors = item.color[0].split(",");
                                let item_sizes = item.size[0].split(",");
                                let item_ages = item.agegroup[0].split(",");
                                let item_seasons = item.season[0].split(",");

                                // current Item multiple values attributes
                                let item_filter = new Array();
                                item_filter[0] = item_materials;
                                item_filter[1] = item_colors;
                                item_filter[2] = item_sizes;
                                item_filter[3] = item_ages;
                                item_filter[4] = item_seasons;

                                console.log(item_filter);
                                let filter = ['material2', 'color2', 'size2', 'ageGroup2', 'season2']


                                let itemMatched = false;
                                // checking current item's multiple attrbutes with selected item's multiple value attributes
                                for (let i = 0; i < 5; i++) {
                                    itemMatched = false;

                                    for (let j = 0; j < item_filter[i].length; j++) {
                                        console.log("f[i][j]", item_filter[i][j])

                                        for (let k = 0; k < data[filter[i]].length; k++) {
                                            if (item_filter[i][j] == data[filter[i]][k]) {
                                                itemMatched = true;
                                                break;
                                            }
                                        }
                                        if (itemMatched == true) {
                                            break;
                                        }
                                    }

                                    if (itemMatched == false) {
                                        break;
                                    }
                                }
                                if (itemMatched == true) {
                                    console.log(" second item matched", itemMatched);
                                    secondLooks.push(item);
                                }

                                if ((item2.length - 1) == index) {
                                    let looks = [];
                                    looks.push(look1);
                                    looks.push(secondLooks);
                                    callback(null, looks);
                                }
                            });
                        },

                    ], function (err, result) {

                        if (err) {
                            return res.status(200).json({
                                status: "403",
                                status_message: "something went wrong generating new looks",
                                error: err
                            });

                        } else {
                            return res.status(200).json({
                                status: "200",
                                status_message: "look generated successfully",
                                slected_product: product,
                                data: result,
                            });
                        }
                    })

                }).catch((err) => {

                    res.json({
                        status: "500",
                        status_message: "something went wrong finding second item | interal server error",
                        error: err
                    });

                })

            }).catch((err) => {

                res.json({
                    status: "500",
                    status_message: "something went wrong finding first item | interal server error",
                    error: err
                });
            })

        }).catch((err) => {
            res.json({
                status: "500",
                status_message: "something went wrong | interal server error",
                error: err
            });
        })


    }

    getUserProducts(req, res, next) {
        _self.Users.findById('592c0d452743890934923d67', 'wardrobe')
            .populate({
                path: 'wardrobe._product',
                select: 'name filters.brands brand'
            })
            .exec(function (err, doc) {
                if (err) return handleError(err);
                return res.json(doc)
            });
    }


    postUserProducts(req, res, next) {
        let {
            productId,
            size,
            color
        } = req.body;
        console.log(productId)
        _self.Users.findByIdAndUpdate("592c0d452743890934923d67", {
            $push: {
                wardrobe: {
                    _product: productId,
                    size,
                    color
                }
            }
        }, {
            new: true
        }, (err, doc) => {

            if (err)
                console.log(err);
            return res.json({
                status: 1,
                status_message: "ok",
                data: doc
            });
        });
    };

    getUserLook(req, res, next) {
        let {
            look,
            _id
        } = req.params;
        if (!_id.match(/^[0-9a-fA-F]{24}$/))
            return res.json({
                status: 0,
                status_message: 'invalid id'
            });

        _self.Users.findById(_id, 'wardrobe')
            .populate({
                path: 'wardrobe._product',
                match: {
                    "looktype": {
                        "$eq": look
                    }
                },
                select: 'category name productimage filter brand color looktype material'
            })
            .lean()
            .exec((err, doc) => {
                if (err) return res.json(err);


                filtersClass.filterBrand(doc.wardrobe, (err, result) => {
                    console.log(result)
                    filtersClass.preFilterChain(result, (err, result) => {

                        return res.json(result);
                    })
                    // filtersClass.filterColors(result, (err, result)=>{
                    //     return res.json(result);
                    //     // filtersClass.filterMaterials(result, (err, result)=>{
                    //     //     return res.json(result);
                    //     //     // filtersClass.filterPatterns(result, (err, _result)=>{
                    //     //     //     return res.json({result,_result});
                    //     //     // });
                    //     // })
                    // });
                })
            });
    };


};


module.exports = apis;