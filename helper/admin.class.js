'use strict'
require('../config/mongo')
require('../models/product.model')
require('../models/category.model')
require('../models/settings.model')

const qr_generator = require('../helper/qr_generator')
//const mongoose = require('mongoose');
// const async = require('async');
//const path = require('path');
const _ = require('underscore')
//const main_folder = path.join(__dirname, '../files/');

//let Products = mongoose.model('Product');
//let Category = mongoose.model('Category')
//let Settings = mongoose.model('Settings');


let _self
class admin {
	constructor(path, mongoose, async) {
		_self = this
		this.async = async
		this.main_folder = path.join(__dirname, '../files/')
		this.Products = mongoose.model('Product')
		this.Category = mongoose.model('Category')
		this.Settings = mongoose.model('Settings')
	}

	getProductsAddData(req, res, next) {

		_self.async.parallel([
				function (callback) {
					_self.Category.find({}, (err, result) => {
						if (err)
							callback(err, 'query error')
						return callback(null, result)
					})
				},
				function (callback) {
					_self.Settings.find({}, (err, result) => {
						if (err)
							callback(err, 'query error')
						return callback(null, result[0])
					})
				}
			],
			function (err, results) {
				if (err)
					res.json({
						status: 510,
						status_message: "query error"
					})
				res.json({
					status: 1,
					status_message: "ok",
					data: {
						categories: results[0],
						settings: results[1]
					}
				})
			})
	}


	getProducts(req, res, next) {
		_self.Products.find().sort({
			created_at: -1
		}).exec((err, doc) => {
			if (err)
				return res.json({
					success: 0,
					message: 'query error'
				})
			return res.json({
				status: 1,
				status_message: "ok",
				data: doc
			})
		})
	}

	deleteProducts(req, res, next) {
		_self.Products.deleteOne({
			_id: req.params.id
		}, (err) => {
			console.log('------------------------')
			if (err)
				console.log(err)
			res.json({
				status: 1,
				status_message: "ok",
				data: {
					msg: "successfully deleted"
				}
			})
		})
	}

	postProducts(req, res, next) {
		console.log("==============================");
		console.log("==========post Products=======");
		console.log("==============================");

		console.log("-------->>>>>req.body:", req.body);

		let {
			name,
			brand,
			gender,
			material,
			pattern,
			agegroup,
			color,
			size,
			season,
			style,
			category,
			finishing,
			product_id,
			link,
			filter_top_brand,
			filter_top_material,
			filter_top_pattern,
			filter_top_finishing,
			filter_top_color,
			filter_top_size,
			filter_top_agegroup,
			filter_top_season,
			filter_top_style,

			filter_bottom_brand,
			filter_bottom_material,
			filter_bottom_pattern,
			filter_bottom_finishing,
			filter_bottom_color,
			filter_bottom_size,
			filter_bottom_agegroup,
			filter_bottom_season,
			filter_bottom_style,

			filter_shoes_brand,
			filter_shoes_material,
			filter_shoes_pattern,
			filter_shoes_finishing,
			filter_shoes_color,
			filter_shoes_size,
			filter_shoes_agegroup,
			filter_shoes_season,
			filter_shoes_style

		} = req.body;

		// Checking null values

		filter_top_brand = (filter_top_brand == undefined) ? [] : (filter_top_brand === '') ? [] : filter_top_brand.split(",");
		filter_top_material = (filter_top_material == undefined) ? [] : (filter_top_material === '') ? [] : filter_top_material.split(",");
		filter_top_pattern = (filter_top_pattern == undefined) ? [] : (filter_top_pattern === '') ? [] : filter_top_pattern.split(",");
		filter_top_finishing = (filter_top_finishing == undefined) ? [] : (filter_top_finishing === '') ? [] : filter_top_finishing.split(",");
		filter_top_color = (filter_top_color == undefined) ? [] : (filter_top_color === '') ? [] : filter_top_color.split(",");
		filter_top_size = (filter_top_size == undefined) ? [] : (filter_top_size === '') ? [] : filter_top_size.split(",");
		filter_top_agegroup = (filter_top_agegroup == undefined) ? [] : (filter_top_agegroup === '') ? [] : filter_top_agegroup.split(",");
		filter_top_season = (filter_top_season == undefined) ? [] : (filter_top_season === '') ? [] : filter_top_season.split(",");
		filter_top_style = (filter_top_style == undefined) ? [] : (filter_top_style === '') ? [] : filter_top_style.split(",");


		filter_bottom_brand = (filter_bottom_brand == undefined) ? [] : (filter_bottom_brand === '') ? [] : filter_bottom_brand.split(",");
		filter_bottom_material = (filter_bottom_material == undefined) ? [] : (filter_bottom_material === '') ? [] : filter_bottom_material.split(",");
		filter_bottom_pattern = (filter_bottom_pattern == undefined) ? [] : (filter_bottom_pattern === '') ? [] : filter_bottom_pattern.split(",");
		filter_bottom_finishing = (filter_bottom_finishing == undefined) ? [] : (filter_bottom_finishing === '') ? [] : filter_bottom_finishing.split(",");
		filter_bottom_color = (filter_bottom_color == undefined) ? [] : (filter_bottom_color === '') ? [] : filter_bottom_color.split(",");
		filter_bottom_size = (filter_bottom_size == undefined) ? [] : (filter_bottom_size === '') ? [] : filter_bottom_size.split(",");
		filter_bottom_agegroup = (filter_bottom_agegroup == undefined) ? [] : (filter_bottom_agegroup === '') ? [] : filter_bottom_agegroup.split(",");
		filter_bottom_season = (filter_bottom_season == undefined) ? [] : (filter_bottom_season === '') ? [] : filter_bottom_season.split(",");
		filter_bottom_style = (filter_bottom_style == undefined) ? [] : (filter_bottom_style === '') ? [] : filter_bottom_style.split(",");


		filter_shoes_brand = (filter_shoes_brand == undefined) ? [] : (filter_shoes_brand === '') ? [] : filter_shoes_brand.split(",");
		filter_shoes_material = (filter_shoes_material == undefined) ? [] : (filter_shoes_material === '') ? [] : filter_shoes_material.split(",");
		filter_shoes_pattern = (filter_shoes_pattern == undefined) ? [] : (filter_shoes_pattern === '') ? [] : filter_shoes_pattern.split(",");
		filter_shoes_finishing = (filter_shoes_finishing == undefined) ? [] : (filter_shoes_finishing === '') ? [] : filter_shoes_finishing.split(",");
		filter_shoes_color = (filter_shoes_color == undefined) ? [] : (filter_shoes_color === '') ? [] : filter_shoes_color.split(",");
		filter_shoes_size = (filter_shoes_size == undefined) ? [] : (filter_shoes_size === '') ? [] : filter_shoes_size.split(",");
		filter_shoes_agegroup = (filter_shoes_agegroup == undefined) ? [] : (filter_shoes_agegroup === '') ? [] : filter_shoes_agegroup.split(",");
		filter_shoes_season = (filter_shoes_season == undefined) ? [] : (filter_shoes_season === '') ? [] : filter_shoes_season.split(",");
		filter_shoes_style = (filter_shoes_style == undefined) ? [] : (filter_shoes_style === '') ? [] : filter_shoes_style.split(",");


		if (req._insertId === undefined || req._insertId === '') {

			console.log("<<<----Inside if of post products--->>>")

			_self.Products.create({
				name,
				brand,
				gender,
				material,
				pattern,
				agegroup,
				color,
				size,
				season,
				style,
				category,
				Top_filter,
				Bottom_filter,
				Shoes_filter,
				Bottom_filter_brand,
				finishing,
				product_id,
				link,

				filter_top_brand,
				filter_top_material,
				filter_top_pattern,
				filter_top_finishing,
				filter_top_color,
				filter_top_size,
				filter_top_agegroup,
				filter_top_season,
				filter_top_style,

				filter_bottom_brand,
				filter_bottom_material,
				filter_bottom_pattern,
				filter_bottom_finishing,
				filter_bottom_color,
				filter_bottom_size,
				filter_bottom_agegroup,
				filter_bottom_season,
				filter_bottom_style,

				filter_shoes_brand,
				filter_shoes_material,
				filter_shoes_pattern,
				filter_shoes_finishing,
				filter_shoes_color,
				filter_shoes_size,
				filter_shoes_agegroup,
				filter_shoes_season,
				filter_shoes_style
			}, (err, product, numAffected) => {
				if (err) {
					console.log("error occured")
					return console.log(err);
				}

				let qr = {
					imagelink: `images/${product._id}.png`,
					pdflink: `pdfs/${product._id}.pdf`,
				}
				console.log(qr)

				//let Top_filter = req.body.Top_filter
				//let Bottom_filter =req.Bottom_filter;
				//let Shoes_filter =req.Bottom_filter;

				_self.Products.findByIdAndUpdate(product._id, {
					qr
				}, {
					new: true
				}, (err, _product, numAffected) => {
					if (err)
						console.log(err)
					let qrgeneratorObj = new qr_generator()
					qrgeneratorObj.generate_png(product._id)
					qrgeneratorObj.generate_pdf(product._id)
					return res.json({
						status: 1,
						status_message: "ok",
						data: _product
					})
				})
			})
		} else {


			console.log("<<<----Inside else of post products--->>>")

			let qr = {
				imagelink: `images/${req._insertId}.png`,
				pdflink: `pdfs/${req._insertId}.pdf`,
			}

			_self.Products.findByIdAndUpdate(req._insertId, {
				name,
				brand,
				gender,
				material,
				pattern,
				agegroup,
				color,
				size,
				category,
				season,
				style,
				qr,
				productimage: req._productimage || null,
				finishing,
				product_id,
				link,

				filter_top_brand,
				filter_top_material,
				filter_top_pattern,
				filter_top_finishing,
				filter_top_color,
				filter_top_size,
				filter_top_agegroup,
				filter_top_season,
				filter_top_style,

				filter_bottom_brand,
				filter_bottom_material,
				filter_bottom_pattern,
				filter_bottom_finishing,
				filter_bottom_color,
				filter_bottom_size,
				filter_bottom_agegroup,
				filter_bottom_season,
				filter_bottom_style,

				filter_shoes_brand,
				filter_shoes_material,
				filter_shoes_pattern,
				filter_shoes_finishing,
				filter_shoes_color,
				filter_shoes_size,
				filter_shoes_agegroup,
				filter_shoes_season,
				filter_shoes_style
			}, {
				new: true,
				upsert: true
			}, (err, doc, numAffected) => {
				if (err)
					return console.log(err)
				let qrgeneratorObj = new qr_generator()
				qrgeneratorObj.generate_png(req._insertId)
				qrgeneratorObj.generate_pdf(req._insertId)

				return res.json({
					status: 1,
					status_message: "ok",
					data: doc
				})
			})
		}

	}

	copyProduct(req, res) {

		console.log("==============================");
		console.log("==========copy Products=======");
		console.log("==============================");

		console.log("-------->>>>>req.body:", req.body);

		let {
			name,
			brand,
			gender,
			material,
			pattern,
			agegroup,
			color,
			size,
			season,
			style,
			category,
			finishing,
			product_id,
			productimage,
			link,
			qr,

			filter_top_brand,
			filter_top_material,
			filter_top_pattern,
			filter_top_finishing,
			filter_top_color,
			filter_top_size,
			filter_top_agegroup,
			filter_top_season,
			filter_top_style,

			filter_bottom_brand,
			filter_bottom_material,
			filter_bottom_pattern,
			filter_bottom_finishing,
			filter_bottom_color,
			filter_bottom_size,
			filter_bottom_agegroup,
			filter_bottom_season,
			filter_bottom_style,

			filter_shoes_brand,
			filter_shoes_material,
			filter_shoes_pattern,
			filter_shoes_finishing,
			filter_shoes_color,
			filter_shoes_size,
			filter_shoes_agegroup,
			filter_shoes_season,
			filter_shoes_style

		} = req.body;

		// Checking null values

		filter_top_brand = (filter_top_brand == undefined) ? [] : (filter_top_brand === '') ? [] : filter_top_brand.split(",");
		filter_top_material = (filter_top_material == undefined) ? [] : (filter_top_material === '') ? [] : filter_top_material.split(",");
		filter_top_pattern = (filter_top_pattern == undefined) ? [] : (filter_top_pattern === '') ? [] : filter_top_pattern.split(",");
		filter_top_finishing = (filter_top_finishing == undefined) ? [] : (filter_top_finishing === '') ? [] : filter_top_finishing.split(",");
		filter_top_color = (filter_top_color == undefined) ? [] : (filter_top_color === '') ? [] : filter_top_color.split(",");
		filter_top_size = (filter_top_size == undefined) ? [] : (filter_top_size === '') ? [] : filter_top_size.split(",");
		filter_top_agegroup = (filter_top_agegroup == undefined) ? [] : (filter_top_agegroup === '') ? [] : filter_top_agegroup.split(",");
		filter_top_season = (filter_top_season == undefined) ? [] : (filter_top_season === '') ? [] : filter_top_season.split(",");
		filter_top_style = (filter_top_style == undefined) ? [] : (filter_top_style === '') ? [] : filter_top_style.split(",");


		filter_bottom_brand = (filter_bottom_brand == undefined) ? [] : (filter_bottom_brand === '') ? [] : filter_bottom_brand.split(",");
		filter_bottom_material = (filter_bottom_material == undefined) ? [] : (filter_bottom_material === '') ? [] : filter_bottom_material.split(",");
		filter_bottom_pattern = (filter_bottom_pattern == undefined) ? [] : (filter_bottom_pattern === '') ? [] : filter_bottom_pattern.split(",");
		filter_bottom_finishing = (filter_bottom_finishing == undefined) ? [] : (filter_bottom_finishing === '') ? [] : filter_bottom_finishing.split(",");
		filter_bottom_color = (filter_bottom_color == undefined) ? [] : (filter_bottom_color === '') ? [] : filter_bottom_color.split(",");
		filter_bottom_size = (filter_bottom_size == undefined) ? [] : (filter_bottom_size === '') ? [] : filter_bottom_size.split(",");
		filter_bottom_agegroup = (filter_bottom_agegroup == undefined) ? [] : (filter_bottom_agegroup === '') ? [] : filter_bottom_agegroup.split(",");
		filter_bottom_season = (filter_bottom_season == undefined) ? [] : (filter_bottom_season === '') ? [] : filter_bottom_season.split(",");
		filter_bottom_style = (filter_bottom_style == undefined) ? [] : (filter_bottom_style === '') ? [] : filter_bottom_style.split(",");


		filter_shoes_brand = (filter_shoes_brand == undefined) ? [] : (filter_shoes_brand === '') ? [] : filter_shoes_brand.split(",");
		filter_shoes_material = (filter_shoes_material == undefined) ? [] : (filter_shoes_material === '') ? [] : filter_shoes_material.split(",");
		filter_shoes_pattern = (filter_shoes_pattern == undefined) ? [] : (filter_shoes_pattern === '') ? [] : filter_shoes_pattern.split(",");
		filter_shoes_finishing = (filter_shoes_finishing == undefined) ? [] : (filter_shoes_finishing === '') ? [] : filter_shoes_finishing.split(",");
		filter_shoes_color = (filter_shoes_color == undefined) ? [] : (filter_shoes_color === '') ? [] : filter_shoes_color.split(",");
		filter_shoes_size = (filter_shoes_size == undefined) ? [] : (filter_shoes_size === '') ? [] : filter_shoes_size.split(",");
		filter_shoes_agegroup = (filter_shoes_agegroup == undefined) ? [] : (filter_shoes_agegroup === '') ? [] : filter_shoes_agegroup.split(",");
		filter_shoes_season = (filter_shoes_season == undefined) ? [] : (filter_shoes_season === '') ? [] : filter_shoes_season.split(",");
		filter_shoes_style = (filter_shoes_style == undefined) ? [] : (filter_shoes_style === '') ? [] : filter_shoes_style.split(",");




		_self.Products.create({
			name,
			brand,
			gender,
			material,
			pattern,
			agegroup,
			color,
			size,
			category,
			season,
			style,
			qr,
			productimage,
			finishing,
			product_id,
			link,

			filter_top_brand,
			filter_top_material,
			filter_top_pattern,
			filter_top_finishing,
			filter_top_color,
			filter_top_size,
			filter_top_agegroup,
			filter_top_season,
			filter_top_style,

			filter_bottom_brand,
			filter_bottom_material,
			filter_bottom_pattern,
			filter_bottom_finishing,
			filter_bottom_color,
			filter_bottom_size,
			filter_bottom_agegroup,
			filter_bottom_season,
			filter_bottom_style,

			filter_shoes_brand,
			filter_shoes_material,
			filter_shoes_pattern,
			filter_shoes_finishing,
			filter_shoes_color,
			filter_shoes_size,
			filter_shoes_agegroup,
			filter_shoes_season,
			filter_shoes_style
			}, (err, doc, numAffected) => {	
			if (err)
				return console.log(err)
			//let qrgeneratorObj = new qr_generator()
			//qrgeneratorObj.generate_png(req._insertId)
			//qrgeneratorObj.generate_pdf(req._insertId)

			return res.json({
				status: 1,
				status_message: "ok",
				data: doc
			})
		})


	}

	updateProducts(req, res, next) {
		console.log("==============================")
		console.log("========== update Products =========")
		console.log("===============================")

		console.log("-------->>>>>req.body:", req.body)

		let {
			_id,
			name,
			gender,
			category,
			brand,
			material,
			pattern,
			finishing,
			agegroup,
			color,
			size,
			season,
			style,
			active,
			product_id,
			link,

			filter_top_brand,
			filter_top_material,
			filter_top_pattern,
			filter_top_finishing,
			filter_top_color,
			filter_top_size,
			filter_top_agegroup,
			filter_top_season,
			filter_top_style,

			filter_bottom_brand,
			filter_bottom_material,
			filter_bottom_pattern,
			filter_bottom_finishing,
			filter_bottom_color,
			filter_bottom_size,
			filter_bottom_agegroup,
			filter_bottom_season,
			filter_bottom_style,

			filter_shoes_brand,
			filter_shoes_material,
			filter_shoes_pattern,
			filter_shoes_finishing,
			filter_shoes_color,
			filter_shoes_size,
			filter_shoes_agegroup,
			filter_shoes_season,
			filter_shoes_style
		} = req.body;


		filter_top_brand = (filter_top_brand == undefined) ? [] : (filter_top_brand === '') ? [] : filter_top_brand.split(",");
		filter_top_material = (filter_top_material == undefined) ? [] : (filter_top_material === '') ? [] : filter_top_material.split(",");
		filter_top_pattern = (filter_top_pattern == undefined) ? [] : (filter_top_pattern === '') ? [] : filter_top_pattern.split(",");
		filter_top_finishing = (filter_top_finishing == undefined) ? [] : (filter_top_finishing === '') ? [] : filter_top_finishing.split(",");
		filter_top_color = (filter_top_color == undefined) ? [] : (filter_top_color === '') ? [] : filter_top_color.split(",");
		filter_top_size = (filter_top_size == undefined) ? [] : (filter_top_size === '') ? [] : filter_top_size.split(",");
		filter_top_agegroup = (filter_top_agegroup == undefined) ? [] : (filter_top_agegroup === '') ? [] : filter_top_agegroup.split(",");
		filter_top_season = (filter_top_season == undefined) ? [] : (filter_top_season === '') ? [] : filter_top_season.split(",");
		filter_top_style = (filter_top_style == undefined) ? [] : (filter_top_style === '') ? [] : filter_top_style.split(",");


		filter_bottom_brand = (filter_bottom_brand == undefined) ? [] : (filter_bottom_brand === '') ? [] : filter_bottom_brand.split(",");
		filter_bottom_material = (filter_bottom_material == undefined) ? [] : (filter_bottom_material === '') ? [] : filter_bottom_material.split(",");
		filter_bottom_pattern = (filter_bottom_pattern == undefined) ? [] : (filter_bottom_pattern === '') ? [] : filter_bottom_pattern.split(",");
		filter_bottom_finishing = (filter_bottom_finishing == undefined) ? [] : (filter_bottom_finishing === '') ? [] : filter_bottom_finishing.split(",");
		filter_bottom_color = (filter_bottom_color == undefined) ? [] : (filter_bottom_color === '') ? [] : filter_bottom_color.split(",");
		filter_bottom_size = (filter_bottom_size == undefined) ? [] : (filter_bottom_size === '') ? [] : filter_bottom_size.split(",");
		filter_bottom_agegroup = (filter_bottom_agegroup == undefined) ? [] : (filter_bottom_agegroup === '') ? [] : filter_bottom_agegroup.split(",");
		filter_bottom_season = (filter_bottom_season == undefined) ? [] : (filter_bottom_season === '') ? [] : filter_bottom_season.split(",");
		filter_bottom_style = (filter_bottom_style == undefined) ? [] : (filter_bottom_style === '') ? [] : filter_bottom_style.split(",");


		filter_shoes_brand = (filter_shoes_brand == undefined) ? [] : (filter_shoes_brand === '') ? [] : filter_shoes_brand.split(",");
		filter_shoes_material = (filter_shoes_material == undefined) ? [] : (filter_shoes_material === '') ? [] : filter_shoes_material.split(",");
		filter_shoes_pattern = (filter_shoes_pattern == undefined) ? [] : (filter_shoes_pattern === '') ? [] : filter_shoes_pattern.split(",");
		filter_shoes_finishing = (filter_shoes_finishing == undefined) ? [] : (filter_shoes_finishing === '') ? [] : filter_shoes_finishing.split(",");
		filter_shoes_color = (filter_shoes_color == undefined) ? [] : (filter_shoes_color === '') ? [] : filter_shoes_color.split(",");
		filter_shoes_size = (filter_shoes_size == undefined) ? [] : (filter_shoes_size === '') ? [] : filter_shoes_size.split(",");
		filter_shoes_agegroup = (filter_shoes_agegroup == undefined) ? [] : (filter_shoes_agegroup === '') ? [] : filter_shoes_agegroup.split(",");
		filter_shoes_season = (filter_shoes_season == undefined) ? [] : (filter_shoes_season === '') ? [] : filter_shoes_season.split(",");
		filter_shoes_style = (filter_shoes_style == undefined) ? [] : (filter_shoes_style === '') ? [] : filter_shoes_style.split(",");



		_self.Products.findByIdAndUpdate(_id, {
			name,
			gender,
			category,
			brand,
			material,
			pattern,
			finishing,
			agegroup,
			color,
			size,
			season,
			style,
			active,
			product_id,
			link,

			filter_top_brand,
			filter_top_material,
			filter_top_pattern,
			filter_top_finishing,
			filter_top_color,
			filter_top_size,
			filter_top_agegroup,
			filter_top_season,
			filter_top_style,

			filter_bottom_brand,
			filter_bottom_material,
			filter_bottom_pattern,
			filter_bottom_finishing,
			filter_bottom_color,
			filter_bottom_size,
			filter_bottom_agegroup,
			filter_bottom_season,
			filter_bottom_style,

			filter_shoes_brand,
			filter_shoes_material,
			filter_shoes_pattern,
			filter_shoes_finishing,
			filter_shoes_color,
			filter_shoes_size,
			filter_shoes_agegroup,
			filter_shoes_season,
			filter_shoes_style
		}, {
			new: true
		}, (err, doc) => {
			if (err)
				console.log(err)
			//console.log(doc);
			return res.json({
				status: 1,
				status_message: "ok",
				data: doc
			})
		})
	}

	logout(req, res, next) {
		res.clearCookie('_connecti', {
			path: '/'
		}).json({
			status: 1,
			status_message: "ok"
		})
	}

	adminDetails(req, res, next) {
		return res.json({
			status: 1,
			status_message: "ok",
			data: req.user
		})
	}
	isAuthenticate(req, res, next) {
		return res.json({
			status: 1,
			status_message: "user authenticated"
		})
	}


	getAppsettings(req, res, next) {

		_self.Settings.find({}, (err, doc) => {

			if (err)
				console.log(err)
			res.json({
				status: 1,
				status_message: "ok",
				data: doc[0]
			})

		})
	}

	postAppsettings(req, res, next) {
		let {
			size,
			agegroup,
			color,
			season,
			style,
			material,
			pattern,
			brand,
			_id,
			finishing,
			categories,
			deleteCategories,
			editCategories
		} = req.body


		console.log("-----------------")
		console.log("-----categories:------", categories)
		console.log("-----------------")

		console.log({
			size,
			agegroup,
			color,
			season,
			style,
			material,
			pattern,
			brand,
			_id,
			finishing,
			categories,
			deleteCategories,
			editCategories
		})

		_self.Settings.findByIdAndUpdate(_id, {
			size,
			agegroup,
			color,
			season,
			style,
			material,
			pattern,
			brand,
			_id,
			finishing
		}, (err, doc, numAffected) => {
			if (err)
				console.log(err)

			for (let items in categories) {
				console.log("------inside for loop------categories[item]:", categories[items])
				console.log("------inside for loop------items:", items)

				console.log("------inside for loop------categories[items]._id:", categories[items]._id)

				_self.Category.findByIdAndUpdate(categories[items]._id, categories[items], (err, doc, numAffected) => {
					if (err)
						console.log("error occured:", err)

					console.log("--------doc:------", doc)

					console.log("------num affected------", numAffected)

				})
			}
			if (_.isEmpty(deleteCategories) === false) {

				for (var items in deleteCategories) {
					console.log(deleteCategories[items])

					if (deleteCategories[items].length > 2) {

						_self.Products.update({
							category: deleteCategories[items]
						}, {
							is_delete: true
						}, {
							multi: true
						}).exec((err, result) => {

							if (err) {
								console.log(err)
								res.json(err)
							}

							console.log(result)
						})

					} else {
						_self.Products.update({
							category: {
								$in: deleteCategories[items]
							}
						}, {
							is_delete: true
						}, {
							multi: true
						}).exec((err, result) => {

							if (err) {
								console.log(err)
								res.json(err)
							}

							console.log(result)
						})
					}
				}

			}

			if (_.isEmpty(editCategories) === false) {

				for (var items in editCategories) {
					_self.Products.update({
						category: editCategories[items].old
					}, {
						$set: {
							"category.$": editCategories[items].new
						}
					}, {
						multi: true
					}).exec((err, result) => {
						if (err) res.json(err)
						console.log(result)
					})
				}


				// for(var items in editCategories){

				// 	if(editCategories[items].path.length > 2){
				// 		Category.find({'nodes.nodes._id': editCategories[items].old}).exec((err, result)=>{
				// 			if(err) console.log(err)

				// 			let nodes = result[0].nodes;
				// 			let _id = editCategories[items].path[1];
				// 			let old = editCategories[items].old;
				// 			let _new = editCategories[items].new;

				// 			_.map(nodes, (x)=>{
				// 				if(x._id === _id){							
				// 					let _index = _.indexOf(x.children, old);
				// 					if(_index != -1)
				// 							x.children[_index] = _new;
				// 					_.map(x.nodes, (y)=>{
				// 						if(y._id === old){
				// 							y._id = _new;
				// 							y.path[2] = _new;
				// 						}
				// 					})
				// 				}
				// 			});

				// 			Category.update({'_id': editCategories[items].path[0]}, {$set:{nodes: nodes}}).exec((err, result)=>{
				// 				if(err) return console.log(err);

				// 				Products.update({category: editCategories[items].old}, { $set: { "category.$" : editCategories[items].new } },  {multi: true}).exec((err, result)=>{
				// 					if(err)	res.json(err)

				// 					console.log(result);

				// 				})
				// 			});
				// 		})

				// 	}else{
				// 		Category.find({'nodes._id': editCategories[items].old}).exec((err, result)=>{
				// 			if(err) console.log(err)

				// 			let old = editCategories[items].old;
				// 			let _new = editCategories[items].new;

				// 			let _index = _.indexOf(result[0].children, old);
				// 				if(_index != -1)
				// 						result[0].children[_index] = _new;

				// 			_.map(result[0].nodes, x=>{
				// 				if(x._id === old){
				// 					x.path[1] = _new;
				// 					x._id = _new;
				// 					_.map(x.nodes, z =>{
				// 						z.path[1] = _new;
				// 					})
				// 				}
				// 			});

				// 			Category.update({'_id': editCategories[items].path[0]}, result[0]).exec((err, result)=>{
				// 				if(err) return console.log(err);

				// 				Products.update({category: editCategories[items].old}, { $set: { "category.$" : editCategories[items].new } },  {multi: true}).exec((err, result)=>{
				// 					if(err)	res.json(err)

				// 					console.log(result);

				// 				})

				// 			});
				// 		})
				// 	}

				// }
			}


			res.json('done')
		})

	}


	productCombination(req, res, next) {
		console.log('-----------------------')
		let {
			selectedlooktype = ``, selectedgender = ``, selectedsize = ``, selectedbrand = ``
		} = req.query
		console.log({
			selectedlooktype,
			selectedgender,
			selectedsize,
			selectedbrand
		})

		let fields = {}
		if (selectedlooktype)
			fields.looktype = selectedlooktype
		if (selectedgender)
			fields.gender = selectedgender
		if (selectedsize)
			fields.size = selectedsize
		if (selectedbrand)
			fields.brand = selectedbrand

		_self.Products.find(fields, (err, doc) => {
			console.log(doc)
			res.json(doc)
		})


	}

	downloadFile(req, res, next) {
		let {
			id
		} = req.params

		let filePath = `${_self.main_folder}/qr/images/${id}.png`
		let fileName = 'download.png'
		console.log({
			filePath,
			fileName
		})

		return res.download(filePath, fileName, (err) => {
			console.log(err)
		})

		//res.end(new Buffer(data, 'binary'));
		//return res.json({asd:'345'});//res.download(filePath, fileName);
	}


	getCategories(req, res, next) {

		_self.Category.find({}, (err, result) => {
			if (err)
				callback(err, 'query error')
			console.log("Categories:  ", result)
			return res.json(result)
		})

	}

}

module.exports = admin