var mysql = require('mysql');
var chalk = require('chalk');

let databaseConstants ={};
try {
  db = JSON.parse(process.env.database);  
  databaseConstants  = {
    host: db.host,
    database: db.database,
    user: db.user,
    password: db.password ,

    multipleStatements: true}
}
catch(e){
  databaseConstants  = {
    host:  'localhost',
    database:  'quiz',
    user:  'root', //'root',
    password:  '',

    multipleStatements: true}
}
var pool = mysql.createPool(databaseConstants);

module.exports = {
  
  databaseConstants:databaseConstants,

  query: function () {
    var sql_args = [];
    var args = [];
    args = Array.prototype.slice.call(arguments);
    // console.log('arguments ::: ', args);
    var callback = args[args.length - 1]; //last arg is callback
    pool.getConnection(function (err, connection) {
      if (err) {
        // connection.release();
        // console.log(err);
        return callback(err);
      }
      if (args.length > 2) {
        sql_args = args[1];
      }
      connection.query(args[0], sql_args, function (err, results) {
        // And done with the connection.
        connection.release(); // always put connection back in pool after last query
        if (err) {
          console.error(chalk.red.bgYellow(err));
          return callback(err);
        }
        callback(null, results);
      });
    });
  }
};
