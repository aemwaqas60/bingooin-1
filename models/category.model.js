let mongoose  = require('mongoose');
let  Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

let CategorySchema = new Schema({
    _id: {type: String, default: null },
    children: { type : Array , "default" : [] },
    nodes:{ type : Array , "default" : [] },
    path:{ type : Array , "default" : [] },
    created_at: {type: Date, default:+new Date()}
});


mongoose.model('Category', CategorySchema);

let Category = mongoose.model('Category');

Category.findOne({}, (err, result)=>{
    if(err)
        console.log(err);
    if (!result) {
        Category.create({}, (err, product, numAffected)=>{
            if(err)
                console.log(err)
        });
    }
}) 
