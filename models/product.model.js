let mongoose = require("mongoose");
let Schema = mongoose.Schema,
  ObjectId = Schema.ObjectId;

let ProductSchema = new Schema({
  name: {
    type: String,
    default: null
  },
  description: {
    type: String,
    default: null
  },
  productimage: {
    type: String,
    default: null
  },
  brand: {
    type: String,
    default: null
  },
  material: {
    type: Array,
    default: []
  },
  pattern: {
    type: String,
    default: null
  },
  finishing: {
    type: String,
    default: null
  },
  color: {
    type: Array,
    default: []
  },
  size: {
    type: Array,
    default: []
  },
  agegroup: {
    type: Array,
    default: []
  },
  season: {
    type: Array,
    default: []
  },
  style: {
    type: String,
    default: null
  },
  gender: {
    type: String,
    default: null
  },
  product_id: {
    type: String,
    default: null
  },
  link: {
    type: String,
    default: null
  },
  images: [{
    name: {
      type: String,
      default: null
    },
    extn: {
      type: String,
      default: null
    },
    default: {
      type: Boolean,
      default: false
    }
  }],
  category: {
    type: Array,
    default: []
  },
  qr: {
    imagelink: {
      type: String,
      default: null
    },
    pdflink: {
      type: String,
      default: null
    }
  },



  filter_top_brand: {
    type: Array,
    default: []
  },
  filter_top_material: {
    type: Array,
    default: []
  },
  filter_top_pattern: {
    type: Array,
    default: []
  },
  filter_top_finishing: {
    type: Array,
    default: []
  },
  filter_top_color: {
    type: Array,
    default: []
  },
  filter_top_size: {
    type: Array,
    default: []
  },
  filter_top_agegroup: {
    type: Array,
    default: []
  },
  filter_top_season: {
    type: Array,
    default: []
  },
  filter_top_style: {
    type: Array,
    default: []
  },


  filter_bottom_brand: {
    type: Array,
    default: []
  },
  filter_bottom_material: {
    type: Array,
    default: []
  },
  filter_bottom_pattern: {
    type: Array,
    default: []
  },
  filter_bottom_finishing: {
    type: Array
  },
  filter_bottom_color: {
    type: Array,
    default: []
  },
  filter_bottom_size: {
    type: Array,
    default: []
  },
  filter_bottom_agegroup: {
    type: Array,
    default: []
  },
  filter_bottom_season: {
    type: Array,
    default: []
  },
  filter_bottom_style: {
    type: Array,
    default: []
  },


  filter_shoes_brand: {
    type: Array,
    default: []
  },
  filter_shoes_material: {
    type: Array,
    default: []
  },
  filter_shoes_pattern: {
    type: Array,
    default: []
  },
  filter_shoes_finishing: {
    type: Array,
    default: []
  },
  filter_shoes_color: {
    type: Array,
    default: []
  },
  filter_shoes_size: {
    type: Array,
    default: []
  },
  filter_shoes_agegroup: {
    type: Array,
    default: []
  },
  filter_shoes_season: {
    type: Array,
    default: []
  },
  filter_shoes_style: {
    type: Array,
    default: []
  },

  is_delete: {
    type: Boolean,
    default: false
  },
  created_at: {
    type: Date,
    default: +new Date()
  }
});

mongoose.model("Product", ProductSchema);