let mongoose  = require('mongoose');
let  Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;


let SettingSchema = new Schema({
    // Common attributes
    size: {type: Array, default: []},
    color: {type: Array, default: []},    
    agegroup: {type: Array, default: []},
    season: {type: Array, default: []},
    style: {type: Array, default: []},
    //material: {type: Array, default: []},
    material: {
        top: {type: Array, default: []},
        bottom: {type: Array, default: []},
        shoes: {type: Array, default: []},
    },
    pattern: {
        top: {type: Array, default: []},
        bottom: {type: Array, default: []},
        shoes: {type: Array, default: []},
    },
    brand: {
        top: {type: Array, default: []},
        bottom: {type: Array, default: []},
        shoes: {type: Array, default: []},
    },
    finishing: {
        top: {type: Array, default: []},
        bottom: {type: Array, default: []},
        shoes: {type: Array, default: []},
    },
    gender: {type: Array, default: []},
    looktype: {type: Array, default: []},
    //brand: {type: Array, default: []},
  
    created_at: {type: Date, default: new Date()},
});


mongoose.model('Settings', SettingSchema);
let Settings = mongoose.model('Settings');

Settings.findOne({}, (err, result)=>{
    if(err)
        console.log(err);
    if (!result) {
        Settings.create({}, (err, product, numAffected)=>{
            if(err)
                console.log(err)
        });
    }
}) 
